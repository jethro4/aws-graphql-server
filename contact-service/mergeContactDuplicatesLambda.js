const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.mergeContactDuplicates = async (event, context, callback) => {
  try {
    const postResponse = await postMergeContactDuplicates(
      event.arguments.input,
    );

    // console.log(
    //   'Done mergeContactDuplicates. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on mergeContactDuplicates', err);
    callback(err);
  }
};

async function postMergeContactDuplicates({
  appName,
  apiKey,
  primaryContactId,
  duplicateContactIds,
  dataObjectActions,
  noteTaskAction,
}) {
  const body = {
    api_key: apiKey,
    primaryContactId,
    dupContactIds: duplicateContactIds,
    doBehavior: dataObjectActions.reduce((acc, item) => {
      acc[item.title] = {
        action: item.action,
        ...(item.relationship && {
          relationship: item.relationship,
        }),
      };

      return acc;
    }, {}),
    noteTaskBehavior: noteTaskAction,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_merge`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {
    primaryContactId,
    success: true,
  };
}
