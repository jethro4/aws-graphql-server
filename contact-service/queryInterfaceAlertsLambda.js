const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryInterfaceAlerts(event.arguments);

    // console.log(
    //   'Done queryInterfaceAlerts. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryInterfaceAlerts', err);

    callback(err);
  }
};

async function queryInterfaceAlerts(query) {
  const {appName, apiKey, userId, contactId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_notification?userId=${userId}&contactId=${contactId}&access_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {message = []} = json;

  return {messages: message.filter((m) => !!m)};
}
