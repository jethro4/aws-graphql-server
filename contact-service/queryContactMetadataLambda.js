const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryContactMetadata(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryContactMetadata. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryContactMetadata', err);

    callback(err);
  }
};

async function queryContactMetadata(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  {email},
) {
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact?api_key=${apiKey}&Email=${email}&sessionName=${sessionName}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const metadata = json ? json[0] : {};
  const items = [];

  if (metadata.NewPurchaseURL) {
    items.push({
      key: 'newPurchase',
      value: metadata.NewPurchaseURL,
    });
  }

  if (metadata.BalancePaymentURL) {
    items.push({
      key: 'memberships',
      value: JSON.stringify(metadata.BalancePaymentURL),
    });
  }

  return {
    items,
  };
}
