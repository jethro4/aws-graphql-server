const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteContact = async (event, context, callback) => {
  try {
    const postResponse = await deleteContactFunc(event.arguments.input);

    // console.log(
    //   'Done deleteContact. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteContact', err);
    callback(err);
  }
};

async function deleteContactFunc({appName, apiKey, id}) {
  const body = {Id: id, api_key: apiKey};

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact`;

  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  return {
    success: json.Status,
    contactId: id,
  };
}
