const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

const EMAIL_COLUMN_KEYS = {
  emailId: 'emailId',
  ContactId: 'contactId',
  ContactName: 'contactName',
  Email: 'email',
  Status: 'status',
  Message: 'message',
  Subject: 'subject',
  'Sent Date/Time': 'sentDateTime',
  'Last Event Received': 'lastEventReceived',
  Attachments: 'attachments',
  Timezone: 'timezone',
  Provider: 'provider',
};

const SMS_COLUMN_KEYS = {
  ContactId: 'contactId',
  ContactName: 'contactName',
  PhoneNumber: 'phoneNumber',
  Status: 'status',
  Message: 'message',
  'Last Event Sent': 'lastEventSent',
  'Sent By Number': 'sentByNumber',
  'Sent By From': 'sentByFrom',
  Timezone: 'timezone',
};

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryCommunicationHistoryLambda(
      event.arguments,
    );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryCommunicationHistoryLambda', err);

    callback(err);
  }
};

function queryCommunicationHistoryLambda(params) {
  if (!params.emailId) {
    return queryCommunicationHistory(params);
  } else if (params.stats) {
    return queryEmailStats(params);
  } else {
    return queryEmailMessage(params);
  }
}

async function queryCommunicationHistory(params) {
  const {appName, apiKey, contactId, type, page, limit} = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/communication/${type}/${contactId}?api_key=${apiKey}&page=${page}&limit=${limit}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformCommunicationHistoryResponse(json, type);
}

async function queryEmailMessage(params) {
  const {appName, apiKey, emailId} = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/communication_item?emailId=${emailId}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformEmailMessageResponse(json);
}

async function queryEmailStats(params) {
  const {appName, apiKey, emailId} = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/communication_item_stats?emailId=${emailId}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformEmailStatsResponse(json);
}

function transformCommunicationHistoryResponse(json, type) {
  const {data = [], total = 0} = json;
  const communicationHistory = [];

  data.forEach((historyItem) => {
    const itemObj = {
      __typename:
        type === 'sms' ? 'CommunicationSMSItem' : 'CommunicationEmailItem',
    };

    Object.entries(historyItem).forEach(([fieldKey, value]) => {
      const fieldName =
        type === 'sms'
          ? SMS_COLUMN_KEYS[fieldKey]
          : EMAIL_COLUMN_KEYS[fieldKey];

      if (fieldName === 'attachments') {
        if (value && typeof value === 'string') {
          const attachments = JSON.parse(value);

          itemObj[fieldName] = attachments.map((a) => {
            const isUrl = !a.content;
            const fileExt = (isUrl ? a.type : a.filename)
              .trim()
              .split('.')
              .pop();

            return {
              thumbnail: !isUrl ? a.content : a.type,
              fileName: a.filename,
              fileExt,
              isUrl,
            };
          });
        }
      } else if (fieldName) {
        itemObj[fieldName] = value;
      }
    });

    communicationHistory.push(itemObj);
  });

  return {
    items: communicationHistory,
    total: parseInt(total),
    __typename: 'CommunicationHistory',
  };
}

function transformEmailMessageResponse(json) {
  const html = json.data?.[0]?.Message;

  return {
    html,
    __typename: 'CommunicationEmailMessage',
  };
}

function transformEmailStatsResponse(json) {
  const {OpenCount: opens, ClickCount: clicks} = json.data || {};

  return {
    opens,
    clicks,
    __typename: 'CommunicationEmailStats',
  };
}
