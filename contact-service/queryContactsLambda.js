const dayjs = require('dayjs');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {convertObjectToArrayWithValue} = require('../utils/array');
const getAccessLevel = require('../utils/getAccessLevel');
const {
  queryDataObjectFields,
  filterContactCustomFields,
} = require('../data-object-service/queryDataObjectFieldsLambda');
const {parseJson} = require('../utils/string');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryContacts(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryContacts. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryContacts', err);

    callback(err);
  }
};

module.exports.queryContacts = queryContacts;

async function queryContacts(
  {
    'app-name': headerAppName,
    'app-api-key': headerApiKey,
    'session-id': sessionName,
  },
  params,
) {
  const appName = headerAppName || params.appName;
  const apiKey = headerApiKey || params.apiKey;
  const {q = '', page = 0, limit = 100, order = 'asc', orderBy} = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/search/contact/by_field_all?api_key=${apiKey}&page=${page}&limit=${limit}`;

  if (q) {
    url = url.concat(`&q=${q}`);
  }

  if (orderBy) {
    url = url.concat(`&sort=${order.toLowerCase()}&sort_by=${orderBy}`);
  }

  if (sessionName) {
    url = url.concat(`&session_name=${sessionName}`);
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const fields = await queryDataObjectFields({
    appName,
    apiKey,
    groupId: 'co_customfields',
  });

  return transformResponse(json, filterContactCustomFields(fields));
}

function transformResponse(json, customFields) {
  const {data = [], count = 0, total = 0} = json;
  const contacts = [];

  data.forEach((contact) => {
    if (!contact.Email) {
      return;
    }

    const createdDate = parseJson(
      contact.DateCreated,
      contact.DateCreated || '',
    );

    const contactObj = {
      id: contact.Id,
      email: contact.Email,
      email2: contact.EmailAddress2,
      email3: contact.EmailAddress3,
      firstName: contact.FirstName,
      middleName: contact.MiddleName,
      lastName: contact.LastName,
      birthday: contact.Birthday,
      title: contact.Title,
      company: contact.Company,
      website: contact.Website,
      jobTitle: contact.JobTitle,
      phoneNumbers: [],
      streetAddress1: contact.StreetAddress1,
      streetAddress2: contact.StreetAddress2,
      city: contact.City,
      state: contact.State,
      postalCode: contact.PostalCode,
      country: contact.Country,
      address2Street1: contact.Address2Street1,
      address2Street2: contact.Address2Street2,
      city2: contact.City2,
      state2: contact.State2,
      postalCode2: contact.PostalCode2,
      country2: contact.Country2,
      loggedInUserPermission: checkLoggedInUserPermission(contact.Permissions),
      phoneNumberExts: [],
      createdDate: transformDate(createdDate.date || createdDate),
      customFields: customFields.map((cf) => {
        return {
          name: cf.name,
          value: contact[cf.name],
        };
      }),
    };

    for (let i = 1; i <= 5; i++) {
      const phoneNumber = contact[`Phone${i}`];
      if (phoneNumber) {
        contactObj.phoneNumbers.push(phoneNumber);
      }
    }

    for (let i = 1; i <= 5; i++) {
      const phoneNumberExt = contact[`Phone${i}Ext`];
      if (phoneNumberExt) {
        contactObj.phoneNumberExts.push(phoneNumberExt);
      }
    }

    contacts.push(contactObj);
  });

  return {
    items: contacts,
    count: Number(count),
    total: parseInt(total),
  };
}

function transformDate(date, format = 'YYYY-MM-DD hh:mm:ss') {
  return date && dayjs(date, format).format('YYYY-MM-DD');
}

function checkLoggedInUserPermission(permissions) {
  const loggedInUserContactPermissions = convertObjectToArrayWithValue(
    permissions,
  ).map((accessArg) => {
    const accessValue =
      accessArg.id !== 'Global'
        ? accessArg.value
        : {Global: {Status: true, Access: accessArg.value}};

    return {
      access: convertObjectToArrayWithValue(accessValue).map((type) => {
        const {Status: status, Access: permission} = type.value;
        return {
          type: type.id,
          status,
          permission: getAccessLevel(permission),
        };
      }),
    };
  });

  const contactViewPermissions = loggedInUserContactPermissions.reduce(
    (acc, item) => {
      return acc.concat(item.access);
    },
    [],
  );

  const isLoggedInUserAllowedToWrite = contactViewPermissions.some(
    (p) => p.status === true && p.permission === 'READ_WRITE',
  );

  const isLoggedInUserAllowedToRead =
    isLoggedInUserAllowedToWrite ||
    contactViewPermissions.some(
      (p) => p.status === true && p.permission === 'READ_ONLY',
    );
  // || !contactViewPermissions.some(
  //   (p) => p.status === true && p.permission === "READ_ONLY"
  // );

  if (isLoggedInUserAllowedToWrite) {
    return 'READ_WRITE';
  } else if (isLoggedInUserAllowedToRead) {
    return 'READ_ONLY';
  }

  return 'NO_ACCESS';
}
