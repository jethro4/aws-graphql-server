const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {queryContacts} = require('./queryContactsLambda');

module.exports.createOrUpdateContact = async (event, context, callback) => {
  try {
    const postResponse = await postContact(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateContact. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateContact', err);
    callback(err);
  }
};

async function postContact(headers, contact) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const phoneNumsObj = getContactPhoneNumbers(contact);
  const phoneNumExtsObj = getContactPhoneNumberExts(contact);

  const body = {
    ...(contact.id && {contactId: contact.id}),
    ...(sessionName && {session_name: sessionName}),
    access_key: apiKey,
    ...(!contact.autoGenerateEmail
      ? {
          autoGenerateEmail: 'no',
          Email: contact.email,
          EmailAddress2: contact.email2,
          EmailAddress3: contact.email3,
        }
      : {
          autoGenerateEmail: 'yes',
          Email: '',
          EmailAddress2: '',
          EmailAddress3: '',
        }),
    FirstName: contact.firstName,
    MiddleName: contact.middleName,
    LastName: contact.lastName,
    Birthday: contact.birthday,
    Title: contact.title,
    Company: contact.company,
    Website: contact.website,
    JobTitle: contact.jobTitle,
    StreetAddress1: contact.streetAddress1,
    StreetAddress2: contact.streetAddress2,
    City: contact.city,
    State: contact.state,
    PostalCode: contact.postalCode,
    Country: contact.country,
    Address2Street1: contact.address2Street1,
    Address2Street2: contact.address2Street2,
    City2: contact.city2,
    State2: contact.state2,
    PostalCode2: contact.postalCode2,
    Country2: contact.country2,
    ...phoneNumsObj,
    ...phoneNumExtsObj,
  };

  contact.customFields &&
    contact.customFields.forEach((cf) => {
      body[cf.name] = cf.value;
    });

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact`;

  if (sessionName) {
    url = url.concat(`?session_name=${sessionName}`);
  }

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  const transformedJson = transformPostResponse(json);

  const transformedContact = await getTransformedContactResponse(
    headers,
    transformedJson.id,
  );

  return transformedContact;
}

function getContactPhoneNumbers(contact) {
  const phoneNumsObj = {};

  for (let i = 0; i < 3; i++) {
    const num = contact.phoneNumbers?.[i] || '';

    phoneNumsObj[`Phone${i + 1}`] = num;
  }

  return phoneNumsObj;
}

function getContactPhoneNumberExts(contact) {
  const phoneNumExtsObj = {};

  for (let i = 0; i < 3; i++) {
    const num = contact.phoneNumberExts?.[i] || '';

    phoneNumExtsObj[`Phone${i + 1}Ext`] = num;
  }

  return phoneNumExtsObj;
}

async function getTransformedContactResponse(headers, q) {
  const listContacts = await queryContacts(
    {...headers, 'session-id': null},
    {
      q,
      page: 0,
      limit: 1,
    },
  );

  return {
    ...listContacts.items[0],
    loggedInUserPermission: 'READ_WRITE',
  };
}

function transformPostResponse(json) {
  const contact = json;

  const contactObj = {
    id: contact.contactId,
  };

  return contactObj;
}
