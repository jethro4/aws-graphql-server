const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.forgotPassword = async (event, context, callback) => {
  const {appName, step = 1, email, code, newPassword} = event.arguments.input;

  try {
    let postResponse;

    if (step === 1) {
      const body = {
        Step: 'step-1',
        Email: email,
      };

      postResponse = await processRequest(appName, body);
    } else if (step === 2) {
      const body = {
        Step: 'step-2',
        EmailPassed: email,
        Code: code,
      };

      postResponse = await processRequest(appName, body);
    } else if (step === 3) {
      const body = {
        Step: 'step-3',
        EmailPassed: email,
        Code: code,
        NewPassword: newPassword,
      };

      postResponse = await processRequest(appName, body);
    }

    // console.log(
    //   'Done forgotPassword. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on forgotPassword', err);
    callback(err);
  }
};

async function processRequest(appName, body) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/login_recovery`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  if (json.error) {
    throw new Error(json.error);
  }

  return {
    success: true,
  };
}
