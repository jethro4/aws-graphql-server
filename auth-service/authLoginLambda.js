const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {
  transformResponse: transformAccessResponse,
} = require('../permission-service/queryAccessPermissionsLambda');

module.exports.userLogin = async (event, context, callback) => {
  const {appName, email, password} = event.arguments;

  try {
    const authResponse = await login(appName, email, password);

    // console.log('Done userLogin. authResponse:', JSON.stringify(authResponse));

    callback(null, authResponse);
  } catch (err) {
    if (err.status === 403) {
      err.message = 'Invalid Email / Password';
    }

    console.log('Error on userLogin', err);

    callback(err);
  }
};

async function login(appName, email, password) {
  const body = {
    App_name: appName,
    Email: email,
    Password: password,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/login`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {
    session_name: sessionId,
    api_key,
    session_data: sessionData,
    autologin_key,
    user_access: userAccess,
  } = json;
  const userDetails =
    (sessionData.Details && JSON.parse(sessionData.Details)) || {};
  const access = {
    Permission: userAccess || {},
  };

  const user = {
    sessionId,
    autoLoginKey: autologin_key,
    apiKey: api_key,
    firstName: userDetails.FirstName,
    lastName: userDetails.LastName,
    userId: userDetails.Id,
    signature: sessionData.Signature,
    permission: transformAccessResponse(access),
  };

  return user;
}
