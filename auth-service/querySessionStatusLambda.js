const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await querySessionStatus(event.arguments);

    // console.log('Done querySessionStatus. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on querySessionStatus', err);
    callback(err);
  }
};

async function querySessionStatus(query) {
  const {appName, apiKey, sessionName} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/validate_session?sessionId=${sessionName}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {status} = json;

  return {expired: status === 0};
}
