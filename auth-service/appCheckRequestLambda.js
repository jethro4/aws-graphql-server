const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.appCheck = async (event, context, callback) => {
  const {appName, apiKey} = event.arguments;

  try {
    await connect(appName, apiKey);

    // console.log("Done appCheck. connectResponse:", JSON.stringify(connectResponse));

    callback(null, {
      success: true,
    });
  } catch (err) {
    console.log('Error on appCheck', err);
    callback(err);
  }
};

async function connect(appName, apiKey) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/app_check/?access_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return json;
}
