const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    let queryResponse = await queryWorkflowBoards(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryWorkflowBoards. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryWorkflowBoards', err);

    callback(err);
  }
};

async function queryWorkflowBoards(
  {'app-name': appName, 'app-api-key': apiKey},
  query,
) {
  const {type} = query;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_tools/workflowboard?api_key=${apiKey}`;

  if (type) {
    url = url.concat(`&queryConnectedDataType=${type}`);
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const workflowBoards = transformResponse(json);

  return workflowBoards;
}

function transformResponse(json) {
  const {data = []} = json;
  const workflowBoards = [];

  for (var workflowBoard of data) {
    const workflowBoardObj = {
      id: workflowBoard.workFlowBoardId,
      title: workflowBoard.workFlowBoardTitle,
      description: workflowBoard.workFlowBoardDescription,
      type: workflowBoard.workFlowBoardDOType,
      displayFields: workflowBoard.displayFields || [],
      keyField: workflowBoard.keyField,
      relationship: workflowBoard.relationship,
      activeColumnsColor: workflowBoard.columnsColor,
    };

    workflowBoards.push(workflowBoardObj);
  }

  return workflowBoards;
}
