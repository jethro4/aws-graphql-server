const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteCustomTab = async (event, context, callback) => {
  try {
    const deleteResponse = await deleteCustomTabFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteCustomTab. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deleteCustomTab', err);
    callback(err);
  }
};

async function deleteCustomTabFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/custom_tabs`;
  const response = await http.delete(url, body);
  await getJsonResponse(response);

  return {
    id: params.id,
    success: true,
  };
}
