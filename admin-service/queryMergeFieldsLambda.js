const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

const query = async (event, context, callback) => {
  try {
    const queryResponse = await queryMergeFields(event.arguments);

    // console.log('Done queryMergeFields. queryResponse:', queryResponse);

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryMergeFields', err);

    callback(err);
  }
};

module.exports.query = query;

async function queryMergeFields({appName, apiKey, types}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/merge_field?types=${types}&api_key=${apiKey}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json);

  return transformedJson;
}

function transformResponse(json) {
  const {mergeFields} = json;

  return {
    mergeFields,
  };
}
