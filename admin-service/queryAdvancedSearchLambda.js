const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformResponseWidgetsData} = require('./queryWidgetsDataLambda');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryAdvancedSearch(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryAdvancedSearch. response:',
    //   JSON.stringify(response),
    // );

    callback(null, response);
  } catch (err) {
    console.log('Error on queryAdvancedSearch', err);
    callback(err);
  }
};

async function queryAdvancedSearch(headers, params) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    saveQuery: params.shouldSave ? 'yes' : 'no',
    ...(params.shouldSave && {
      queryId: params.queryId,
      queryName: params.name,
      queryDescription: params.description,
    }),
    page: params.page || 0,
    limit: params.limit || 25,
    order: params.order ? params.order.toUpperCase() : 'ASC',
    orderBy: params.orderBy,
    queryConnectedDataType: params.doType,
    to: params.chosenFields ? JSON.parse(params.chosenFields) : [],
    queryCDField: params.doConditions
      ? JSON.parse(params.doConditions).map(
          ({name, logic, operator, values, value = ''}) => ({
            queryCDFieldName: name,
            queryCDFieldLogic: logic,
            queryCDFieldOperator: operator,
            ...(values && values.length > 0
              ? {
                  queryCDFieldValues: values,
                }
              : {
                  queryCDFieldValue: value,
                }),
          }),
        )
      : [],
    queryContact: params.contactConditions
      ? JSON.parse(params.contactConditions).map(({relationship, logic}) => ({
          queryContactRelationship: relationship,
          queryContactRelationshipFieldLogic: logic,
        }))
      : [],
    queryUser: params.userConditions
      ? JSON.parse(params.userConditions).map(
          ({relationship, logic, operator, userId}) => ({
            queryUserRelationship: relationship,
            queryUserRelationshipFieldLogic: logic,
            queryUserOperator: operator,
            queryUserId: userId,
          }),
        )
      : [],
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/advanced_search`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  return {
    queryId: json.query.queryId,
    results: transformResponseWidgetsData(
      {
        data: {
          NewResults: json.results,
          TableColumn: json.tableColumn,
          FieldProperties: json.fieldProperties,
          DOFields: json.DOFields,
          Total: json.total,
        },
      },
      'QueryWidgets',
    ),
  };
}
