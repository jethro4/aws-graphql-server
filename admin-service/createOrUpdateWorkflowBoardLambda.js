const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateWorkflowBoard = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postWorkflowBoard(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateWorkflowBoard. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateWorkflowBoard', err);
    callback(err);
  }
};

async function postWorkflowBoard(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    workFlowBoardTitle: params.title,
    workFlowBoardDescription: params.description,
    workFlowBoardDOType: params.type,
    displayFields: params.displayFields,
    keyField: params.keyField,
    relationship: params.relationship,
    columnsColor: params.activeColumnsColor,
    ...(params.id && {
      workFlowBoardId: params.id,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_tools/workflowboard`;
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: params.id || json?.data?.workFlowBoardId || `board-id-${Date.now()}`,
    title: params.title,
    description: params.description,
    type: params.type,
    displayFields: params.displayFields,
    keyField: params.keyField,
    relationship: params.relationship,
    activeColumnsColor: params.activeColumnsColor,
  };
}
