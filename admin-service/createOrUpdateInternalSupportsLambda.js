const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateInternalSupports = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postInternalSupportEmail(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateInternalSupports. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateInternalSupports', err);
    callback(err);
  }
};

async function postInternalSupportEmail(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    internalSupport: params.internalSupports,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/internal_support/add_support`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
