const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryFormBuilders(
      event.request.headers,
      event.arguments,
    );

    // console.log('Done queryFormBuilders. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryFormBuilders', err);
    callback(err);
  }
};

async function queryFormBuilders(headers, params) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/form_builder`;

  if (params.id) {
    url += `/${params.id}`;
  }

  url += `?api_key=${apiKey}&sessionName=${sessionName}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  if (params.id) {
    return transformObjectResponse(json, params.id);
  } else {
    return transformArrayResponse(json);
  }
}

function transformObjectResponse(json, id) {
  const {data = []} = json;

  return transformItem(data.find((d) => d.id === id));
}

function transformArrayResponse(json) {
  const {data = []} = json;

  let items = [];

  data.forEach((d) => {
    const {id, title, description, type} = d;

    items.push({id, title, description, type});
  });

  return items;
}

function transformItem(item) {
  return {
    id: item.id,
    title: item.title,
    description: item.description,
    thankMessage: item.thankMessage,
    type: item.type,
    contactGroups: item.contactGroups.map((group) => ({
      ...group,
      fields: group.fields.map((field) => ({
        __typename: 'FormBuilderField',
        fieldId: field.id,
        label: field.label,
        tooltip: field.tooltip,
        required: field.required,
        column: field.column,
      })),
    })),
    dataObject: item.dataObject
      ? {
          dataObjectId: item.dataObject.id,
          dataObjectGroups: item.dataObject.dataObjectGroups.map((group) => ({
            ...group,
            fields: group.fields.map((field) => ({
              __typename: 'FormBuilderField',
              fieldId: field.id,
              label: field.label,
              tooltip: field.tooltip,
              required: field.required,
              column: field.column,
              ...(field.type === 'file' && {
                __typename: 'UploadFormBuilderField',
                type: field.type,
                ...(field.referenceFieldId
                  ? {
                      referenceFieldId: field.referenceFieldId,
                    }
                  : {
                      multiple: field.multiple,
                      mimeTypes: field.mimeTypes,
                    }),
              }),
            })),
          })),
          relationships: item.dataObject.relationships,
          ...(item.dataObject.integrationField && {
            integrationField: {
              ...item.dataObject.integrationField,
              displayFields: item.dataObject.integrationField.displayFields,
              conditions: item.dataObject.integrationField.conditions.map(
                (condition) => ({
                  name: condition.queryCDFieldName,
                  logic: condition.queryCDFieldLogic,
                  operator: condition.queryCDFieldOperator,
                  values: condition.queryCDFieldValues,
                  value: condition.queryCDFieldValue,
                }),
              ),
            },
          }),
        }
      : null,
    customCSS: Buffer.from(item.customCSS, 'base64').toString('utf8'),
    redirectUrl: item.redirectUrl,
    submitLabel: item.submitLabel,
    jsCode: item.jsCode,
    iFrameCode: item.iFrameCode,
    htmlCode: item.htmlCode,
    link: item.link,
  };
}
