const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteWorkflowBoard = async (event, context, callback) => {
  try {
    const deleteResponse = await deleteWorkflowBoardFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteWorkflowBoard. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deleteWorkflowBoard', err);
    callback(err);
  }
};

async function deleteWorkflowBoardFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    Id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_tools/workflowboard`;
  const response = await http.delete(url, body);
  const json = await getJsonResponse(response);

  return {
    id: params.id,
    success: json.Status,
  };
}
