const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryListAssignments(event.arguments);

    // console.log(
    //   'Done queryListAssignments. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryListAssignments', err);

    callback(err);
  }
};

async function queryListAssignments({appName, apiKey, queryId}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/list_assignment?api_key=${apiKey}&queryId=${queryId}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json, queryId);

  return transformedJson;
}

function transformResponse(json, queryId) {
  const {
    assignmentType = '',
    unassignmentBehavior = '',
    selectedRoundRobin = '',
    selectedUsers,
    assignedRelationship = '',
  } = json && json.assignedRelationship ? json : {};

  return {
    id: `assignment-${queryId}`,
    assignmentType,
    unassignmentBehavior,
    selectedRoundRobin,
    selectedUsers: selectedUsers || [],
    assignedRelationship,
    status: json ? json.status : 'active',
  };
}
