const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryQueryBuilders(event.request.headers);

    // console.log(
    //   'Done queryQueryBuilders. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryQueryBuilders', err);

    callback(err);
  }
};

async function queryQueryBuilders({
  'app-name': appName,
  'app-api-key': apiKey,
}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_query?api_key=${apiKey}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json);

  return transformedJson;
}

function transformResponse(json) {
  const {data = []} = json;
  const queryBuilders = [];

  data.forEach((item) => {
    queryBuilders.push({
      id: item.queryId,
      doType: item.queryConnectedDataType,
      name: item.queryName,
      description: item.queryDescription,
      doConditions: item.queryCDField.map((qc) => ({
        name: qc.queryCDFieldName,
        logic: qc.queryCDFieldLogic,
        operator: qc.queryCDFieldOperator,
        values: qc.queryCDFieldValues,
        value: qc.queryCDFieldValue,
      })),
      contactConditions: item.queryContact
        .filter((qc) => !!qc.queryContactRelationship)
        .map((qc) => ({
          relationship: qc.queryContactRelationship,
          logic: qc.queryContactRelationshipFieldLogic,
        })),
      userConditions: item.queryUser
        .filter((qc) => !!qc.queryUserRelationship)
        .map((qc) => ({
          relationship: qc.queryUserRelationship,
          logic: qc.queryUserRelationshipFieldLogic,
          operator: qc.queryUserOperator,
          userId: qc.queryUserId,
        })),
      chosenFields: item.chosenFields || [],
      criteriaMessageA: item.queryMessageA,
      criteriaMessageB: item.queryMessageB,
    });
  });

  return queryBuilders;
}
