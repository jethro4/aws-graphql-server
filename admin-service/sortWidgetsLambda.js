const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.sortWidgets = async (event, context, callback) => {
  const {appName, apiKey, ...params} = event.arguments.input;

  try {
    const postResponse = await postSortWidgets(appName, apiKey, params);

    // console.log(
    //   'Done sortWidgets. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sortWidgets', err);
    callback(err);
  }
};

async function postSortWidgets(appName, apiKey, params) {
  const body = {
    api_key: apiKey,
    userId: params.userId,
    WidgetsOrder: params.widgetsOrder.map((title) =>
      title.replace(/('|\s)/g, ''),
    ),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
