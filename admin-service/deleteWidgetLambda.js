const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteWidget = async (event, context, callback) => {
  try {
    const deleteResponse = await deleteWidgetFunc(event.arguments.input);

    // console.log(
    //   'Done deleteWidget. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deleteWidget', err);
    callback(err);
  }
};

module.exports.deleteWidgetFunc = deleteWidgetFunc;

async function deleteWidgetFunc({appName, apiKey, id, userId, queryId}) {
  const body = {
    api_key: apiKey,
    userId: userId,
    QueryId: queryId,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget`;

  const response = await http.delete(url, body);

  await getJsonResponse(response);

  return {
    success: true,
    widgetId: id,
  };
}
