const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteQueryBuilder = async (event, context, callback) => {
  try {
    const postResponse = await deleteQueryBuilderFunc(event.arguments.input);

    // console.log(
    //   'Done deleteQueryBuilder. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteQueryBuilder', err);
    callback(err);
  }
};

async function deleteQueryBuilderFunc({appName, apiKey, queryId}) {
  const body = {
    api_key: apiKey,
    queryId,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_query`;
  const response = await http.delete(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
