const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

const query = async (event, context, callback) => {
  try {
    const queryResponse = await queryWidgets(event.arguments);

    // console.log('Done queryWidgets. queryResponse:', queryResponse);

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryWidgets', err);

    callback(err);
  }
};

module.exports.query = query;
module.exports.queryWidgets = queryWidgets;

async function queryWidgets({appName, apiKey, userId}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget?userId=${userId}&api_key=${apiKey}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json);

  return transformedJson;
}

function transformResponse(json) {
  const {
    addedWidgets = [],
    availableTaskWidgets = [],
    availableQueryWidgets = [],
  } = json;

  return {
    addedWidgets: transformAddedWidgets(addedWidgets),
    availableTaskWidgets: transformAvailableWidgets(
      availableTaskWidgets,
      'MacantaTaskSearch',
    ),
    availableQueryWidgets: transformAvailableWidgets(
      availableQueryWidgets,
      'MacantaQuery',
    ),
  };
}

function transformAddedWidgets(addedWidgets) {
  let widgets = [];
  for (let w of addedWidgets) {
    widgets.push({
      id: `${w.UserId}-${w.QueryId}-${w.AddedBy}`,
      userId: w.UserId,
      queryId: w.QueryId,
      type: w.Type,
      title: w.Title,
      access: w.Access || '',
      addedBy: w.AddedBy,
      currentCount: Number(w.CurrentCount),
      last7DayCount: Number(w.Last7DayCount),
      last30DayCount: Number(w.Last30DayCount),
      last90DayCount: Number(w.Last90DayCount),
      allCount: Number(w.AllCount),
      savedSearchID: w.SavedSearchID,
      groupName: w.GroupName,
    });
  }

  return widgets;
}

function transformAvailableWidgets(availableWidgets, type) {
  let widgets = [];
  for (let w of availableWidgets) {
    widgets.push({
      id:
        type === 'MacantaTaskSearch'
          ? `${w.UserId}-${type}`
          : `${w.UserId}-${w.QueryId}-${type}`,
      userId: w.UserId,
      queryId: w.QueryId,
      type,
      title: w.Title,
      access: w.Access || '',
    });
  }

  return widgets;
}
