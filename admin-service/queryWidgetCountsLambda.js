const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryWidgetCounts(
      event.request.headers,
      event.arguments,
    );

    // console.log('Done query. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on query', err);
    callback(err);
  }
};

async function queryWidgetCounts(
  {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
    force,
  },
  {userId, queryId},
) {
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget_count?api_key=${apiKey}&userId=${userId}&queryId=${queryId}&sessionName=${sessionName}`;

  if (force) {
    url += '&force=true';
  }

  const response = await http.get(url);
  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {
    CurrentCount: currentCount,
    Last7DayCount: last7DayCount,
    Last30DayCount: last30DayCount,
    Last90DayCount: last90DayCount,
    AllCount: allCount,
  } = json && json[0];

  return {
    currentCount: parseInt(currentCount),
    last7DayCount: parseInt(last7DayCount),
    last30DayCount: parseInt(last30DayCount),
    last90DayCount: parseInt(last90DayCount),
    allCount: parseInt(allCount),
  };
}
