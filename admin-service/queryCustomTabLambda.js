const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    let queryResponse = await queryCustomTab(event.arguments);

    // console.log(
    //   'Done queryCustomTab. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryCustomTab', err);

    callback(err);
  }
};

async function queryCustomTab(query) {
  const {appName, apiKey, id} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/custom_tabs/id/${id}?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const customTabs = transformResponse(json, id);

  return customTabs;
}

function transformResponse(json, id) {
  const {title = '', content = '', link} = json;

  return {
    id: `tab-${id}`,
    title,
    content: Buffer.from(content, 'base64').toString('utf8'),
    hasLink: link === 'yes',
  };
}
