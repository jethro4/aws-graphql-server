const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.unassignListUsers = async (event, context, callback) => {
  try {
    const postResponse = await postUnassignListUsers(event.arguments.input);

    // console.log(
    //   'Done unassignListUsers. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on unassignListUsers', err);
    callback(err);
  }
};

async function postUnassignListUsers({appName, apiKey, queryId, action}) {
  const body = {
    api_key: apiKey,
    queryId,
    action,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/unassign_users`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
