const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryInternalSupports(event.request.headers);

    // console.log(
    //   'Done queryInternalSupports. response:',
    //   JSON.stringify(response),
    // );

    callback(null, response);
  } catch (err) {
    console.log('Error on queryInternalSupports', err);
    callback(err);
  }
};

async function queryInternalSupports(headers) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/internal_support/get_support?sessionName=${sessionName}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = []} = json;

  let items = [];

  data.forEach((support) => {
    items.push({
      email: support.email,
      fullName: support.fullName,
    });
  });

  return items;
}
