const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryListRoundRobins(event.arguments);

    // console.log(
    //   'Done queryListRoundRobins. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryListRoundRobins', err);

    callback(err);
  }
};

async function queryListRoundRobins({appName, apiKey, groupId}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/list_round_robins?api_key=${apiKey}&groupId=${groupId}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json);

  return transformedJson;
}

function transformResponse(json = []) {
  const roundRobins = [];

  json.forEach(({id, name, relationship}) => {
    roundRobins.push({
      id,
      name,
      relationship,
    });
  });

  return roundRobins;
}
