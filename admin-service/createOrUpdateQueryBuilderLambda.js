const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateQueryBuilder = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postQueryBuilder(event.arguments.input);

    // console.log(
    //   'Done createOrUpdateQueryBuilder. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateQueryBuilder', err);
    callback(err);
  }
};

async function postQueryBuilder({
  appName,
  apiKey,
  id,
  doType,
  name,
  description,
  doConditions = [],
  contactConditions = [],
  userConditions = [],
  chosenFields = [],
  criteriaMessageA,
  criteriaMessageB,
}) {
  const body = {
    api_key: apiKey,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    queryCDField: doConditions.map(
      ({name, logic, operator, values, value = ''}) => ({
        queryCDFieldName: name,
        queryCDFieldLogic: logic,
        queryCDFieldOperator: operator,
        ...(values && values.length > 0
          ? {
              queryCDFieldValues: values,
            }
          : {
              queryCDFieldValue: value,
            }),
      }),
    ),
    queryContact: contactConditions.map(({relationship, logic}) => ({
      queryContactRelationship: relationship,
      queryContactRelationshipFieldLogic: logic,
    })),
    queryUser: userConditions.map(
      ({relationship, logic, operator, userId}) => ({
        queryUserRelationship: relationship,
        queryUserRelationshipFieldLogic: logic,
        queryUserOperator: operator,
        queryUserId: userId,
      }),
    ),
    chosenFields,
    queryMessageA: criteriaMessageA,
    queryMessageB: criteriaMessageB,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_query`;
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    doType,
    name,
    description,
    doConditions,
    contactConditions,
    userConditions,
    chosenFields,
    criteriaMessageA,
    criteriaMessageB,
  };
}
