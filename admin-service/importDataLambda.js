const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.importData = async (event, context, callback) => {
  try {
    const deleteResponse = await importDataFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done importData. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on importData', err);
    callback(err);
  }
};

async function importDataFunc(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    importDataType: params.dataType,
    fileName: params.fileName,
    b64EncodedFile: params.attachment,
    ...(params.dataType !== 'Contacts' && {
      importDataId: params.fieldName,
      itemStatus: params.automationStatus,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_tools/import`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
