const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

const query = async (event, context, callback) => {
  try {
    const queryResponse = await queryBlastProgress(event.arguments);

    // console.log('Done queryBlastProgress. queryResponse:', queryResponse);

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryBlastProgress', err);

    callback(err);
  }
};

module.exports.query = query;
module.exports.queryBlastProgress = queryBlastProgress;

async function queryBlastProgress({appName, apiKey, userId}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/messaging_queue?userId=${userId}&api_key=${apiKey}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json);

  return transformedJson;
}

function transformResponse(json) {
  const {data = []} = json || {};

  const blastData = [];

  for (let d of data) {
    blastData.push({
      type: d['Type'],
      queryId: d['QueryId'],
      subject: d['Subject'],
      senderEmail: d['SenderEmail'],
      groupName: d['GroupName'],
      widgetName: d['WidgetName'],
      status: d['Status'],
      progress: d['Progress'],
      created: d['Created'],
      updated: d['Updated'],
      runtime: d['Runtime'],
    });
  }

  return {
    items: blastData,
  };
}
