const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {
  getAccessPermissions,
  filterByAccess,
} = require('../utils/accessFilters');

module.exports.query = async (event, context, callback) => {
  try {
    let queryResponse = await queryCustomTabs(event.arguments);

    if (event.arguments.userEmail) {
      queryResponse = await filterCustomTabsWithPermissions(
        queryResponse,
        event.arguments,
      );
    }

    // console.log(
    //   'Done queryCustomTabs. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryCustomTabs', err);

    callback(err);
  }
};

async function queryCustomTabs(query) {
  const {appName, apiKey} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/custom_tabs?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const customTabs = transformResponse(json);

  return customTabs;
}

async function filterCustomTabsWithPermissions(customTabs, query) {
  const {appName, apiKey, userEmail: email} = query;

  const accessPermissions = await getAccessPermissions({
    appName,
    apiKey,
    email,
  });

  if (accessPermissions.isAdmin) {
    return customTabs;
  } else {
    const permittedCustomTabs = filterByAccess(accessPermissions, null, 'tab');

    const filteredCustomTabs = customTabs.filter((type) => {
      return permittedCustomTabs.some((tab) => type.id === tab.groupId);
    });

    return filteredCustomTabs;
  }
}

function transformResponse(json) {
  const {data = []} = json;
  const customTabs = [];

  for (var customTab of data) {
    const customTabObj = {
      id: customTab.id,
      title: customTab.title,
    };

    customTabs.push(customTabObj);
  }

  return customTabs;
}
