const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteInternalSupports = async (event, context, callback) => {
  try {
    const postResponse = await deleteInternalSupportsFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteInternalSupports. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteInternalSupports', err);
    callback(err);
  }
};

async function deleteInternalSupportsFunc(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    emails: params.emails,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/internal_support/remove_support`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {
    success: true,
  };
}
