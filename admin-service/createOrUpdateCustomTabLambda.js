const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateCustomTab = async (event, context, callback) => {
  const {appName, apiKey, ...params} = event.arguments.input;

  try {
    const postResponse = await postCustomTab(appName, apiKey, params);

    // console.log(
    //   'Done createOrUpdateCustomTab. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateCustomTab', err);
    callback(err);
  }
};

async function postCustomTab(appName, apiKey, params) {
  const body = {
    api_key: apiKey,
    title: params.title,
    content: Buffer.from(params.content || '').toString('base64'),
    link: params.link,
    ...(params.id && {
      id: params.id,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/custom_tabs`;
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: `tab-${params.id || json?.data?.Id}`,
    title: params.title,
    content: params.content,
    hasLink: params.link === 'yes',
  };
}
