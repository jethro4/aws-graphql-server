const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateListAssignment = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postListAssignment(event.arguments.input);

    // console.log(
    //   'Done createOrUpdateListAssignment. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateListAssignment', err);
    callback(err);
  }
};

async function postListAssignment({appName, apiKey, ...params}) {
  const body = {
    api_key: apiKey,
    queryId: params.queryId,
    assignmentType: params.assignmentType,
    unassignmentBehavior: params.unassignmentBehavior,
    selectedRoundRobin: params.selectedRoundRobin,
    selectedUsers: params.selectedUsers,
    assignedRelationship: params.assignedRelationship,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/${
    params.status !== 'inactive' ? 'list_assignment' : 'activate_assignment'
  }`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    id: `assignment-${params.queryId}`,
    assignmentType: params.assignmentType,
    unassignmentBehavior: params.unassignmentBehavior,
    selectedRoundRobin: params.selectedRoundRobin,
    selectedUsers: params.selectedUsers,
    assignedRelationship: params.assignedRelationship,
    status: 'active',
  };
}
