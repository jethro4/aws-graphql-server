const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {queryWidgets} = require('./queryWidgetsLambda');
const {deleteWidgetFunc} = require('./deleteWidgetLambda');

module.exports.createOrUpdateWidget = async (event, context, callback) => {
  const {appName, apiKey, isEdit, ...params} = event.arguments.input;

  try {
    if (isEdit) {
      await deleteWidgetFunc({
        appName,
        apiKey,
        userId: params.userId,
        queryId: params.queryId,
      });
    }
    const postResponse = await postWidget(appName, apiKey, params);

    // console.log(
    //   'Done createOrUpdateWidget. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateWidget', err);
    callback(err);
  }
};

async function postWidget(appName, apiKey, params) {
  const body = {
    api_key: apiKey,
    userId: params.userId,
    QueryId: params.queryId,
    Title: params.title,
    Group: params.type,
    Access: params.access,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  const allWidgets = await queryWidgets({
    appName,
    apiKey,
    userId: params.userId,
  });
  const savedWidget = allWidgets.addedWidgets.find(
    (addedWidget) => addedWidget.queryId === params.queryId,
  );

  return savedWidget;
}
