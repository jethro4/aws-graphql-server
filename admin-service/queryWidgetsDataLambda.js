const isArray = require('lodash/isArray');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

const query = async (event, context, callback) => {
  try {
    const queryResponse = await queryWidgetsData(event.arguments);

    // console.log(
    //   'Done queryWidgetsData. queryResponse:',
    //   JSON.stringify(queryResponse.items),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryWidgetsData', err);

    callback(err);
  }
};

module.exports.query = query;
module.exports.transformResponseWidgetsData = transformResponse;

async function queryWidgetsData({
  appName,
  apiKey,
  type,
  range,
  startDate: startDateVal,
  endDate: endDateVal,
  userId,
  queryId,
  itemId,
  page = 0,
  limit = 25,
  q = '',
  filter = '',
  order = 'asc',
  orderBy,
}) {
  const widgetType =
    type === 'MacantaTaskSearch' ? 'TaskWidgets' : 'QueryWidgets';
  let url;
  let startDate = startDateVal;
  let endDate = endDateVal ? `${endDateVal} 23:59` : '';

  switch (range) {
    case 'main': {
      startDate = encodeURIComponent('1 DAY');
      break;
    }
    case '7': {
      startDate = encodeURIComponent(`7 day`);
      break;
    }
    case '30': {
      startDate = encodeURIComponent(`30 day`);
      break;
    }
    case '90': {
      startDate = encodeURIComponent(`90 day`);
      break;
    }
    case 'all': {
      startDate = '2010-01-01';
      break;
    }
  }

  if (widgetType === 'TaskWidgets') {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget?userId=${userId}&widgetType=${widgetType}&startDate=${startDate}&endDate=${endDate}&api_key=${apiKey}&page=${page}&limit=${limit}`;
  } else {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/widget?userId=${userId}&queryId=${queryId}&widgetType=${widgetType}&startDate=${startDate}&endDate=${endDate}&api_key=${apiKey}&page=${page}&limit=${limit}`;
  }

  if (q) {
    url = url.concat(`&q=${q}`);
  }

  if (filter) {
    url = url.concat(`&filter=${Buffer.from(filter).toString('base64')}`);
  }

  if (orderBy) {
    url += `&orderBy=${orderBy}&order=${order.toUpperCase()}`;
  }

  if (itemId) {
    url = url.concat(`&itemId=${itemId}`);
  }

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = transformResponse(json, widgetType);

  return transformedJson;
}

function transformResponse(json, widgetType) {
  const {
    data: {
      NewResults: results,
      TableColumn: tableColumn,
      FieldProperties: contactFieldProperties = {},
      DOFields: doFields = {},
      Total: total,
    },
  } = json;

  const columns = Object.keys(tableColumn);
  const fields =
    widgetType === 'TaskWidgets'
      ? columns.concat('ItemId')
      : columns.concat(['ItemId', 'ContactId', 'EditPermission']);

  const data =
    widgetType === 'TaskWidgets'
      ? results.map((r) => ({
          ...r,
          ...(r.CustomFields &&
            r.CustomFields._DO && {
              ItemId: r.CustomFields._DO.ItemId,
            }),
        }))
      : results;

  return transformWidgetsData(
    data,
    fields,
    Object.keys(contactFieldProperties),
    doFields,
    total,
    widgetType,
  );
}

function transformWidgetsData(
  results,
  fields,
  contactFieldProperties,
  doFields,
  total,
  widgetType,
) {
  const items = [];

  results.forEach((result) => {
    if (widgetType !== 'TaskWidgets' && !result['ContactId']) {
      return;
    }

    const data = [];
    const idPrefix =
      widgetType === 'TaskWidgets'
        ? `${result['Id']}`
        : `${result['ItemId']}-${result['ContactId']}`;

    Object.entries(result).forEach(([label, value]) => {
      if (fields.includes(label)) {
        const rawField = doFields[label];

        if (label === 'EditPermission') {
          data.push({
            id: `${idPrefix}-${label}`,
            fieldKey: label,
            value: value && !value.dataReadOnly ? 'yes' : 'no',
          });
        } else {
          data.push({
            id: `${idPrefix}-${label}`,
            isContactField: contactFieldProperties.includes(label),
            fieldKey: label,
            fieldType: rawField && rawField.fieldType,
            value: String(value),
            choices: transformFieldChoices(
              rawField && rawField['fieldChoices'],
            ),
          });
        }
      }
    });

    items.push({
      data,
      itemId: result['ItemId'],
      contactId: result['ContactId'],
    });
  });

  return {
    items,
    fields,
    total: parseInt(total) || 0,
  };
}

function transformFieldChoices(choices) {
  return (
    (!isArray(choices)
      ? choices?.split(',').filter((choice) => !!choice)
      : choices) || []
  );
}
