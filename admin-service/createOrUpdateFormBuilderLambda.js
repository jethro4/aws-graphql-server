const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateFormBuilder = async (event, context, callback) => {
  try {
    const postResponse = await postFormBuilder(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateFormBuilder. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateFormBuilder', err);
    callback(err);
  }
};

async function postFormBuilder(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    title: params.title,
    description: params.description,
    thankMessage: params.thankMessage,
    type: params.type,
    contactGroups: params.contactGroups.map((group) => ({
      ...group,
      fields: group.fields.map((field) => ({
        id: field.fieldId,
        label: field.label,
        tooltip: field.tooltip,
        required: field.required,
        column: field.column,
      })),
    })),
    dataObject: !params.dataObject
      ? ''
      : {
          id: params.dataObject.dataObjectId,
          dataObjectGroups: params.dataObject.dataObjectGroups.map((group) => ({
            ...group,
            fields: group.fields.map((field) => ({
              id: field.fieldId,
              label: field.label,
              tooltip: field.tooltip,
              required: field.required,
              column: field.column,
              ...(field.type === 'file' && {
                type: field.type,
                ...(field.referenceFieldId
                  ? {
                      referenceFieldId: field.referenceFieldId,
                    }
                  : {
                      multiple: field.multiple,
                      mimeTypes: field.mimeTypes,
                    }),
              }),
            })),
          })),
          relationships: params.dataObject.relationships,
          ...(params.dataObject.integrationField && {
            integrationField: {
              ...params.dataObject.integrationField,
              displayFields:
                params.dataObject.integrationField.displayFields ||
                getDefaultDisplayFields(
                  params.dataObject.integrationField.conditions,
                ),
              conditions: params.dataObject.integrationField.conditions.map(
                (condition) => ({
                  queryCDFieldName: condition.name,
                  queryCDFieldLogic: condition.logic,
                  queryCDFieldOperator: condition.operator,
                  queryCDFieldValues: condition.values,
                  queryCDFieldValue: condition.value,
                }),
              ),
            },
          }),
        },
    ...(params.id && {
      id: params.id,
    }),
    customCSS: params.customCSS
      ? Buffer.from(params.customCSS).toString('base64')
      : '',
    redirectUrl: params.redirectUrl,
    submitLabel: params.submitLabel,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/form_builder`;
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: params.id || json?.data?.[0]?.id || `builder-id-${Date.now()}`,
    title: params.title,
    description: params.description,
    thankMessage: params.thankMessage,
    type: params.type,
    contactGroups: params.contactGroups.map((group) => ({
      ...group,
      fields: group.fields.map((field) => ({
        __typename: 'FormBuilderField',
        fieldId: field.fieldId,
        label: field.label,
        tooltip: field.tooltip,
        required: field.required,
        column: field.column,
      })),
    })),
    dataObject: params.dataObject
      ? {
          ...params.dataObject,
          dataObjectGroups: params.dataObject.dataObjectGroups.map((group) => ({
            ...group,
            fields: group.fields.map((field) => ({
              __typename: 'FormBuilderField',
              fieldId: field.fieldId,
              label: field.label,
              tooltip: field.tooltip,
              required: field.required,
              column: field.column,
              ...(field.type === 'file' && {
                __typename: 'UploadFormBuilderField',
                type: field.type,
                ...(field.referenceFieldId
                  ? {
                      referenceFieldId: field.referenceFieldId,
                    }
                  : {
                      multiple: field.multiple,
                      mimeTypes: field.mimeTypes,
                    }),
              }),
            })),
          })),
        }
      : null,
    customCSS: params.customCSS,
    redirectUrl: params.redirectUrl,
    submitLabel: params.submitLabel,
    jsCode: json?.data?.[0]?.jsCode,
    iFrameCode: json?.data?.[0]?.iFrameCode,
    htmlCode: json?.data?.[0]?.htmlCode,
    link: json?.data?.[0]?.link,
  };
}

function getDefaultDisplayFields(conditions = []) {
  return conditions.map((condition) => `~CD.${condition.name}~`).join(' - ');
}
