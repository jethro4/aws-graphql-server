const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postEmailActionTemplate = async (event, context, callback) => {
  try {
    const postResponse = await sendPostEmailActionTemplate(
      event.arguments.input,
    );
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostSmsAction', err);
    callback(err);
  }
};

async function sendPostEmailActionTemplate({
  appName,
  apiKey,
  sessionName,
  id,
  name,
  compiledEmailHTML,
  emailHTML,
  emailCSS,
  templateHTML,
  templateCSS,
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    Title: name,
    CompiledEmailHTML: compiledEmailHTML,
    EmailHTML: emailHTML,
    EmailCSS: emailCSS,
    TemplateHTML: templateHTML,
    TemplateCSS: templateCSS,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/emailTemplate`;
  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);
  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
