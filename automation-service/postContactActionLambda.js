const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postContactAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostContactAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostContactAction', err);
    callback(err);
  }
};
const actionTypeFields = {
  Task: ['taskTitle', 'taskText', 'taskDate', 'assignType', 'assignTypeValue'],
  Note: ['noteTitle', 'noteTags', 'noteText'],
};
async function sendPostContactAction({
  appName,
  apiKey,
  sessionName,
  id,
  name,
  description,
  doType,
  actionType,
  fieldValues = [],
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    ...actionTypeFields[actionType]?.reduce((acc, item) => {
      acc[item] = fieldValues[item] || '';
      return acc;
    }, {}),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    actionType,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/contactAction/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
