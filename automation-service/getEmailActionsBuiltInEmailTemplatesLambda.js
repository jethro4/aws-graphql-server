const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
module.exports.query = async (event, context, callback) => {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    id: id,
  } = event.request.headers;
  try {
    const response = await getEmailActionsBuiltInEmailTemplates(
      appName,
      apiKey,
      id,
    );
    // console.log('Done query. response:', JSON.stringify(response));
    callback(null, response);
  } catch (err) {
    console.log('Error on query', err);
    callback(err);
  }
};

module.exports.transformResponse = transformResponse;

async function getEmailActionsBuiltInEmailTemplates(appName, apiKey, id) {
  let Id = id || '';
  let APIName = id ? 'builtInEmailTemplate' : 'builtInEmailTemplates';
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/${APIName}/${Id}?access_key=${apiKey}`;
  const response = await http.get(url);
  let json;
  try {
    json = await getJsonResponse(response);
  } catch (err) {
    if (err.status === 403) {
      json = {Error: 'Forbidden'};
    } else {
      throw err;
    }
  }

  return transformResponse(json);
}

function transformResponse(json) {
  return {
    action: json.action || 'get',
    count: json.count || 0,
    status: json.status || false,
    data: json.data || [],
  };
}
