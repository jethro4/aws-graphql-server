const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postWebhookAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostWebhookAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostWebhookAction', err);
    callback(err);
  }
};
async function sendPostWebhookAction({
  appName,
  apiKey,
  sessionName,
  id,
  doType,
  postURL,
  name,
  description,
  fieldKeyValue = [],
  customKeyValue = [],
  contactConditions = [],
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    queryPostURL: postURL,
    queryCDField: fieldKeyValue.map(({fieldName, value = ''}) => ({
      queryCDFieldName: fieldName,
      queryCDFieldValue: value,
    })),
    queryCustomField: customKeyValue.map(({fieldName, value = ''}) => ({
      queryCDFieldName: fieldName,
      queryCDFieldValue: value,
    })),
    queryContact: contactConditions.map(({relationship, logic}) => ({
      queryContactRelationship: relationship,
      queryContactRelationshipFieldLogic: logic,
    })),
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/webhookAction/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
