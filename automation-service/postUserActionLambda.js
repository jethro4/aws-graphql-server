const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postUserAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostUserAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostUserAction', err);
    callback(err);
  }
};

async function sendPostUserAction({
  appName,
  apiKey,
  sessionName,
  id,
  name,
  description,
  doType,
  contactRelationship,
  macantaUserId,
  message,
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    queryContactRelationship: contactRelationship,
    queryMacantaUser: macantaUserId,
    queryUserActionDetails: message,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/userAction/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
