const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
module.exports.query = async (event, context, callback) => {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    id: id,
  } = event.request.headers;
  try {
    const response = await getEmailActionPreview(appName, apiKey, id);

    // console.log('Done query. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on query', err);
    callback(err);
  }
};

module.exports.transformResponse = transformResponse;

async function getEmailActionPreview(appName, apiKey, id) {
  let Id = '&EmailId=' + id || '';
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/emailActionPreview?access_key=${apiKey}${Id}`;
  const response = await http.get(url);
  let json;
  try {
    json = await getJsonResponse(response);
  } catch (err) {
    if (err.status === 403) {
      json = {Error: 'Forbidden'};
    } else {
      throw err;
    }
  }

  return transformResponse(json);
}

function transformResponse(json) {
  return {
    action: json.action || 'get',
    status: json.status || false,
    preview: json.data || [],
  };
}
