const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postSmsAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostSmsAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostSmsAction', err);
    callback(err);
  }
};

async function sendPostSmsAction({
  appName,
  apiKey,
  sessionName,
  id,
  name,
  description,
  phoneField,
  message,
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryPhoneField: phoneField,
    querySMSMessage: message,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/smsAction/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
