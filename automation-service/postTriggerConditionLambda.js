const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postTriggerCondition = async (event, context, callback) => {
  try {
    const postResponse = await sendPostTriggerCondition(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostTriggerCondition', err);
    callback(err);
  }
};

async function sendPostTriggerCondition({
  appName,
  apiKey,
  sessionName,
  id,
  doType,
  name,
  description,
  doConditions = [],
  contactConditions = [],
  userConditions = [],
  fileAttachedByUser,
  fileAttachedByContact,
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    queryCDField: doConditions.map(
      ({fieldName, logic, operator, values, value = ''}) => ({
        queryCDFieldName: fieldName,
        queryCDFieldLogic: logic,
        queryCDFieldOperator: operator,
        ...(values && values.length > 0
          ? {
              queryCDFieldValues: values,
            }
          : {
              queryCDFieldValue: value,
            }),
      }),
    ),
    queryContact: contactConditions.map(({relationship, logic}) => ({
      queryContactRelationship: relationship,
      queryContactRelationshipFieldLogic: logic,
    })),
    queryUser: userConditions.map(
      ({relationship, logic, operator, userId}) => ({
        queryUserRelationship: relationship,
        queryUserRelationshipFieldLogic: logic,
        queryUserOperator: operator,
        queryUserId: userId,
      }),
    ),
    FileAttachedByUser: fileAttachedByUser,
    FileAttachedByContact: fileAttachedByContact,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/triggerCondition/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
