const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postEmailAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostEmailAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostEmailAction', err);
    callback(err);
  }
};

async function sendPostEmailAction({
  appName,
  apiKey,
  sessionName,
  id,
  name,
  description,
  fromAddress,
  fromName,
  subject,
  previewText,
  senderSigRelationship,
  attachment,
  ccEmail,
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryFromAddress: fromAddress,
    queryFromName: fromName,
    querySubject: subject,
    queryPreviewText: previewText,
    querySenderSigRelationship: senderSigRelationship,
    queryAttachment: attachment,
    cc_email: ccEmail,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/emailAction/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
