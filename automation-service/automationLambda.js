const {deleteAutomations} = require('./deleteAutomationsLambda');
const {query: getContactActions} = require('./getContactActionsLambda');
const {
  query: getEmailActionsBuiltInEmailTemplates,
} = require('./getEmailActionsBuiltInEmailTemplatesLambda');
const {query: getEmailActions} = require('./getEmailActionsLambda');
const {
  query: getEmailActionsPreview,
} = require('./getEmailActionsPreviewLambda');
const {
  query: getEmailActionTemplateGroups,
} = require('./getEmailActionTemplateGroupsLambda');
const {query: getFieldActions} = require('./getFieldActionsLambda');
const {query: getSmsActions} = require('./getSmsActionsLambda');
const {query: getTriggerActions} = require('./getTriggerActionsLambda');
const {query: getTriggerConditions} = require('./getTriggerConditionsLambda');
const {query: getUserActions} = require('./getUserActionsLambda');
const {query: getWebhookActions} = require('./getWebhookActionsLambda');
const {postContactAction} = require('./postContactActionLambda');
const {postEmailAction} = require('./postEmailActionLambda');
const {postEmailActionTemplate} = require('./postEmailActionTemplateLambda');
const {postFieldAction} = require('./postFieldActionLambda');
const {postSmsAction} = require('./postSmsActionLambda');
const {postTriggerAction} = require('./postTriggerActionLambda');
const {postTriggerCondition} = require('./postTriggerConditionLambda');
const {postUserAction} = require('./postUserActionLambda');
const {postWebhookAction} = require('./postWebhookActionLambda');

module.exports.automation = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );
  switch (event.info.fieldName) {
    case 'deleteAutomations':
      deleteAutomations(...params);
      break;
    case 'getContactActions':
      getContactActions(...params);
      break;
    case 'getEmailActionsBuiltInEmailTemplates':
      getEmailActionsBuiltInEmailTemplates(...params);
      break;
    case 'getEmailActions':
      getEmailActions(...params);
      break;
    case 'getEmailActionsPreview':
      getEmailActionsPreview(...params);
      break;
    case 'getEmailActionTemplateGroups':
      getEmailActionTemplateGroups(...params);
      break;
    case 'getFieldActions':
      getFieldActions(...params);
      break;
    case 'getSmsActions':
      getSmsActions(...params);
      break;
    case 'getTriggerActions':
      getTriggerActions(...params);
      break;
    case 'getTriggerConditions':
      getTriggerConditions(...params);
      break;
    case 'getUserActions':
      getUserActions(...params);
      break;
    case 'getWebhookActions':
      getWebhookActions(...params);
      break;
    case 'postContactAction':
      postContactAction(...params);
      break;
    case 'postEmailAction':
      postEmailAction(...params);
      break;
    case 'postEmailActionTemplate':
      postEmailActionTemplate(...params);
      break;
    case 'postFieldAction':
      postFieldAction(...params);
      break;
    case 'postSmsAction':
      postSmsAction(...params);
      break;
    case 'postTriggerAction':
      postTriggerAction(...params);
      break;
    case 'postTriggerCondition':
      postTriggerCondition(...params);
      break;
    case 'postUserAction':
      postUserAction(...params);
      break;
    case 'postWebhookAction':
      postWebhookAction(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
