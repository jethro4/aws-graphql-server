const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteAutomations = async (event, context, callback) => {
  try {
    const postResponse = await sendDeleteAutomations(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteFormBuilder. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteFormBuilder', err);
    callback(err);
  }
};

async function sendDeleteAutomations(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/`;

  const response = await http.delete(url, body);

  await getJsonResponse(response);

  return {
    id: params.id,
    success: true,
  };
}
