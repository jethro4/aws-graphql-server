const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postTriggerAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostTriggerAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostTriggerAction', err);
    callback(err);
  }
};

async function sendPostTriggerAction({
  appName,
  apiKey,
  sessionName,
  id,
  doType,
  emailActionId,
  smsActionId,
  fieldActionId,
  contactActionId,
  userActionId,
  webhookActionId,
  name,
  description,
  doConditions = [],
  contactConditions = [],
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    queryConnectedDataEmail: emailActionId,
    queryConnectedDataSMS: smsActionId,
    queryConnectedDataFieldAction: fieldActionId,
    queryConnectedDataContactAction: contactActionId,
    queryConnectedDataUserAction: userActionId,
    queryConnectedDataHTTPPost: webhookActionId,
    queryCDField: doConditions.map(({fieldName, values, value = ''}) => ({
      queryCDFieldName: fieldName,
      ...(values && values.length > 0
        ? {
            queryCDFieldValues: values,
          }
        : {
            queryCDFieldValue: value,
          }),
    })),
    queryContact: contactConditions.map(({relationship, logic}) => ({
      queryContactRelationship: relationship,
      queryContactRelationshipFieldLogic: logic,
    })),
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/triggerAction/`;

  if (id) {
    url += `/${id}`;
  }
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
