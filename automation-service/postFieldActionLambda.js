const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.postFieldAction = async (event, context, callback) => {
  try {
    const postResponse = await sendPostFieldAction(event.arguments.input);
    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sendPostFieldAction', err);
    callback(err);
  }
};
/*
 *  actionType
 *   - Addition : queryCDFieldName1, queryCDFieldName2, queryCDFieldNameResult
 *   - Substraction : queryCDFieldName1, queryCDFieldName2, queryCDFieldNameResult
 *   - Multiplication : queryCDFieldName1, queryCDFieldName2, queryCDFieldNameResult
 *   - Division : queryCDFieldName1, queryCDFieldName2, queryCDFieldNameResult
 *   - TicketNumber : queryTicketNumber, queryTicketNumberPrefix, queryCDFieldNameResult
 *   - ActionCounter : queryCDFieldNameResult
 *   - HOWOLD/HOWLONG : queryCDFieldName1, queryFormatting(Years | Years and Months | Years, Months and Days | Days | Days Number Only), queryCDFieldNameResult
 *   - NextBirthday : queryCDFieldName1, birthdayResultFormat(all php date formats), queryCDFieldNameResult
 *   - TimeDifferenceFields : queryCDFieldName1, queryCDFieldName2, queryCDFieldNameResult
 *   - HumaniseDates : queryCDFieldName1, queryCDFieldNameResult, queryCDFieldNameFormat(d F Y, l d F Y, l)
 *   - ZeroData : queryCDFieldName[]
 *   - NumberDate : dateNumber(e.g. first), dateDay(e.g. Monday), dateMonth(e.g. January), queryCDRequestdate, queryCDFieldNameResult
 *   - AddToDate : dayCount, weekCount, monthCount, yearCount, queryCDRequestdate, queryCDFieldNameResult, queryCDFieldNameFormat(Y-m-d | d F Y | l d F Y | l)
 *   - NextAvailableNumber : queryCDFieldNameResult
 *   - RoundRobin : queryContactListId
 * */
const actionTypeFields = {
  Addition: [
    'queryCDFieldName1',
    'queryCDFieldName2',
    'queryCDFieldNameResult',
  ],
  Substraction: [
    'queryCDFieldName1',
    'queryCDFieldName2',
    'queryCDFieldNameResult',
  ],
  Multiplication: [
    'queryCDFieldName1',
    'queryCDFieldName2',
    'queryCDFieldNameResult',
  ],
  Division: [
    'queryCDFieldName1',
    'queryCDFieldName2',
    'queryCDFieldNameResult',
  ],
  TicketNumber: [
    'queryTicketNumber',
    'queryTicketNumberPrefix',
    'queryCDFieldNameResult',
  ],
  ActionCounter: ['queryCDFieldNameResult'],
  'HOWOLD/HOWLONG': [
    'queryCDFieldName1',
    'queryFormatting',
    'queryCDFieldNameResult',
  ],
  NextBirthday: [
    'queryCDFieldName1',
    'birthdayResultFormat',
    'queryCDFieldNameResult',
  ],
  TimeDifferenceFields: [
    'queryCDFieldName1',
    'queryCDFieldName2',
    'queryCDFieldNameResult',
  ],
  HumaniseDates: [
    'queryCDFieldName1',
    'queryCDFieldNameResult',
    'queryCDFieldNameFormat',
  ],
  ZeroData: ['queryCDFieldName'],
  NumberDate: [
    'dateNumber',
    'dateDay',
    'dateMonth',
    'queryCDRequestdate',
    'queryCDFieldNameResult',
  ],
  AddToDate: [
    'dayCount',
    'weekCount',
    'monthCount',
    'yearCount',
    'queryCDRequestdate',
    'queryCDFieldNameResult',
    'queryCDFieldNameFormat',
  ],
  NextAvailableNumber: ['queryCDFieldNameResult'],
  RoundRobin: ['queryContactListId'],
};
async function sendPostFieldAction({
  appName,
  apiKey,
  sessionName,
  id,
  name,
  description,
  doType,
  actionType,
  fieldValues = [],
  contactRelationship,
}) {
  const body = {
    api_key: apiKey,
    sessionName: sessionName,
    ...(id && {
      queryId: id,
    }),
    queryName: name,
    queryDescription: description,
    queryConnectedDataType: doType,
    actionType,
    ...actionTypeFields[actionType]?.reduce((acc, item) => {
      acc[item] = fieldValues[item] || '';
      return acc;
    }, {}),
    queryContactRelationship: contactRelationship,
  };
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/automation/fieldAction/`;

  if (id) {
    url += `/${id}`;
  }
  console.log('body:');
  console.log(body);
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: id || (json && json.queryId),
    name,
    action: json.action,
    data: json.data,
    status: json.status,
  };
}
