const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformResponse} = require('./createOrUpdatePermissionTemplateLambda');

module.exports.duplicatePermissionTemplate = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await duplicatePermissionTemplateFunc(
      event.request.headers,
      event.arguments.input,
    );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on duplicatePermissionTemplate', err);

    callback(err);
  }
};

async function duplicatePermissionTemplateFunc(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/template/duplicate`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}
