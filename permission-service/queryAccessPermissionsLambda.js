const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const {convertObjectToArrayWithValue} = require('../utils/array');
const getJsonResponse = require('../utils/getJsonResponse');
const getAccessLevel = require('../utils/getAccessLevel');
const {parseJson} = require('../utils/string');

module.exports.query = async (event, context, callback) => {
  const {appName, apiKey, email} = event.arguments;

  try {
    const response = await queryAccessPermissions(appName, apiKey, email);

    // console.log('Done query. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on query', err);
    callback(err);
  }
};

module.exports.transformResponse = transformResponse;
module.exports.transformPermissions = transformPermissions;

async function queryAccessPermissions(appName, apiKey, email) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user_info/?access_key=${apiKey}&Email=${email}`;

  const response = await http.get(url);

  let json;

  try {
    json = await getJsonResponse(response);
  } catch (err) {
    if (err.status === 403) {
      json = {Permission: {}};
    } else {
      throw err;
    }
  }

  return transformResponse(json);
}

function transformResponse(json) {
  const {
    Permission: {Level: level, Access: access = {}},
  } = json;

  const permission = {
    isAdmin: level === 'administrator',
    isUser: level === 'user',
    isNonUser: !level,

    ...transformPermissions(access),
  };

  return permission;
}

function transformPermissions(permissions) {
  const {
    PermissionMeta: permissionMetaProp,
    TabAccess: tabs,
    SectionAccess: sections,
    MQBAccess: mqbs,
    MQBWidgetAccess: mqbWidgetAccess,
    MQBWidgetEmail: mqbWidgetEmail,
    TaskAccess: tasks,
    ContactViewPermissions: contacts,
    UserGuides: userGuides,
    WorkflowBoards: workflowBoards,
    InlineEmailTemplateAccess: emailTemplates,
  } = permissions;

  const permissionMeta = permissionMetaProp
    ? parseJson(permissionMetaProp)
    : {};
  const userDOEditPermission = permissionMeta.UserDOEditPermission;
  const taskPermissions = tasks
    ? convertObjectToArrayWithValue(parseJson(tasks)).map((access) => ({
        taskId: access.id,
        allow: access.value === 'yes',
        specificIds: access.id === 'UserTask' ? access.value : [],
      }))
    : [];

  return {
    permissionMeta: {
      allowUserAttachment: permissionMeta.AllowUserAttachment === 'yes',
      allowUserDeleteContact: permissionMeta.AllowUserDeleteContact === 'yes',
      allowUserDeleteDOAttachment:
        permissionMeta.AllowUserDeleteDOAttachment === 'yes',
      allowUserDeleteDOItem: permissionMeta.AllowUserDeleteDOItem === 'yes',
      allowUserToMergeContacts:
        permissionMeta.AllowUserToMergeContacts === 'yes',
      allowEmailSync: permissionMeta.AllowEmailSync === 'yes',
      allowAddRemoveEmailDomainFilter:
        permissionMeta.AllowAddRemoveEmailDomainFilter === 'yes',
      allowContactExport: permissionMeta.AllowContactExport === 'yes',
      allowDataObjectExport: permissionMeta.AllowDataObjectExport === 'yes',
      allowNoteTaskExport: permissionMeta.AllowNoteTaskExport === 'yes',
      allowAddDirectRelationship:
        permissionMeta.AllowAddDirectRelationship === 'yes',
      userDOEditPermission: userDOEditPermission && {
        otherUserRelationship: Array.isArray(
          userDOEditPermission.OtherUserRelationship,
        )
          ? userDOEditPermission.OtherUserRelationship.filter((item) => !!item)
          : [],
        withOtherUserDataToggle:
          userDOEditPermission.WithOtherUserDataToggle === 'yes',
        withOtherUserRelationshipDataToggle:
          userDOEditPermission.WithOtherUserRelationshipDataToggle === 'yes',
        withOtherUserRelationshipRelationshipToggle:
          userDOEditPermission.WithOtherUserRelationshipRelationshipToggle ===
          'yes',
        withOtherUserRelationshipToggle:
          userDOEditPermission.WithOtherUserRelationshipToggle === 'yes',
      },
      hideActionButtonsForGroups: permissionMeta.ActionButton
        ? convertObjectToArrayWithValue(permissionMeta.ActionButton).reduce(
            (acc, access) => {
              if (access.value === 'Hidden') {
                return acc.concat(access.id);
              }

              return acc;
            },
            [],
          )
        : [],
      hideConnectButtonsForGroups: permissionMeta.ConnectButton
        ? convertObjectToArrayWithValue(permissionMeta.ConnectButton).reduce(
            (acc, access) => {
              if (access.value === 'Hidden') {
                return acc.concat(access.id);
              }

              return acc;
            },
            [],
          )
        : [],
      hidePurchaseButton: permissionMeta.PurchaseButton === 'Hidden',
    },
    tabs: tabs
      ? convertObjectToArrayWithValue(parseJson(tabs)).map((tab) => ({
          groupId: tab.id,
          permission: getAccessLevel(tab.value),
        }))
      : [],
    sections: sections
      ? convertObjectToArrayWithValue(parseJson(sections)).map((access) => ({
          groupId: access.id,
          access: convertObjectToArrayWithValue(access.value).map(
            (section) => ({
              name: section.id,
              permission: getAccessLevel(section.value),
            }),
          ),
        }))
      : [],
    mqbs: mqbs
      ? convertObjectToArrayWithValue(parseJson(mqbs)).map((access) => ({
          queryId: access.id,
          permission: getAccessLevel(access.value),
        }))
      : [],
    mqbWidgetAccess: mqbWidgetAccess
      ? convertObjectToArrayWithValue(parseJson(mqbWidgetAccess)).map(
          (access) => ({
            queryId: access.id,
            show: access.value === 'checked',
          }),
        )
      : [],
    mqbWidgetEmail: mqbWidgetEmail
      ? convertObjectToArrayWithValue(parseJson(mqbWidgetEmail)).map(
          (access) => ({
            queryId: access.id,
            show: access.value === 'checked',
          }),
        )
      : [],
    tasks: taskPermissions.reduce((acc, item) => {
      if (item.taskId === 'SpecificUserTask' && item.allow) {
        const userTask = taskPermissions.find((a) => a.taskId === 'UserTask');

        return acc.concat({
          ...item,
          specificIds: userTask.specificIds,
        });
      } else if (item.taskId !== 'UserTask') {
        return acc.concat(item);
      }

      return acc;
    }, []),
    contacts: contacts
      ? convertObjectToArrayWithValue(parseJson(contacts)).map((access) => ({
          groupId: access.id,
          access: convertObjectToArrayWithValue(
            access.id === 'Global'
              ? {TypeA: access.value, TypeB: access.value, TypeC: access.value}
              : access.value,
          )
            .map((doItem) => ({
              ...doItem,
              value:
                typeof doItem.value === 'object'
                  ? doItem.value.Access
                  : doItem.value,
              relationship: doItem.value?.Relationship || '',
            }))
            .map((doItem) => ({
              name: doItem.id,
              permission: getAccessLevel(doItem.value),
              relationship: doItem.relationship,
            })),
        }))
      : [],
    userGuides: userGuides
      ? convertObjectToArrayWithValue(parseJson(userGuides)).reduce(
          (acc, type) => {
            convertObjectToArrayWithValue(type.value).map((subType) => {
              if (typeof subType.value === 'object') {
                Object.entries(subType.value).forEach(([key, value]) => {
                  acc.push({
                    guideId: `${type.id}-${subType.id}-${key}`,
                    type: type.id,
                    value,
                    subType: key,
                    meta: [
                      {
                        name: 'doType',
                        value: subType.id,
                      },
                    ],
                  });
                });
              } else {
                acc.push({
                  guideId: `${type.id}-${subType.id}`,
                  type: type.id,
                  value: subType.value,
                  subType: subType.id,
                  meta: [],
                });
              }
            });

            return acc;
          },
          [],
        )
      : [],
    emailTemplates: emailTemplates
      ? convertObjectToArrayWithValue(parseJson(emailTemplates)).map(
          (access) => ({
            templateGroupId: access.id,
            name: access.value,
          }),
        )
      : [],
    workflowBoards: workflowBoards
      ? convertObjectToArrayWithValue(parseJson(workflowBoards)).map(
          (board) => ({
            boardId: board.id,
            scope: board.value,
          }),
        )
      : [],
  };
}
