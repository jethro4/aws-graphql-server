const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformPermissions} = require('./queryAccessPermissionsLambda');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryPermissionTemplates(event.request.headers);

    // console.log(
    //   'Done queryPermissionTemplates. response:',
    //   JSON.stringify(response),
    // );

    callback(null, response);
  } catch (err) {
    console.log('Error on queryPermissionTemplates', err);
    callback(err);
  }
};

async function queryPermissionTemplates(headers) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/templates?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = []} = json;

  let templates = [];

  data.forEach((template) => {
    const {
      TemplateId: id,
      TemplateName: name,
      TemplateDescription: description,
      Status: status,
    } = template;

    templates.push({
      id,
      name,
      description,
      status,
      permission: transformPermissions(template),
    });
  });

  return templates;
}
