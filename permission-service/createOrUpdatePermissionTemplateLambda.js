const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformPermissions} = require('./queryAccessPermissionsLambda');

module.exports.createOrUpdatePermissionTemplate = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postPermissionTemplate(
      event.request.headers,
      event.arguments.input,
    );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdatePermissionTemplate', err);

    callback(err);
  }
};

module.exports.transformResponse = transformResponse;

async function postPermissionTemplate(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    status: 'Active',
    ...(params.id && {
      id: params.id,
    }),
    ...(params.name && {
      templateName: params.name,
    }),
    ...(params.description && {
      templateDescription: params.description,
    }),
    ...params.data?.reduce((acc, d) => {
      const accObj = {...acc};
      const value = JSON.parse(d.jsonValue);
      let key;

      switch (d.key) {
        case 'mqbAccess': {
          key = 'MQBAccess';

          break;
        }
        case 'mqbWidgetAccess': {
          key = 'MQBWidgetAccess';

          break;
        }
        case 'mqbWidgetEmail': {
          key = 'MQBWidgetEmail';

          break;
        }
        default: {
          key = d.key;
        }
      }

      accObj[key] = value;

      return accObj;
    }, {}),
  };

  const url = `https://${appName}.${
    envConfig.apiDomain
  }/rest/v1/user/template/${!params.id ? 'add' : 'edit'}`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {
    id,
    data: {
      TemplateName: name,
      TemplateDescription: description,
      Status: status,
      ...template
    },
  } = json;

  return {
    id,
    name,
    description,
    status,
    permission: transformPermissions(template),
  };
}
