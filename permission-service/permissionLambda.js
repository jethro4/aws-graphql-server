const {
  query: queryAccessPermissions,
} = require('./queryAccessPermissionsLambda');
const {
  query: queryPermissionTemplates,
} = require('./queryPermissionTemplatesLambda');
const {
  createOrUpdatePermissionTemplate,
} = require('./createOrUpdatePermissionTemplateLambda');
const {
  duplicatePermissionTemplate,
} = require('./duplicatePermissionTemplateLambda');
const {deletePermissionTemplate} = require('./deletePermissionTemplateLambda');

module.exports.permission = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'getAccessPermissions':
      queryAccessPermissions(...params);
      break;
    case 'listPermissionTemplates':
      queryPermissionTemplates(...params);
      break;
    case 'createOrUpdatePermissionTemplate':
      createOrUpdatePermissionTemplate(...params);
      break;
    case 'duplicatePermissionTemplate':
      duplicatePermissionTemplate(...params);
      break;
    case 'deletePermissionTemplate':
      deletePermissionTemplate(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
