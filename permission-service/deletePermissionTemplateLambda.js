const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deletePermissionTemplate = async (event, context, callback) => {
  try {
    const deleteResponse = await deletePermissionTemplateFunc(
      event.request.headers,
      event.arguments.input,
    );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deletePermissionTemplate', err);

    callback(err);
  }
};

async function deletePermissionTemplateFunc(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/template/delete`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {
    id: params.id,
    success: true,
  };
}
