const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteDataObjectType = async (event, context, callback) => {
  try {
    const postResponse = await deleteDataObjectTypeFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteDataObjectType. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteDataObjectType', err);
    callback(err);
  }
};

async function deleteDataObjectTypeFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    Id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/type`;

  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  return {
    success: json.Status,
    groupId: params.id,
  };
}
