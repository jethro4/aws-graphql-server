const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {
  getAccessPermissions,
  filterByAccess,
} = require('../utils/accessFilters');

module.exports.query = async (event, context, callback) => {
  try {
    let queryResponse = await queryDataObjectTypes(event.arguments);

    if (event.arguments.userEmail) {
      queryResponse = await filterDataObjectTypesWithPermissions(
        queryResponse,
        event.arguments,
      );
    }

    // console.log(
    //   'Done queryDataObjectTypes. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDataObjectTypes', err);

    callback(err);
  }
};

module.exports.queryDataObjectTypes = queryDataObjectTypes;

async function queryDataObjectTypes(query) {
  const {appName, apiKey, includeContactObject} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/types?access_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const dataObjectTypes = transformResponse(json, includeContactObject);

  return dataObjectTypes;
}

async function filterDataObjectTypesWithPermissions(dataObjectTypes, query) {
  const {appName, apiKey, userEmail: email} = query;

  const accessPermissions = await getAccessPermissions({
    appName,
    apiKey,
    email,
  });

  if (accessPermissions.isAdmin) {
    return dataObjectTypes;
  } else {
    const permittedDOTypes = filterByAccess(accessPermissions, null, 'tab');

    const filteredDataObjectTypes = dataObjectTypes.filter((type) => {
      return permittedDOTypes.some((doType) => type.id === doType.groupId);
    });

    return filteredDataObjectTypes;
  }
}

function transformResponse(json, includeContactObject) {
  const dataObjectTypes = [];

  for (var dataObjectType of json) {
    if (dataObjectType.id !== 'co_customfields' || includeContactObject) {
      const dataObjectTypeObj = {
        id: dataObjectType.id,
        title: dataObjectType.title,
      };

      dataObjectTypes.push(dataObjectTypeObj);
    }
  }

  return dataObjectTypes;
}
