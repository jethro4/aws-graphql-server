const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateDONote = async (event, context, callback) => {
  const {
    appName,
    apiKey,
    id: noteId,
    doItemId,
    sessionName,
    ...note
  } = event.arguments.input;

  try {
    const postResponse = await postNote(
      appName,
      apiKey,
      doItemId,
      sessionName,
      noteId,
      note,
    );

    // console.log(
    //   'Done createOrUpdateDONote. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateDONote', err);
    callback(err);
  }
};

async function postNote(appName, apiKey, doItemId, sessionName, noteId, note) {
  const body = {
    access_key: apiKey,
    item_id: doItemId,
    note_title: note.title,
    note_type: note.type || 'General', //TODO: should be adjusted in api to remove requirement for 'type' attribute
    rich_text_notes: '',
    plain_text_notes: note.note,
    tags: note.tags && note.tags.map((tag) => `#${tag}`).join(','),
    ...(noteId && {note_id: noteId}),
    sessionName,
  };

  const url = `https://${appName}.${
    envConfig.apiDomain
  }/rest/v1/data_object/note/${!noteId ? 'add' : 'edit'}`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return json;
}
