const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {queryDataObjectTypes} = require('./queryDataObjectTypesLambda');
const {convertObjectToArray, sortArrayByObjectKey} = require('../utils/array');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDataObjects(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryDataObjects. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDataObjects', err);

    callback(err);
  }
};

module.exports.convertDOItemValue = convertDOItemValue;

module.exports.getContactSpecificValues = getContactSpecificValues;

async function queryDataObjects(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const {
    groupId = '',
    contactId,
    relationship,
    page = 0,
    limit,
    contactField,
    q = '',
    filter,
    order = 'asc',
    orderBy,
  } = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/search/data_object?api_key=${apiKey}&sessionName=${sessionName}`;

  if (groupId) {
    url = url.concat(`&group_id=${groupId}`);
  }

  if (contactId) {
    url = url.concat(`&contactId=${contactId}`);
  }

  if (relationship) {
    url = url.concat(`&relationship=${relationship}`);
  }

  if (limit) {
    url += `&page=${page}&limit=${limit}`;
  }

  if (contactField) {
    url = url.concat(`&contactField=true`);
  }

  if (q) {
    url = url.concat(`&q=${q}`);
  }

  if (filter) {
    url = url.concat(`&filter=${Buffer.from(filter).toString('base64')}`);
  }

  if (orderBy) {
    url += `&sort_by=${orderBy}&sort=${order.toLowerCase()}`;
  }

  const response = await http.get(encodeURI(url));

  const json = await getJsonResponse(response);

  const doTypes = await queryDataObjectTypes({
    appName,
    apiKey,
  });

  const dataObjectItems = transformResponse(json, doTypes, orderBy);

  return dataObjectItems;
}

function transformResponse(json, doTypes, orderBy) {
  const {total = 0, data = {}} = json;
  const doSearchItems = [];

  Object.entries(data).map(([groupId, itemsObj]) => {
    if (groupId === 'co_customfields') {
      return;
    }

    const {title} = doTypes.find((type) => type.id === groupId);

    const doData = {
      groupId,
      type: title,
      items: [],
    };

    const itemsData = convertObjectToArray(itemsObj);
    const sortedItemsData = orderBy
      ? itemsData
      : sortArrayByObjectKey(itemsData, 'created', 'desc');

    sortedItemsData.map(({id, value = {}, connected_contact}) => {
      const itemValues = [];

      Object.entries(value).map(([fieldId, fieldValue]) => {
        if (fieldId) {
          itemValues.push({
            id: `${id}-${fieldId}`,
            fieldId,
            value: convertDOItemValue(fieldValue),
            contactSpecificValues: getContactSpecificValues(fieldValue),
          });
        }
      });

      const connectedContacts = convertObjectToArray(connected_contact).map(
        (contact) => {
          const {
            id: contactId,
            FirstName: firstName,
            LastName: lastName,
            Email: email,
            relationships = [],
          } = contact;
          const relationshipsArr = [];

          relationships.forEach((relId) => {
            relationshipsArr.push({
              id: relId,
            });
          });

          return {
            contactId,
            firstName,
            lastName,
            email,
            relationships: relationshipsArr,
          };
        },
      );

      doData.items.push({
        id,
        data: itemValues,
        connectedContacts,
      });
    });

    doSearchItems.push(doData);
  });

  return {total: parseInt(total), items: doSearchItems};
}

function convertDOItemValue(value) {
  if (isContactSpecificValue(value)) {
    return '';
  }

  return value;
}

function getContactSpecificValues(value) {
  let refValues = [];

  if (isContactSpecificValue(value)) {
    Object.entries(value).forEach(([key, value]) => {
      const contactId = key.split('id_')[1];
      if (contactId) {
        refValues.push({
          contactId,
          value,
        });
      }
    });
  }

  return refValues;
}

function isContactSpecificValue(value) {
  return typeof value === 'object' && !Array.isArray(value);
}
