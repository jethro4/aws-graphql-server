const isArray = require('lodash/isArray');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {
  getAccessPermissions,
  filterByAccess,
  ACCESS_LEVELS,
} = require('../utils/accessFilters');
const {sortArrayByPriority} = require('../utils/array');

module.exports.query = async (event, context, callback) => {
  try {
    const args = {
      ...event.arguments,
      groupId:
        event.arguments.groupId === 'contactDefaultFields' ||
        event.arguments.groupId === 'contactCustomFields'
          ? 'co_customfields'
          : event.arguments.groupId,
    };
    let queryResponse = await queryDataObjectFields(args);

    if (
      event.arguments.groupId === 'contactDefaultFields' ||
      event.arguments.groupId === 'contactCustomFields'
    ) {
      queryResponse = await filterContactFields(
        queryResponse,
        event.arguments,
        event.arguments.groupId,
      );
    } else if (event.arguments.userEmail) {
      queryResponse = await filterDataObjectFieldsWithPermissions(
        queryResponse,
        event.arguments,
      );
    } else {
      queryResponse = transformWithPermission(queryResponse, true);
    }

    queryResponse = transformChoices(queryResponse);

    // console.log(
    //   'Done queryDataObjectFields. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDataObjectFields', err);

    callback(err);
  }
};

module.exports.queryDataObjectFields = queryDataObjectFields;
module.exports.filterContactCustomFields = filterContactCustomFields;

async function queryDataObjectFields(query) {
  const {appName, apiKey, groupId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/fields?type=${groupId}&access_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  // Separated API call because old data doesn't have sections sorted
  const sortedFieldsIds = await querySortedFieldIds(query);

  const dataObjectFields = transformResponse(json, sortedFieldsIds);

  return dataObjectFields;
}

async function querySortedFieldIds(query) {
  const {appName, apiKey, groupId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/types/orderBySection?groupId=${groupId}&access_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const sortedFieldIds = transformSortedFieldIdsResponse(json);

  return sortedFieldIds;
}

async function filterDataObjectFieldsWithPermissions(dataObjectFields, query) {
  const {appName, apiKey, groupId, userEmail: email} = query;

  const accessPermissions = await getAccessPermissions({
    appName,
    apiKey,
    email,
  });

  if (accessPermissions.isAdmin) {
    return transformWithPermission(dataObjectFields, true);
  } else {
    const permittedSections = filterByAccess(
      accessPermissions,
      {
        groupId,
      },
      'section',
    );

    const permittedDOTypes = filterByAccess(accessPermissions, null, 'tab');
    const permittedTab = permittedDOTypes.find(
      (tab) => tab.groupId === groupId,
    );

    return transformWithPermission(
      dataObjectFields,
      false,
      permittedSections,
      permittedTab,
    );
  }
}

async function filterContactFields(dataObjectFields, query, type) {
  const {appName, apiKey, userEmail: email} = query;

  if (type === 'contactDefaultFields') {
    return filterContactDefaultFields(dataObjectFields);
  } else if (!email) {
    return filterContactCustomFields(dataObjectFields);
  } else {
    const accessPermissions = await getAccessPermissions({
      appName,
      apiKey,
      email,
    });

    const filteredContactFields = dataObjectFields.filter((field) => {
      return (
        field.fieldApplicableTo === 'All' ||
        (field.fieldApplicableTo === 'Admin' && accessPermissions.isAdmin) ||
        (field.fieldApplicableTo === 'User' && accessPermissions.isUser) ||
        (field.fieldApplicableTo === 'Contact' &&
          accessPermissions.isNonUser) ||
        (field.fieldApplicableTo === 'Both' &&
          (accessPermissions.isUser || accessPermissions.isNonUser))
      );
    });

    return filterContactCustomFields(filteredContactFields);
  }
}

function filterContactDefaultFields(contactFields) {
  return filterContactCustomFields(contactFields, true);
}

function filterContactCustomFields(fields, isDefaultFields) {
  const customFields = fields.filter((f) => {
    const isCustomField =
      f.name !== 'Id' &&
      f.name !== 'Email' &&
      f.name !== 'EmailAddress2' &&
      f.name !== 'EmailAddress3' &&
      f.name !== 'FirstName' &&
      f.name !== 'MiddleName' &&
      f.name !== 'LastName' &&
      f.name !== 'Birthday' &&
      f.name !== 'Title' &&
      f.name !== 'Company' &&
      f.name !== 'Website' &&
      f.name !== 'JobTitle' &&
      f.name !== 'StreetAddress1' &&
      f.name !== 'StreetAddress2' &&
      f.name !== 'City' &&
      f.name !== 'State' &&
      f.name !== 'PostalCode' &&
      f.name !== 'Country' &&
      f.name !== 'Address2Street1' &&
      f.name !== 'Address2Street2' &&
      f.name !== 'City2' &&
      f.name !== 'State2' &&
      f.name !== 'PostalCode2' &&
      f.name !== 'Country2' &&
      f.name !== 'DateCreated' &&
      f.name !== 'Phone1' &&
      f.name !== 'Phone2' &&
      f.name !== 'Phone3' &&
      f.name !== 'Phone4' &&
      f.name !== 'Phone5' &&
      f.name !== 'Phone1Ext' &&
      f.name !== 'Phone2Ext' &&
      f.name !== 'Phone3Ext' &&
      f.name !== 'Phone4Ext' &&
      f.name !== 'Phone5Ext';

    return !isDefaultFields ? isCustomField : !isCustomField;
  });

  return customFields;
}

function transformResponse(fields, sortedFieldsIds) {
  const transformedFields = fields.map(
    ({
      key: name,
      fieldType: type,
      fieldSubGroup: subGroup,
      sectionTagId: sectionTag = '',
      showInTable,
      showOrder,
      contactspecificField,
      placeHolder,
      helperText: helpText,
      defaultValue,
      fieldHeadingText: headingText,
      addDivider,
      ...field
    }) => {
      return {
        ...field,
        name,
        type,
        subGroup,
        sectionTag,
        showInTable: showInTable === 'yes',
        showOrder: Number(showOrder),
        contactSpecificField: contactspecificField === 'yes',
        placeholder: placeHolder,
        helpText,
        default: defaultValue,
        headingText,
        addDivider: addDivider === 'yes',
        ...(field.fileUpload &&
          type === 'Select' && {
            type: 'FileUpload',
          }),
      };
    },
  );

  const filteredFields = transformedFields.filter((f) => f && !!f.id);

  return sortedFieldsIds?.length
    ? sortArrayByPriority(filteredFields, 'id', sortedFieldsIds)
    : filteredFields;
}

function transformWithPermission(
  dataObjectFields,
  isAdmin,
  permittedSections,
  permittedTab,
) {
  return dataObjectFields.map((field) => {
    const transformedField = {
      ...field,
    };

    if (isAdmin) {
      transformedField.permission = ACCESS_LEVELS.READ_WRITE;
    } else {
      const fieldSection = permittedSections.find(
        (section) => field.sectionTag === section.name,
      );

      if (
        !fieldSection ||
        fieldSection.permission === ACCESS_LEVELS.NO_ACCESS
      ) {
        transformedField.permission = permittedTab
          ? permittedTab.permission
          : ACCESS_LEVELS.READ_ONLY;
      } else {
        transformedField.permission = fieldSection.permission;
      }
    }

    return transformedField;
  });
}

function transformChoices(fields) {
  return fields.map(({choices, fieldChoices, ...f}) => ({
    ...f,
    ...(hasChoices(f.type) && {
      choices: choices || fieldChoices,
    }),
  }));
}

function transformSortedFieldIdsResponse(response) {
  const type = response[0];
  const sections = type?.sections;

  if (sections && !isArray(sections)) {
    const fieldIds = [];
    const sectionsArr = Object.values(sections);

    sectionsArr.forEach((section) => {
      const subSectionsArr = Object.values(section.subSections);

      subSectionsArr.forEach((subSection) => {
        subSection.fields
          .filter((f) => !!f?.Id)
          .forEach((f) => {
            fieldIds.push(f.Id);
          });
      });
    });

    return fieldIds;
  }

  return null;
}

function hasChoices(fieldType) {
  return ['Select', 'Radio', 'Checkbox', 'FileUpload'].includes(fieldType);
}
