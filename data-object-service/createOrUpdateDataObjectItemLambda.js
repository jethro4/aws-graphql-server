const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateDataObjectItem = async (
  event,
  context,
  callback,
) => {
  const {appName, apiKey, doItemId: id, ...dataObject} = event.arguments.input;

  try {
    const postResponse = await postDataObjectItem(
      appName,
      apiKey,
      id,
      dataObject,
    );

    // console.log(
    //   'Done createOrUpdateDataObjectItem. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateDataObjectItem', err);
    callback(err);
  }
};

async function postDataObjectItem(appName, apiKey, id, dataObject) {
  let relationship = '';

  if (dataObject.relationship) {
    relationship = dataObject.relationship;
  } else if (dataObject.relationships) {
    relationship = dataObject.relationships
      .map((relationship) => relationship.trim())
      .join(',');
  }

  const body = {
    access_key: apiKey,
    sessionName: dataObject.sessionName,
    connected_group: dataObject.groupId,
    cd_guid: id,
    contactId: dataObject.contactId,
    relationship,
    accept_macanta_default: 'yes',
    ...(dataObject.fields &&
      dataObject.fields.reduce(function (prev, curr) {
        prev[curr.key] = curr.value;
        return prev;
      }, {})),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/${
    id ? 'edit' : 'add'
  }`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  const itemId = json.id || json.NormalForm.ItemInfo.Id;

  return {
    id: itemId,
    success: true,
  };
}
