const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateDataObjectType = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postDataObjectType(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateDataObjectType. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateDataObjectType', err);
    callback(err);
  }
};

async function postDataObjectType(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    dataObjectName: params.name,
    fieldSections: params.fieldSections.map((section) => ({
      sectionName: section.name,
      position: section.position,
      subSections: section.subSections.map((subSection) => ({
        subSectionName: subSection.name,
        position: subSection.position,
        fields: subSection.fields.map((field) => ({
          fieldId: field.id,
          fieldName: field.name,
          fieldType: field.type,
          helperText: field.helpText || '',
          placeHolder: field.placeholder || '',
          defaultValue: field.default || '',
          requiredField: field.required ? 'yes' : 'no',
          showInTable: field.showInTable ? 'yes' : 'no',
          showOrder: field.showOrder,
          fieldChoices: field.choices,
          contactSpecificField: field.contactSpecificField ? 'yes' : 'no',
          fieldHeadingText: field.headingText || '',
          addDivider: field.addDivider ? 'yes' : 'no',
          useAsPrimaryKey: 'no',
          ...(field.fieldApplicableTo && {
            fieldApplicableTo: field.fieldApplicableTo,
          }),
          ...(field.type === 'FileUpload' && {
            fieldType: 'Select',
            fileUpload: field.fileUpload,
          }),
        })),
      })),
    })),
    ...(params.relationships && {
      relationships: params.relationships.map((relationship) => ({
        relationshipName: relationship.role,
        exclusive: relationship.exclusive,
        limit: relationship.limit,
        autoAssignLoggedInUser: relationship.autoAssignLoggedInUser
          ? 'yes'
          : 'no',
        autoAssignContact: relationship.autoAssignContact ? 'yes' : 'no',
      })),
    }),
    ...(params.id && {
      dataObjectId: params.id,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/type`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return {
    id: json.Id || params.id,
    title: params.name,
  };
}
