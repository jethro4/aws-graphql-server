const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteDataObjectItem = async (event, context, callback) => {
  try {
    const postResponse = await deleteDataObjectItemFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteDataObjectItem. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteDataObjectItem', err);
    callback(err);
  }
};

async function deleteDataObjectItemFunc(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {Id: params.id, api_key: apiKey};

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object`;

  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  if (!json.Status) {
    throw new Error('Delete failed');
  }

  return {
    success: json.Status,
    id: params.id,
  };
}
