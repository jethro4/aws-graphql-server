const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const {
  sortArrayByObjectKey,
  convertObjectToArray,
  tagsToArray,
} = require('../utils/array');
const getJsonResponse = require('../utils/getJsonResponse');
const {
  convertDOItemValue,
  getContactSpecificValues,
} = require('./queryDataObjectsLambda');
const {parseJson} = require('../utils/string');

module.exports.query = async (event, context, callback) => {
  const {appName, ...query} = event.arguments;

  try {
    const queryResponse = await queryDataObjectItem(appName, query);

    // console.log(
    //   'Done queryDataObjectItem. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDataObjectItem', err);

    callback(err);
  }
};

module.exports.queryDataObjectItem = queryDataObjectItem;

async function queryDataObjectItem(appName, query) {
  const {apiKey, itemId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/item?access_key=${apiKey}&item=${itemId}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const groupId = json.group;

  const dataObjectItem = transformResponse(json, groupId);

  return dataObjectItem;
}

function transformResponse(json, groupId) {
  const {
    id,
    value: data,
    attachments,
    notes,
    meta,
    connected_contact: connected_contacts,
  } = json;

  const transformedNotes = notes.map((note) => ({
    ...note,
    CreationDate: note.CreationDate && JSON.parse(note.CreationDate),
    LastUpdated: note.LastUpdated && JSON.parse(note.LastUpdated),
  }));

  return transformDOItem({
    id,
    groupId,
    data: data ? transformDOData(JSON.parse(data)) : {},
    attachments: transformAttachments(attachments),
    notes: sortArrayByObjectKey(transformedNotes, 'CreationDate.date', 'desc'),
    meta: parseJson(meta, {}),
    connected_contacts: connected_contacts
      ? transformDOConnectedContacts(JSON.parse(connected_contacts))
      : {},
  });
}

function transformDOData(data) {
  let transformedData = {};

  for (var key in data) {
    const value = data[key];

    const itemObj = {
      id: key,
      value,
    };

    transformedData[key] = itemObj;
  }

  return transformedData;
}

function transformAttachments(data = []) {
  return data.map(
    ({id, filename: fileName, thumbnail, download_url: downloadUrl, meta}) => ({
      id,
      fileName,
      thumbnail,
      downloadUrl,
      meta: parseJson(meta, {}),
    }),
  );
}

function transformDOConnectedContacts(data) {
  let transformedData = {};

  for (var key in data) {
    const contact = data[key];

    const {relationships, ...itemObj} = contact;

    transformedData[key] = {
      Relationships: relationships,
      ...itemObj,
    };
  }

  return transformedData;
}

function transformDOItem({
  id,
  groupId,
  data,
  attachments,
  notes,
  meta,
  connected_contacts,
}) {
  return {
    id,
    groupId,
    data: convertObjectToArray(data)
      .filter((d) => !!d.id) //weird fix to prevent api from returning null item fieldIds
      .map((d) => {
        const {id: fieldId, value} = d;

        return {
          id: `${id}-${fieldId}`,
          fieldId,
          value: convertDOItemValue(value),
          contactSpecificValues: getContactSpecificValues(value),
        };
      }),
    attachments,
    notes: notes.map((note) => {
      return {
        id: note.Id || note.id,
        accepted: note.Accepted,
        userId: note.UserID,
        creationDate: note.CreationDate && note.CreationDate.date,
        completionDate: note.CompletionDate,
        lastUpdated: note.LastUpdated && note.LastUpdated.date,
        lastUpdatedBy: note.LastUpdatedBy,
        endDate: note.EndDate,
        type: note.ActionType,
        actionDate: note.ActionDate,
        title: note.ActionDescription,
        noteType: note._JSON && note._JSON.note_type,
        note: note.CreationNotes,
        tags: tagsToArray(note.tags),
        createdBy: note.userName,
      };
    }),
    meta: {
      editable: meta.editable === 'yes',
      searchable: meta.editable === 'yes',
      multiple_link: meta.editable === 'yes',
    },
    connectedContacts: convertObjectToArray(connected_contacts).map(
      (contact) => {
        const {
          id: contactId,
          FirstName: firstName,
          LastName: lastName,
          Email: email,
          Relationships: relationships,
        } = contact;
        const relationshipsArr = [];

        relationships.forEach((relId) => {
          relationshipsArr.push({
            id: relId,
          });
        });

        return {
          contactId,
          firstName,
          lastName,
          email,
          relationships: relationshipsArr,
        };
      },
    ),
  };
}
