const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteDOSectionCondition = async (event, context, callback) => {
  try {
    const postResponse = await deleteDOSectionConditionFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteDOSectionCondition. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteDOSectionCondition', err);
    callback(err);
  }
};

async function deleteDOSectionConditionFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    dataObjectId: params.dataObjectId,
    sectionName: params.sectionName,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/sectionDisplayCondition`;

  const response = await http.delete(url, body);

  await getJsonResponse(response);

  return {
    id: `${params.dataObjectId}-${params.sectionName}`,
    success: true,
  };
}
