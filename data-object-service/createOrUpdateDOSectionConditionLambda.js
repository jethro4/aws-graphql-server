const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateDOSectionCondition = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postDOSectionCondition(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateDOSectionCondition. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateDOSectionCondition', err);
    callback(err);
  }
};

async function postDOSectionCondition(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    dataObjectId: params.dataObjectId,
    dataObjectName: params.dataObjectName,
    queryField: {
      ...params.sections.reduce((sectionsAcc, section) => {
        const sectionsAccObj = {...sectionsAcc};

        sectionsAccObj[section.sectionName] = {
          ...section.conditions.reduce(
            (conditionsAcc, condition, conditionIndex) => {
              const conditionsAccObj = {...conditionsAcc};

              conditionsAccObj[conditionIndex] = {
                fieldName: condition.name,
                fieldLogic: condition.logic,
                fieldOperator: condition.operator,
                fieldValues: condition.values,
                fieldValue: condition.value,
              };

              return conditionsAccObj;
            },
            {},
          ),
        };

        return sectionsAccObj;
      }, {}),
    },
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/sectionDisplayCondition`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return params.sections.map((section) => ({
    ...section,
    id: `${params.dataObjectId}-${section.sectionName}`,
  }));
}
