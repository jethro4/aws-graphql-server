const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteDataObjectField = async (event, context, callback) => {
  try {
    const postResponse = await deleteDataObjectFieldFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteDataObjectField. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteDataObjectField', err);
    callback(err);
  }
};

async function deleteDataObjectFieldFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    Id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/field`;

  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  return {
    success: json.Status,
    fieldId: params.id,
  };
}
