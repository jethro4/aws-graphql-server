const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.uploadDOAttachmentUrl = async (event, context, callback) => {
  try {
    const doAttachmentUrlResponse = await postAttachmentUrl(
      event.arguments.input,
    );

    // console.log(
    //   'Done uploadDOAttachmentUrl. doAttachmentUrlResponse:',
    //   JSON.stringify(doAttachmentUrlResponse),
    // );

    callback(null, doAttachmentUrlResponse);
  } catch (err) {
    console.log('Error on uploadDOAttachmentUrl', err);

    callback(err);
  }
};

async function postAttachmentUrl(params) {
  const {appName, apiKey, ...data} = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/connected_data_attach_file?access_key=${apiKey}`;

  const response = await http.post(url, {
    ItemId: data.itemId,
    Title: data.fileName,
    FileURL: data.url,
  });

  const json = await getJsonResponse(response);

  return transformResponse(json, data.itemId);
}

function transformResponse(json, itemId) {
  const {DBdata, db_record} = json;
  const fileList = db_record[itemId] || [];
  const fileItem = fileList[fileList.length - 1];
  const {filename: fileName, thumbnail, download_url: downloadUrl} = DBdata;

  return {
    success: true,
    fileId: fileItem && fileItem.id,
    fileName,
    thumbnail,
    downloadUrl,
  };
}
