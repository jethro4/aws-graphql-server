const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDOSectionConditions(event.arguments);

    // console.log(
    //   'Done queryDOSectionConditions. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDOSectionConditions', err);

    callback(err);
  }
};

async function queryDOSectionConditions(query) {
  const {appName, apiKey, groupId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/sectionDisplayCondition?access_key=${apiKey}&dataObjectId=${groupId}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const doSectionConditions = transformResponse(json);

  return doSectionConditions;
}

function transformResponse(json) {
  const {dataObjectId, queryField} = json.data || {};
  const doSectionConditions = [];

  if (queryField) {
    Object.entries(queryField).forEach(([sectionName, conditionsObj]) => {
      const section = {
        id: `${dataObjectId}-${sectionName}`,
        sectionName,
        conditions: [],
      };
      Object.values(conditionsObj).forEach(
        ({
          fieldName: name,
          fieldLogic: logic,
          fieldOperator: operator,
          fieldValues: values,
          fieldValue: value,
        }) => {
          section.conditions.push({
            name,
            logic,
            operator,
            values,
            value,
          });
        },
      );

      doSectionConditions.push(section);
    });
  }

  return doSectionConditions;
}
