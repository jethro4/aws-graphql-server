const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDataObjectItemHistory(event.arguments);

    // console.log(
    //   'Done queryDataObjectItemHistory. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDataObjectItemHistory', err);

    callback(err);
  }
};

async function queryDataObjectItemHistory(query) {
  const {
    appName,
    apiKey,
    type = 'data',
    doItemId,
    contactId,
    page,
    limit,
  } = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/history/${type}/${doItemId}?api_key=${apiKey}&contactId=${contactId}&page=${page}&limit=${limit}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const doItemHistory = transformResponse(json, type);

  return doItemHistory;
}

function transformResponse(json, type) {
  const {data} = json;
  const doItemHistory = [];

  if (typeof data === 'object' && !Array.isArray(data)) {
    Object.entries(data).forEach(([date, itemsInDate]) => {
      for (const item of itemsInDate) {
        const itemValues = [
          {
            fieldKey: 'dateChanged',
            value: date,
            sharedFieldValue: '',
          },
        ];

        Object.entries(item).map(([fieldKey, value = '']) => {
          itemValues.push({
            fieldKey,
            value,
            sharedFieldValue: date,
          });
        });

        doItemHistory.push({
          data: itemValues,
        });
      }
    });
  } else {
    for (var item of data) {
      const itemValues = [];
      Object.entries(item).map(([fieldKey, value = '']) => {
        itemValues.push({
          fieldKey,
          value,
          sharedFieldValue: '',
        });
      });

      doItemHistory.push({
        data: itemValues,
      });
    }
  }

  return {
    fields: getDOHistoryFields(type),
    items: doItemHistory,
  };
}

function getDOHistoryFields(type) {
  switch (type) {
    case 'automation': {
      return [
        {
          label: 'Condition',
          fieldKey: 'TriggerCondition',
          isSharedField: false,
        },
        {
          label: 'Trigger Action',
          fieldKey: 'TriggerAction',
          isSharedField: false,
        },
        {
          label: 'Contact ID',
          fieldKey: 'ContactId',
          isSharedField: false,
        },
        {
          label: 'Trigger Date',
          fieldKey: 'TriggeredDate',
          isSharedField: false,
        },
      ];
    }
    case 'data': {
      return [
        {
          label: 'Date Changed',
          fieldKey: 'dateChanged',
          isSharedField: true,
        },
        {
          label: 'Field Name',
          fieldKey: 'fieldName',
          isSharedField: false,
        },
        {
          label: 'Changed From',
          fieldKey: 'from',
          isSharedField: false,
        },
        {
          label: 'Changed To',
          fieldKey: 'to',
          isSharedField: false,
        },
        {
          label: 'Contact Name',
          fieldKey: 'contact',
          isSharedField: false,
        },
        {
          label: 'Contact ID',
          fieldKey: 'contactId',
          isSharedField: false,
        },
        {
          label: 'Changed By',
          fieldKey: 'changedBy',
          isSharedField: false,
        },
      ];
    }
    case 'relationship': {
      return [
        {
          label: 'Date Changed',
          fieldKey: 'dateChanged',
          isSharedField: true,
        },
        {
          label: 'Contact Name',
          fieldKey: 'contact',
          isSharedField: false,
        },
        {
          label: 'Contact ID',
          fieldKey: 'contactId',
          isSharedField: false,
        },
        {
          label: 'Previous Relationship',
          fieldKey: 'prevRelationship',
          isSharedField: false,
        },
        {
          label: 'New Relationship',
          fieldKey: 'newRelationship',
          isSharedField: false,
        },
        {
          label: 'Changed By',
          fieldKey: 'changedBy',
          isSharedField: false,
        },
      ];
    }
  }
}
