const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryEmailSignature(event.arguments);

    // console.log(
    //   'Done queryEmailSignature. response:',
    //   JSON.stringify(response),
    // );

    callback(null, response);
  } catch (err) {
    console.log('Error on queryEmailSignature', err);
    callback(err);
  }
};

async function queryEmailSignature(query) {
  const {appName, apiKey, userId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/signature?userId=${userId}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data} = json;

  return {
    html: data,
  };
}
