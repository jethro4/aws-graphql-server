const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.sms = async (event, context, callback) => {
  const {appName, apiKey, ...params} = event.arguments.input;

  try {
    const postResponse = await postSMS(appName, apiKey, params);

    // console.log('Done sms. postResponse:', JSON.stringify(postResponse));

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on sms', err);
    callback(err);
  }
};

async function postSMS(appName, apiKey, params) {
  const body = {
    api_key: apiKey,
    ...params,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/sms`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
