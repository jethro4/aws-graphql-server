const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryEmailTemplateGroups(event.request.headers);

    // console.log('Done queryEmailTemplateGroups. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryEmailTemplateGroups', err);
    callback(err);
  }
};

async function queryEmailTemplateGroups({
  'app-name': appName,
  'app-api-key': apiKey,
}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/email/templateGroups?&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data, count} = json;
  const templates = [];

  Object.entries(data).forEach(([templateId, {Title: title}]) => {
    templates.push({
      id: templateId,
      title,
    });
  });

  return {
    items: templates,
    count: Number(count),
  };
}
