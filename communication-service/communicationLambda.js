const {query: queryEmailTemplate} = require('./queryEmailTemplateLambda');
const {query: queryEmailTemplates} = require('./queryEmailTemplatesLambda');
const {
  query: queryEmailTemplateGroups,
} = require('./queryEmailTemplateGroupsLambda');
const {query: queryEmailSignature} = require('./queryEmailSignatureLambda');
const {query: queryNylasInfo} = require('./queryNylasInfoLambda');
const {query: queryNylasUrl} = require('./queryNylasUrlLambda');
const {
  createOrUpdateEmailSignature,
} = require('./createOrUpdateEmailSignatureLambda');
const {emailContactSupport} = require('./emailContactSupportLambda');
const {sms} = require('./smsLambda');
const {email} = require('./emailLambda');

module.exports.communication = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'getEmailTemplate':
      queryEmailTemplate(...params);
      break;
    case 'listEmailTemplates':
      queryEmailTemplates(...params);
      break;
    case 'listEmailTemplateGroups':
      queryEmailTemplateGroups(...params);
      break;
    case 'getEmailSignature':
      queryEmailSignature(...params);
      break;
    case 'getNylasInfo':
      queryNylasInfo(...params);
      break;
    case 'getNylasUrl':
      queryNylasUrl(...params);
      break;
    case 'sms':
      sms(...params);
      break;
    case 'email':
      email(...params);
      break;
    case 'createOrUpdateEmailSignature':
      createOrUpdateEmailSignature(...params);
      break;
    case 'emailContactSupport':
      emailContactSupport(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
