const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryNylasInfo(event.arguments);

    // console.log('Done queryNylasInfo. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryNylasInfo', err);
    callback(err);
  }
};

async function queryNylasInfo(query) {
  const {appName, apiKey, userId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/nylas_check_token?userId=${userId}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {NylasAccessToken, Status} = json;

  return {
    isValidToken: !!NylasAccessToken,
    isValidUser: !!Status,
  };
}
