const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryEmailTemplates(event.arguments);

    // console.log('Done queryEmailTemplates. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryEmailTemplates', err);
    callback(err);
  }
};

async function queryEmailTemplates(query) {
  const {appName, apiKey} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/email/template_ids/all?&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data, count} = json;
  const templates = [];

  Object.entries(data).forEach(
    ([templateId, {TemplateGroupId: templateGroupId, Title: title}]) => {
      if (templateGroupId) {
        templates.push({
          templateGroupId,
          templateId,
          title,
        });
      }
    },
  );

  return {
    items: templates,
    count: Number(count),
  };
}
