type EmailTemplate {
  html: String
}

type EmailTemplateItem {
  templateGroupId: String
  templateId: String
  title: String
}

type EmailTemplates {
  items: [EmailTemplateItem]
  count: Int
}

type EmailSignature {
  html: String
}

input CreateOrUpdateEmailSignatureInput {
  appName: String!
  apiKey: String!
  userId: String!
  signature: String
}

type EmailSignatureResponse {
  success: Boolean
}

type NylasInfo {
  isValidToken: Boolean
  isValidUser: Boolean
}

type NylasUrl {
  url: String
}

type SMSResponse {
  success: Boolean
}

type EmailResponse {
  success: Boolean
}

input EmailContactSupportInput {
  subject: String
  message: String
}

type EmailContactSupportResponse {
  success: Boolean
}

input SMSInput {
  appName: String!
  apiKey: String!
  userId: ID!
  sendMethod: String!
  contactId: ID
  toPhone: String
  txtMessage: String
  dataObjectItemId: ID
  queryId: ID
  dataObjectName: String
  widgetName: String
}

input EmailAttachmentInput {
  fileName: String
  content: String
}

input EmailInput {
  appName: String!
  apiKey: String!
  userId: ID!
  sendMethod: String!
  mode: String
  toContacts: String
  subject: String
  message: String
  signature: String
  uploadedFileAttachments: [EmailAttachmentInput]
  selectedFileAttachments: String
  queryId: String
  groupName: String
  widgetName: String
  itemId: String
  resend: Boolean
  emailId: ID
}

type EmailTemplateGroup {
  id: ID
  title: String
}

type EmailTemplateGroups {
  items: [EmailTemplateGroup]
  count: Int
}

type Query {
  getEmailTemplate(
    appName: String!
    apiKey: String!
    templateId: String!
  ): EmailTemplate
  listEmailTemplates(appName: String!, apiKey: String!): EmailTemplates
  listEmailTemplateGroups: EmailTemplateGroups
  getEmailSignature(
    appName: String!
    apiKey: String!
    userId: String!
  ): EmailSignature
  getNylasInfo(appName: String!, apiKey: String!, userId: String!): NylasInfo
  getNylasUrl(appName: String!, apiKey: String!, email: String!): NylasUrl
}

type Mutation {
  sms(input: SMSInput!): SMSResponse
  email(input: EmailInput!): EmailResponse
  createOrUpdateEmailSignature(
    input: CreateOrUpdateEmailSignatureInput!
  ): EmailSignatureResponse
  emailContactSupport(
    input: EmailContactSupportInput!
  ): EmailContactSupportResponse
}

schema {
  query: Query
  mutation: Mutation
}
