const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryNylasUrl(event.arguments);

    // console.log('Done queryNylasUrl. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryNylasUrl', err);
    callback(err);
  }
};

async function queryNylasUrl(query) {
  const {appName, apiKey, email} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/nylas_connect?Email=${email}&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {URL: url} = json;

  return {
    url,
  };
}
