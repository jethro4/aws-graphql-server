const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryEmailTemplate(event.arguments);

    // console.log('Done queryEmailTemplate. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryEmailTemplate', err);
    callback(err);
  }
};

async function queryEmailTemplate(query) {
  const {appName, apiKey, templateId} = query;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/email/template/${templateId}?&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json, templateId);
}

function transformResponse(json, templateId) {
  const {data} = json;

  return {
    html:
      data[templateId] && data[templateId].CompiledEmailHTML
        ? Buffer.from(data[templateId].CompiledEmailHTML, 'base64').toString(
            'utf-8',
          )
        : '',
  };
}
