const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateEmailSignature = async (
  event,
  context,
  callback,
) => {
  const {appName, apiKey, ...params} = event.arguments.input;

  try {
    const postResponse = await postSignature(appName, apiKey, params);

    // console.log(
    //   'Done createOrUpdateEmailSignature. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateEmailSignature', err);
    callback(err);
  }
};

async function postSignature(appName, apiKey, params) {
  const body = {
    api_key: apiKey,
    userId: params.userId,
    signature: params.signature,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/signature`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
