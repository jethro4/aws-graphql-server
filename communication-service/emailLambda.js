const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.email = async (event, context, callback) => {
  const {appName, apiKey, ...params} = event.arguments.input;

  try {
    const postResponse = await postEmail(appName, apiKey, params);

    // console.log('Done email. postResponse:', JSON.stringify(postResponse));

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on email', err);
    callback(err);
  }
};

async function postEmail(appName, apiKey, params) {
  const body = {
    api_key: apiKey,
    userId: params.userId,
    sendMethod: params.sendMethod,
    mode: params.mode,
    toContacts: params.toContacts,
    subject: params.subject,
    message: params.message
      ? Buffer.from(params.message).toString('base64')
      : '',
    signature:
      !params.resend &&
      !(params.message && params.message.includes('~Sender.Signature~'))
        ? Buffer.from('~Sender.Signature~').toString('base64')
        : '',
    uploadedFileAttachments: params.uploadedFileAttachments
      ? params.uploadedFileAttachments.map((f) => ({
          filename: f.fileName,
          content: f.content,
        }))
      : [],
    //For widgets
    queryId: params.queryId,
    groupName: params.groupName,
    widgetName: params.widgetName,
    itemId: params.itemId,
    ...(params.selectedFileAttachments && {
      selectedFileAttachments: params.selectedFileAttachments,
    }),
    ...(params.emailId && {
      emailId: params.emailId,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/email`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
