const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.emailContactSupport = async (event, context, callback) => {
  try {
    const postResponse = await postEmailContactSupport(
      event.request.headers,
      event.arguments.input,
    );

    // console.log('Done email. postResponse:', JSON.stringify(postResponse));

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on email', err);
    callback(err);
  }
};

async function postEmailContactSupport(
  {'app-name': appName, 'app-api-key': apiKey, 'session-id': sessionName},
  params,
) {
  const body = {
    api_key: apiKey,
    sessionName,
    subject: params.subject,
    message: params.message,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/internal_support/send_message`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
