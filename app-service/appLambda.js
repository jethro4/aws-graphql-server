const {query: queryAppSettings} = require('./queryAppSettingsLambda');
const {updateAppSettings} = require('./updateAppSettingsLambda');
const {query: queryAppSubscription} = require('./queryAppSubscriptionLambda');

module.exports.app = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'getAppSettings':
      queryAppSettings(...params);
      break;
    case 'updateAppSettings':
      updateAppSettings(...params);
      break;
    case 'getAppSubscription':
      queryAppSubscription(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
