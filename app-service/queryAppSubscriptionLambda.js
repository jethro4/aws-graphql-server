const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryAppSubscription(event.request.headers);

    callback(null, response);
  } catch (err) {
    console.log('Error on queryAppSubscription', err);
    callback(err);
  }
};

async function queryAppSubscription(headers) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/app_subscription?api_key=${apiKey}&sessionName=${sessionName}&redirectUrl=https://${appName}.macantacrm.app/app/admin-settings/user-management`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  return {
    planId: json?.subscription?.planId,
    status: json?.status,
    options:
      json?.options?.map((o) => {
        const plan = json.plans.find((p) => p.id === o.planId);

        return {
          id: o.planId,
          name: o.planName,
          price: o.planPrice,
          currencyCode: o.currencyCode,
          period: plan.period,
          periodUnit: plan.periodUnit,
          pricingModel: plan.pricingModel,
          subscriptionUrl: json.hostedPage[o.planId].url,
        };
      }) || [],
  };
}
