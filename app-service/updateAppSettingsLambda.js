const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.updateAppSettings = async (event, context, callback) => {
  try {
    const postResponse = await postAppSettings(event.arguments.input);

    // console.log(
    //   'Done updateAppSettings. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on updateAppSettings', err);
    callback(err);
  }
};

async function postAppSettings({
  appName,
  apiKey,
  type,
  value,
  fileName,
  mimeType,
}) {
  const body = {
    api_key: apiKey,
    settingName: type,
    settingValue: value,
    ...(mimeType && {
      fileName,
      mimeTpe: mimeType,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/app_settings`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {
    success: true,
    type,
    value,
  };
}
