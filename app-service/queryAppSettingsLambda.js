const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryAppSettings(event.request.headers);

    // console.log('Done queryAppSettings. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryAppSettings', err);
    callback(err);
  }
};

async function queryAppSettings({'app-name': appName}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/app_settings?q=ui_colour,macanta_custom_logo,AppTimeZone,MacantaTabOrder,sitename,IndirectRelationshipGroups,FilteredEmailDomain,MacantaPlanId,EnableCDImport,AutoGenerateEmailFormat,login_disabled`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {
    ui_colour: uiColour,
    macanta_custom_logo: customLogo = {},
    AppTimeZone: appTimeZone,
    MacantaTabOrder: macantaTabOrder,
    sitename,
    IndirectRelationshipGroups: indirectRelationshipGroups = [],
    FilteredEmailDomain: filteredEmailDomain,
    MacantaPlanId: macantaPlanId,
    EnableCDImport: enableCDImport,
    AutoGenerateEmailFormat: generatedEmailFormat,
    login_disabled: loginDisabledMessage,
    SalesFerryNotice: salesFerryNotice,
  } = json;

  const appSettings = {
    uiColour,
    customLogo,
    appTimeZone,
    macantaTabOrder,
    sitename,
    indirectRelationshipGroups,
    filteredEmailDomain: filteredEmailDomain || [],
    macantaPlanId,
    enableCDImport: enableCDImport === 'yes',
    generatedEmailFormat:
      generatedEmailFormat === false ? '' : generatedEmailFormat.trim(),
    loginDisabledMessage:
      loginDisabledMessage === false ? '' : loginDisabledMessage.trim(),
    salesFerryRedirect: salesFerryNotice?.redirect || '',
  };

  return appSettings;
}
