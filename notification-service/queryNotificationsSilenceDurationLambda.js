const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryNotificationsStatus(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryNotificationsStatus. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryNotificationsStatus', err);

    callback(err);
  }
};

async function queryNotificationsStatus(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;
  const {userId, type} = params;

  let url = '';

  if (!type) {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/deepwork?api_key=${apiKey}`;
  } else {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/${type}/status?api_key=${apiKey}`;
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response, '{"TimeSet":"","Deepwork":"0"}');

  const result = !type ? json && JSON.parse(json) : json;
  const startTime = result.TimeSet || '';
  const duration = result.Deepwork ? Number(result.Deepwork) : 0;

  return {
    status: result.status || (!duration ? 'inactive' : 'active'),
    startTime,
    duration,
  };
}
