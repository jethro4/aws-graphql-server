const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteNotifications = async (event, context, callback) => {
  try {
    const deleteResponse = await deleteNotifs(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteNotifications. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deleteNotifications', err);

    callback(err);
  }
};

async function deleteNotifs(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;
  const {id, userId, type} = params;

  let url = '';

  if (id) {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/delete/${id}?api_key=${apiKey}`;
  } else {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/${type}/delete?api_key=${apiKey}`;
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return {
    success: json.result,
  };
}
