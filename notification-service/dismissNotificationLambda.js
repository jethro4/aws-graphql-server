const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.dismissNotification = async (event, context, callback) => {
  try {
    const dismissResponse = await dismiss(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done dismissNotification. dismissResponse:',
    //   JSON.stringify(dismissResponse),
    // );

    callback(null, dismissResponse);
  } catch (err) {
    console.log('Error on dismissNotification', err);

    callback(err);
  }
};

async function dismiss(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;
  const {id, userId} = params;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/dismiss/${id}?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return {
    success: json.result,
    notificationId: id,
  };
}
