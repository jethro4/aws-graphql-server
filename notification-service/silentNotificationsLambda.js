const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.silentNotifications = async (event, context, callback) => {
  try {
    const silentResponse = await silentNotifs(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done silentNotifications. silentResponse:',
    //   JSON.stringify(silentResponse),
    // );

    callback(null, silentResponse);
  } catch (err) {
    console.log('Error on silentNotifications', err);

    callback(err);
  }
};

async function silentNotifs(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;
  const {userId, type, action, duration} = params;

  let url = '';

  if (type) {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/${type}/${action}?api_key=${apiKey}`;
  } else {
    url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}/deepwork/${duration}?api_key=${apiKey}`;
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return {
    success: json.status === 1,
  };
}
