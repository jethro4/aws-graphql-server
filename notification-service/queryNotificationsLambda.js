const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {sortArrayByObjectKey} = require('../utils/array');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryNotifications(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryNotifications. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryNotifications', err);

    callback(err);
  }
};

async function queryNotifications(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;
  const {userId} = params;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/notifications/${userId}?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = [], total = 0} = json;
  const notifications = [];

  for (var notification of data) {
    const meta = (notification.Meta && JSON.parse(notification.Meta)) || {};

    const notificationObj = {
      id: notification.Id,
      contactId: notification.ContactId,
      userContactId: notification.UserContactId,
      email: notification.ContactEmail,
      name: notification.ContactFullName,
      message: notification.Message,
      meta: {
        itemId: meta.ItemId,
        groupId: meta.GroupId,
        groupTitle: meta.GroupTitle,
      },
      status: notification.Status,
      type: notification.Type,
      createdDate: notification.Created,
    };

    notifications.push(notificationObj);
  }

  return {
    items: sortArrayByObjectKey(notifications, 'createdDate', 'desc'),
    total: Number(total),
  };
}
