const {query: queryNotifications} = require('./queryNotificationsLambda');
const {
  query: queryNotificationsStatus,
} = require('./queryNotificationsSilenceDurationLambda');
const {dismissNotification} = require('./dismissNotificationLambda');
const {deleteNotifications} = require('./deleteNotificationsLambda');
const {silentNotifications} = require('./silentNotificationsLambda');

module.exports.notification = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'listNotifications':
      queryNotifications(...params);
      break;
    case 'getNotificationsStatus':
      queryNotificationsStatus(...params);
      break;
    case 'dismissNotification':
      dismissNotification(...params);
      break;
    case 'deleteNotifications':
      deleteNotifications(...params);
      break;
    case 'silentNotifications':
      silentNotifications(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
