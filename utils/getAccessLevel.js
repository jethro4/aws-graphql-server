const accessLevels = {
  NoAccess: "NO_ACCESS",
  ReadOnly: "READ_ONLY",
  ReadWrite: "READ_WRITE",
  GlobalAccess: "GLOBAL_ACCESS",
};

module.exports = (value) => {
  if (typeof value === "string") {
    return accessLevels[value];
  }

  return accessLevels.NoAccess;
};
