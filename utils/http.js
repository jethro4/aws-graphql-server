const fetch = require('node-fetch');

async function fetchWithLogger(url, options) {
  const method = options?.method || 'get';
  console.log(
    'FETCH REQUEST -',
    `URL: ${url}\n`,
    Object.assign(
      {
        method,
      },
      options?.body && {
        body: options.body,
      },
    ),
  );

  const response = await fetch(
    url,
    Object.assign(
      {},
      options && {
        ...options,
        body: JSON.stringify(options.body),
      },
    ),
  );

  return Object.assign(response, {params: options?.body});
}

module.exports = {
  get: (url) => fetchWithLogger(url),
  post: (url, body) =>
    fetchWithLogger(url, {
      method: 'post',
      body,
      headers: {'Content-Type': 'application/json'},
    }),
  upload: (url, body) =>
    fetchWithLogger(url, {
      method: 'post',
      body,
      headers: {'Content-Type': 'multipart/form-data'},
    }),
  delete: (url, body) =>
    fetchWithLogger(url, {
      method: 'delete',
      body,
      headers: {'Content-Type': 'application/json'},
    }),
};
