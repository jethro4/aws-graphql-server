const isArray = require('lodash/isArray');
const isFunction = require('lodash/isFunction');

const delay = (timeout, val) =>
  new Promise(function (resolve) {
    setTimeout(() => resolve(val), timeout);
  });

const syncPromises = async (arr = [], promiseCallback) => {
  return await arr.reduce(async (acc, valueOrCallback, currentIndex) => {
    const accArr = await acc;

    let result;

    if (isArray(valueOrCallback)) {
      result = await syncPromises(
        valueOrCallback,
        !promiseCallback ? null : (val) => promiseCallback(val, currentIndex),
      );
    } else if (promiseCallback) {
      result = await promiseCallback(valueOrCallback, currentIndex, accArr);
    } else if (isFunction(valueOrCallback)) {
      result = await valueOrCallback(currentIndex, accArr);
    } else {
      result = valueOrCallback;
    }

    return [...accArr, result];
  }, Promise.resolve([]));
};

module.exports = {
  delay,
  syncPromises,
};
