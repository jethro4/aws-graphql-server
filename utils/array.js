const get = require('lodash/get');
const isNumber = require('lodash/isNumber');
const isNaN = require('lodash/isNaN');

const convertPropertiesToArray = (obj, options) => {
  const {labelKey, valueKey, omitKeys} = Object.assign(
    {labelKey: 'key', valueKey: 'value', omitKeys: []},
    options,
  );
  const tempObj = {...obj};

  omitKeys.forEach((key) => {
    delete tempObj[key];
  });

  const entries = Object.entries(tempObj);
  const arr = entries.map(([key, value]) => ({
    [labelKey]: key,
    [valueKey]: value,
  }));

  return arr;
};

const convertObjectToArray = (obj) => {
  const items = [];

  for (var key in obj) {
    const item = obj[key];

    const itemObj = {
      id: key,
      ...item,
    };

    items.push(itemObj);
  }

  return items;
};

const convertObjectToArrayWithValue = (obj) => {
  const items = [];

  for (var key in obj) {
    const value = obj[key];

    const itemObj = {
      id: key,
      value,
    };

    items.push(itemObj);
  }

  return items;
};

const tagsToArray = (tags = '') => {
  return tags
    .split(',')
    .map((tag) => (tag.charAt(0) === '#' ? tag.substring(1) : tag))
    .filter((tag) => !!tag);
};

const sortArray = (arr = [], sortDirection) => {
  return arr.slice().sort(compareObjectByValue(null, sortDirection));
};

const sortArrayByObjectKey = (arr = [], key, sortBy) => {
  return arr.slice().sort(compareObjectByValue(key, sortBy));
};

const sortArrayByPriority = (arr = [], key, priority = []) => {
  return arr.slice().sort(compareObjectByPriority(key, priority));
};

const sortArrayByObjectGroups = (arr = [], keys = []) => {
  return arr.slice().sort(compareObjectByGroups(keys));
};

const compareObjectByValue =
  (key, sortBy = 'asc') =>
  (item1, item2) => {
    const item1Val = String(key ? get(item1, key) : item1).toLowerCase();
    const item2Val = String(key ? get(item2, key) : item2).toLowerCase();
    const ascIndex = sortBy === 'desc' ? -1 : 1;

    if (item1Val < item2Val) {
      return -ascIndex;
    }
    if (item1Val > item2Val) {
      return ascIndex;
    }
    return 0;
  };

const compareObjectByPriority =
  (key, priority = []) =>
  (item1, item2) => {
    const item1Val = key ? item1[key] : item1;
    const item2Val = key ? item2[key] : item2;

    const item1PriorityIndex = priority.indexOf(item1Val);
    const item2PriorityIndex = priority.indexOf(item2Val);

    if (item1PriorityIndex !== -1) {
      if (item2PriorityIndex !== -1) {
        if (item1PriorityIndex > item2PriorityIndex) {
          return 1;
        } else if (item2PriorityIndex === item1PriorityIndex) {
          return 0;
        }
      }

      return -1;
    }

    return 0;
  };

const compareObjectByGroups = (keys) => (item1, item2) => {
  return keys.reduce((acc, key) => {
    const item1Val = item1[key];
    const item2Val = item2[key];
    let sortIndex = 0;

    if (isNumber(item1Val) && !isNaN(Number(item1Val))) {
      sortIndex = item1Val - item2Val;
    } else {
      sortIndex = item1Val.localeCompare(item2Val);
    }

    return acc || sortIndex;
  }, 0);
};

module.exports = {
  convertPropertiesToArray,
  convertObjectToArray,
  convertObjectToArrayWithValue,
  tagsToArray,
  sortArray,
  sortArrayByObjectKey,
  sortArrayByPriority,
  sortArrayByObjectGroups,
};
