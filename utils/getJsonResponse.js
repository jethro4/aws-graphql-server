module.exports = async (response, defaultJSONResponse) => {
  const text = await response.text();

  if (response.ok) {
    const jsonResponse = getJSON(text);

    if (!jsonResponse) {
      if (defaultJSONResponse) {
        return defaultJSONResponse;
      } else {
        logError(response, text);

        throw new SuccessJSONParseException(text, response);
      }
    }

    return jsonResponse;
  } else {
    const jsonResponse = getJSON(text);

    logError(response, text);

    if (!jsonResponse) {
      throw new FailedJSONParseException(text, response);
    } else if (jsonResponse.status === 'sessionExpired') {
      throw new SessionExpiredException('Session has expired', response);
    } else {
      let errorMessage = jsonResponse;

      if (jsonResponse.preview?.Error) {
        errorMessage = jsonResponse.preview.Error;
      } else if (jsonResponse.error) {
        errorMessage = jsonResponse.error?.message || jsonResponse.error;
      }

      if (response.status < 500) {
        if (response.status === 403) {
          throw new ForbiddenAccessException(errorMessage, response);
        } else {
          throw new InvalidParametersException(errorMessage, response);
        }
      } else {
        throw new InternalServerException(errorMessage, response);
      }
    }
  }
};

//validates if response is in html format
function getJSON(text) {
  try {
    const jsonResponse = JSON.parse(text);

    return jsonResponse;
  } catch (err) {
    return false;
  }
}

function logError(response, body) {
  console.error(
    'RESPONSE DETAILS:',
    JSON.stringify({
      ok: response.ok,
      status: response.status,
      statusText: response.statusText,
      url: response.url,
      params: response.params,
      body,
    }),
  );
}

class HttpException extends Error {
  constructor(message, response) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, HttpException);
    }
    this.name = 'HttpException';
    this.status = response.status;
  }
}

class InvalidParametersException extends HttpException {
  constructor(message, response) {
    super(message, response);
    if (HttpException.captureStackTrace) {
      HttpException.captureStackTrace(this, InvalidParametersException);
    }
    this.name = 'InvalidParametersException';
  }
}

class ForbiddenAccessException extends HttpException {
  constructor(message, response) {
    super(message, response);
    if (HttpException.captureStackTrace) {
      HttpException.captureStackTrace(this, ForbiddenAccessException);
    }
    this.name = 'ForbiddenAccessException';
  }
}

class InternalServerException extends HttpException {
  constructor(message, response) {
    super(message, response);
    if (HttpException.captureStackTrace) {
      HttpException.captureStackTrace(this, InternalServerException);
    }
    this.name = 'InternalServerException';
  }
}

class SessionExpiredException extends HttpException {
  constructor(message, response) {
    super(message, response);
    if (HttpException.captureStackTrace) {
      HttpException.captureStackTrace(this, SessionExpiredException);
    }
    this.name = 'SessionExpiredException';
  }
}

class SuccessJSONParseException extends HttpException {
  constructor(message, response) {
    super(message, response);
    if (HttpException.captureStackTrace) {
      HttpException.captureStackTrace(this, SuccessJSONParseException);
    }
    this.name = 'SuccessJSONParseException';
  }
}

class FailedJSONParseException extends HttpException {
  constructor(message, response) {
    super(message, response);
    if (HttpException.captureStackTrace) {
      HttpException.captureStackTrace(this, FailedJSONParseException);
    }
    this.name = 'FailedJSONParseException';
  }
}
