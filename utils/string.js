const isObject = require('lodash/isObject');
const isNil = require('lodash/isNil');

function parseJson(text, defaultValue) {
  try {
    const jsonResponse = JSON.parse(text);

    return jsonResponse;
  } catch (err) {
    if (!isNil(defaultValue)) {
      return defaultValue;
    } else if (isObject(text)) {
      return text;
    }

    throw err;
  }
}

module.exports = {
  parseJson,
};
