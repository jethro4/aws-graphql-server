const {tagsToArray, sortArrayByObjectKey} = require('./array');

describe('Array Utils', () => {
  describe('tagsToArray', () => {
    let tags;

    beforeEach(() => {
      tags = '#test1,#test2';
    });

    it('convert tags with hash to array', () => {
      const actualResult = tagsToArray(tags);
      const expectedResult = ['test1', 'test2'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('convert tags with no hash to array', () => {
      tags = 'test1,test2';
      const actualResult = tagsToArray(tags);
      const expectedResult = ['test1', 'test2'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('convert some tags with no hash to array', () => {
      tags = '#test1,test2';
      const actualResult = tagsToArray(tags);
      const expectedResult = ['test1', 'test2'];

      expect(actualResult).toEqual(expectedResult);
    });

    it('happy - sort array by key (date string)', () => {
      const arr = [
        {
          Id: '226',
          CreationDate: {
            date: '2021-04-07 15:21:17',
            timezone_type: 3,
            timezone: 'UTC',
          },
        },
        {
          Id: '224',
          CreationDate: {
            date: '2021-04-09 07:33:58',
            timezone_type: 3,
            timezone: 'UTC',
          },
        },
        {
          Id: '222',
          CreationDate: {
            date: '2021-04-10 18:40:52',
            timezone_type: 3,
            timezone: 'UTC',
          },
        },
      ];

      const actualResult = sortArrayByObjectKey(
        arr,
        'CreationDate.date',
        'desc',
      );

      const expectedResult = [arr[2], arr[1], arr[0]];

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
