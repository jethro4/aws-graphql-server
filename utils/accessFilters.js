const {
  query: queryAccessPermissions,
} = require('../permission-service/queryAccessPermissionsLambda');

const ACCESS_LEVELS = {
  NO_ACCESS: 'NO_ACCESS',
  READ_ONLY: 'READ_ONLY',
  READ_WRITE: 'READ_WRITE',
  GLOBAL_ACCESS: 'GLOBAL_ACCESS',
};

const getAccessPermissions = ({appName, apiKey, email}) => {
  return new Promise((resolve, reject) => {
    queryAccessPermissions(
      {
        arguments: {
          appName,
          apiKey,
          email,
        },
      },
      {},
      (err, response) => {
        if (err) {
          reject(err);
        } else {
          const accessPermissions = response;

          resolve(accessPermissions);
        }
      },
    );
  });
};

const filterByAccess = (accessPermissions, data, type) => {
  switch (type) {
    case 'section': {
      const {sections} = accessPermissions;
      const {groupId} = data;
      const doType = sections.find((section) => groupId === section.groupId);

      const accessConditionKey = 'permission';
      const permittedSections = doType
        ? doType.access.filter((access) => {
            return [
              ACCESS_LEVELS.READ_ONLY,
              ACCESS_LEVELS.READ_WRITE,
              ACCESS_LEVELS.GLOBAL_ACCESS,
            ].includes(access[accessConditionKey]);
          })
        : [];

      return permittedSections;
    }
    case 'tab': {
      const {tabs} = accessPermissions;

      const accessConditionKey = 'permission';
      const permittedDOTypes = tabs.filter((tab) => {
        return [
          ACCESS_LEVELS.READ_ONLY,
          ACCESS_LEVELS.READ_WRITE,
          ACCESS_LEVELS.GLOBAL_ACCESS,
        ].includes(tab[accessConditionKey]);
      });

      return permittedDOTypes;
    }
  }
};

module.exports = {
  getAccessPermissions,
  filterByAccess,
  ACCESS_LEVELS,
};
