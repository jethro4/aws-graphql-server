const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformNote, queryNotes} = require('./queryNotesLambda');

module.exports.createOrUpdateNote = async (event, context, callback) => {
  const {appName, apiKey, id, sessionName, ...note} = event.arguments.input;

  try {
    const postResponse = await postNote(appName, apiKey, id, sessionName, note);

    // console.log(
    //   'Done createOrUpdateNote. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateNote', err);
    callback(err);
  }
};

async function postNote(appName, apiKey, id, sessionName, note) {
  const body = {
    api_key: apiKey,
    title: note.title,
    note: note.note,
    tags: note.tags && note.tags.map((tag) => `#${tag}`),
    contactId: note.contactId,
    sessionName,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/note${
    id ? '/edit/' + id : ''
  }`;

  const response = await http.post(url, body);
  const json = await getJsonResponse(response);
  // implemented this way because create has array respone and edit has object response
  let transformedJson;
  if (!id) {
    transformedJson = transformNote(json[0] || json);
  } else {
    const note = await queryNotes(appName, {apiKey, id}); // need to call query to populate response with all task attributes, may change if /task/complete API provides all task attributes
    transformedJson = {...note.items[0], ...json};
  }

  return transformedJson;
}
