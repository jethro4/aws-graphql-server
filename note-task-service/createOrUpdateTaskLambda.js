const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformTask} = require('./queryTasksLambda');

module.exports.createOrUpdateTask = async (event, context, callback) => {
  const {appName, apiKey, id, sessionName, ...task} = event.arguments.input;

  try {
    const postResponse = await postTask(appName, apiKey, id, sessionName, task);

    // console.log(
    //   'Done createOrUpdateTask. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateTask', err);
    callback(err);
  }
};

async function postTask(appName, apiKey, id, sessionName, task) {
  const body = {
    api_key: apiKey,
    task_description: task.title,
    task_note: task.note,
    action_date: task.actionDate,
    user_email: task.userEmail,
    tags: task.tags && task.tags.map((tag) => `#${tag}`),
    sessionName,
  };

  if (task.id) {
    body['id'] = task.id;
  } else {
    body['contactId'] = task.contactId;
  }

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/task${
    id ? '/edit/' + id : ''
  }`;

  const response = await http.post(url, body);
  const json = await getJsonResponse(response);
  const transformedJson = transformTask(json[0] || json);

  return transformedJson;
}
