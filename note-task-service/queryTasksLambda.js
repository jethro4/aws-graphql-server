const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const {tagsToArray} = require('../utils/array');
const getJsonResponse = require('../utils/getJsonResponse');
const {parseJson} = require('../utils/string');

const query = async (event, context, callback) => {
  const {
    appName,
    apiKey,
    id,
    contactId,
    page = 0,
    limit = 100,
    filter,
    filterBy,
  } = event.arguments;

  try {
    const query = {
      apiKey,
      id,
      contactId,
      page,
      limit,
      filter,
      filterBy,
    };

    const queryResponse = await queryTasks(appName, query);

    // console.log('Done queryTasks. queryResponse:', queryResponse);

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryTasks', err);

    callback(err);
  }
};

async function queryTasks(appName, query) {
  const {id, contactId, page, limit, apiKey, filter, filterBy} = query;

  let queryString = `&contactId=${contactId}&page=${page}&limit=${limit}`;

  if (id) {
    queryString = `&id=${id}`;
  } else {
    if (filter) {
      queryString += `&filter=${filter}`;
    }
    if (filterBy) {
      queryString += `&filter_by=${filterBy}`;
    }
  }

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/task?api_key=${apiKey}${queryString}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = id ? {data: json} : json; // need this because having 'id' as query has removed 'data' field in api response
  const tasks = transformResponse(transformedJson);

  tasks.items = tasks.items
    .slice()
    .sort((a, b) => new Date(b.creationDate) - new Date(a.creationDate));

  return tasks;
}

function transformResponse(json) {
  const {data = [], count = 0, total = 0} = json;
  const tasks = [];

  for (var task of data) {
    const newTask = transformTask(task);
    tasks.push(newTask);
  }

  return {
    items: tasks,
    count: Number(count),
    total: Number(total),
  };
}

function transformTask(task) {
  const creationDate = parseJson(task.CreationDate, task.CreationDate);
  const lastUpdated = parseJson(task.LastUpdated, task.LastUpdated);
  const completionDate = parseJson(task.CompletionDate, task.CompletionDate);
  const completed = !!completionDate;
  const actionDate = parseJson(task.ActionDate, task.ActionDate);

  return {
    id: task.Id,
    accepted: task.Accepted,
    userId: task.UserID,
    creationDate: creationDate.date || creationDate,
    contactId: task.ContactId,
    completionDate: completionDate.date || completionDate,
    completed,
    lastUpdated: lastUpdated.date || lastUpdated,
    lastUpdatedBy: task.LastUpdatedBy,
    endDate: task.EndDate,
    type: task.ActionType,
    actionDate: actionDate.date || actionDate,
    title: task.ActionDescription && task.ActionDescription.trim(),
    assignedBy: task._Taskassignedby,
    assignedTo: task._Taskassignedto,
    note: task.CreationNotes && task.CreationNotes.trim(),
    tags: tagsToArray(task.tags),
  };
}

module.exports.query = query;
module.exports.queryTasks = queryTasks;
module.exports.transformTask = transformTask;
