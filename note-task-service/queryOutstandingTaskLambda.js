const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryOutstandingTask(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryOutstandingTask. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryOutstandingTask', err);

    callback(err);
  }
};

async function queryOutstandingTask(
  {'app-name': appName, 'app-api-key': apiKey},
  {contactId},
) {
  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/outstanding_task?api_key=${apiKey}&contactId=${contactId}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  return {
    hasTask: !!json?.hasTask,
  };
}
