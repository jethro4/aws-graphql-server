const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {queryTasks} = require('./queryTasksLambda');

module.exports.completeTask = async (event, context, callback) => {
  const {appName, apiKey, id} = event.arguments.input;

  try {
    const postResponse = await completeTask(appName, apiKey, id);

    // console.log(
    //   'Done completeTask. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on completeTask', err);
    callback(err);
  }
};

async function completeTask(appName, apiKey, id) {
  const body = {
    api_key: apiKey,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/task/complete/${id}`;

  const response = await http.post(url, body);
  const json = await getJsonResponse(response);
  const task = await queryTasks(appName, {apiKey, id}); // need to call query to populate response with all task attributes, may change if /task/complete API provides all task attributes
  const transformedJson = {...task.items[0], ...json};

  return transformedJson;
}
