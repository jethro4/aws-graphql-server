const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const {tagsToArray} = require('../utils/array');
const getJsonResponse = require('../utils/getJsonResponse');
const {parseJson} = require('../utils/string');

const query = async (event, context, callback) => {
  const {
    appName,
    apiKey,
    id,
    contactId,
    page = 0,
    limit = 100,
    filter,
    filterBy,
  } = event.arguments;

  try {
    const query = {
      apiKey,
      id,
      contactId,
      page,
      limit,
      filter,
      filterBy,
    };

    const queryResponse = await queryNotes(appName, query);

    // console.log('Done queryNotes. queryResponse:', queryResponse);

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryNotes', err);

    callback(err);
  }
};

async function queryNotes(appName, query) {
  const {id, contactId, page, limit, apiKey, filter, filterBy} = query;

  let queryString = `&contactId=${contactId}&page=${page}&limit=${limit}`;

  if (id) {
    queryString = `&id=${id}`;
  } else {
    if (filter) {
      queryString += `&filter=${filter}`;
    }
    if (filterBy) {
      queryString += `&filter_by=${filterBy}`;
    }
  }

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/note?api_key=${apiKey}${queryString}`;

  const response = await http.get(url);
  const json = await getJsonResponse(response);
  const transformedJson = id ? {data: json} : json; // need this because having 'id' as query has removed 'data' field in api response
  const notes = transformResponse(transformedJson);

  notes.items = notes.items
    .slice()
    .sort((a, b) => new Date(b.creationDate) - new Date(a.creationDate));

  return notes;
}

function transformResponse(json) {
  const {data = [], count = 0, total = 0} = json;
  const notes = [];

  for (var note of data) {
    const newNote = transformNote(note);
    notes.push(newNote);
  }

  return {
    items: notes,
    count: Number(count),
    total: Number(total),
  };
}

function transformNote(note) {
  const creationDate = parseJson(note.CreationDate, note.CreationDate);
  const lastUpdated = parseJson(note.LastUpdated, note.LastUpdated);
  const completionDate = parseJson(note.CompletionDate, note.CompletionDate);
  const actionDate = parseJson(note.ActionDate, note.ActionDate);

  return {
    id: note.Id || note.id,
    accepted: note.Accepted,
    userId: note.UserID,
    creationDate: creationDate.date || creationDate,
    contactId: note.ContactId,
    completionDate: completionDate.date || completionDate,
    lastUpdated: lastUpdated.date || lastUpdated,
    lastUpdatedBy: note.LastUpdatedBy,
    endDate: note.EndDate,
    type: note.ActionType,
    actionDate: actionDate.date || actionDate,
    title: note.ActionDescription && note.ActionDescription.trim(),
    noteType: note._JSON && note._JSON.note_type,
    note: note.CreationNotes && note.CreationNotes.trim(),
    tags: tagsToArray(note.tags),
    createdBy: note.userName,
  };
}

module.exports.query = query;
module.exports.queryNotes = queryNotes;
module.exports.transformNote = transformNote;
