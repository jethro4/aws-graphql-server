const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  const {appName, apiKey} = event.arguments;

  try {
    const response = await queryNoteTags(appName, apiKey);

    // console.log('Done queryNoteTags. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryNoteTags', err);
    callback(err);
  }
};

async function queryNoteTags(appName, apiKey) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/note/tags?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = [], total = 0} = json;
  const tags = [];

  for (var tag of data) {
    const {id, tag_slug: slug, tag_name: name} = tag;

    const hasValue = Boolean(removeTagSign(name));

    if (hasValue) {
      tags.push({id, slug, name});
    }
  }

  return {
    items: tags,
    count: tags.length,
    total: Number(total),
  };
}

function removeTagSign(tag) {
  return tag.replace(/^#+/i, '');
}
