const dayjs = require('dayjs');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {sortArrayByObjectKey} = require('../utils/array');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDOSchedulesUser(
      event.request.headers,
      event.arguments,
    );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDOSchedulesUser', err);

    callback(err);
  }
};

async function queryDOSchedulesUser(headers, params) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const currentYear = dayjs().year();
  const {schedulerId, contactId, dateStart, dateEnd} = Object.assign(
    {dateStart: '2000-01-01', dateEnd: `${currentYear + 1}-12-31`},
    params,
  );

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/scheduler_do?api_key=${apiKey}&sessionName=${sessionName}&schedulerId=${schedulerId}&contactId=${contactId}`;

  if (dateStart) {
    url += `&dateStart=${dateStart}`;
  }
  if (dateEnd) {
    url += `&dateEnd=${dateEnd}`;
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {total, schedulerData} = Object.assign(json, {
    total: Number(json.total),
  });
  const entries = Object.entries(schedulerData);

  const data = entries.reduce((acc, [date, items]) => {
    return acc.concat({
      date,
      items: items.map((item) => item.ItemId),
    });
  }, []);

  return {
    total,
    items: sortArrayByObjectKey(data, 'date'),
  };
}
