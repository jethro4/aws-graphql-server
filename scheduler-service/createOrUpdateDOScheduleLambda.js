const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateDOSchedule = async (event, context, callback) => {
  try {
    const postResponse = await postDOSchedule(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateDOSchedule. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateDOSchedule', err);
    callback(err);
  }
};

async function postDOSchedule(headers, params) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const isCreate = !params.id;

  const body = {
    api_key: apiKey,
    sessionName,
    action: isCreate ? 'create' : 'update',
    ...(!isCreate && {
      id: params.id,
    }),
    groupId: params.groupId,
    name: params.name,
    description: params.description,
    userRelationshipId: params.userRelationshipId,
    contactRelationshipId: params.contactRelationshipId,
    startDateFieldId: params.startDateFieldId,
    endDateFieldId: params.endDateFieldId,
    schedulerType: params.schedulerType,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/scheduler`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return {
    id: json.id || params.id,
    groupId: params.groupId,
    name: params.name,
    description: params.description,
    userRelationshipId: params.userRelationshipId,
    contactRelationshipId: params.contactRelationshipId,
    startDateFieldId: params.startDateFieldId,
    endDateFieldId: params.endDateFieldId,
    schedulerType: params.schedulerType,
  };
}
