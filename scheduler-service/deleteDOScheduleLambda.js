const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteDOSchedule = async (event, context, callback) => {
  try {
    const postResponse = await deleteDOScheduleFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteDOSchedule. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteDOSchedule', err);
    callback(err);
  }
};

async function deleteDOScheduleFunc(headers, params) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const body = {
    api_key: apiKey,
    sessionName,
    id: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/scheduler`;

  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  return {
    success: json.status,
    id: params.id,
  };
}
