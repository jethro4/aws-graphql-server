const dayjs = require('dayjs');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {sortArrayByObjectKey} = require('../utils/array');
const {parseJson} = require('../utils/string');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryTaskSchedulesUser(
      event.request.headers,
      event.arguments,
    );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryTaskSchedulesUser', err);

    callback(err);
  }
};

async function queryTaskSchedulesUser(headers, params) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const currentYear = dayjs().year();
  const {contactId, dateStart, dateEnd} = Object.assign(
    {dateStart: '2000-01-01', dateEnd: `${currentYear + 1}-12-31`},
    params,
  );

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/scheduler_task?api_key=${apiKey}&sessionName=${sessionName}&contactId=${contactId}`;

  if (dateStart) {
    url += `&dateStart=${dateStart}`;
  }
  if (dateEnd) {
    url += `&dateEnd=${dateEnd}`;
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {total, schedulerData} = Object.assign(json, {
    total: Number(json.total),
  });
  const entries = Object.entries(schedulerData);

  const data = entries.reduce((acc, [date, tasks]) => {
    return acc.concat({
      date,
      items: tasks.map((task) => {
        const creationDate = parseJson(task.CreationDate, task.CreationDate);
        const actionDate = parseJson(task.ActionDate, task.ActionDate);

        return {
          id: task.Id,
          userId: task.UserID,
          creationDate: creationDate.date || creationDate,
          contactId: task.ContactId,
          type: task.ActionType,
          actionDate: actionDate.date || actionDate,
          title: task.ActionDescription && task.ActionDescription.trim(),
          note: task.CreationNotes && task.CreationNotes.trim(),
        };
      }),
    });
  }, []);

  return {
    total,
    items: sortArrayByObjectKey(data, 'date'),
  };
}
