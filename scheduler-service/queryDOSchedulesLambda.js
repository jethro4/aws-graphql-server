const dayjs = require('dayjs');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDOSchedules(
      event.request.headers,
      event.arguments,
    );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDOSchedules', err);

    callback(err);
  }
};

async function queryDOSchedules(headers) {
  const {
    'app-name': appName,
    'app-api-key': apiKey,
    'session-id': sessionName,
  } = headers;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/scheduler?api_key=${apiKey}&sessionName=${sessionName}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = []} = json;
  const doSchedules = [];

  data.forEach((d) => {
    const doScheduleObj = {
      id: d.Id,
      groupId: d.GroupId,
      name: d.Name,
      description: d.Description,
      userRelationshipId: d.UserRelationshipId,
      contactRelationshipId: d.ContactRelationshipId,
      startDateFieldId: d.StartDateFieldId,
      endDateFieldId: d.EndDateFieldId,
      schedulerType: d.SchedulerType,
      dateCreated: dayjs(d.Created).toISOString(),
    };

    doSchedules.push(doScheduleObj);
  });

  return doSchedules;
}
