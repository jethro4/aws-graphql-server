const {query: queryDOSchedules} = require('./queryDOSchedulesLambda');
const {query: queryDOSchedulesUser} = require('./queryDOSchedulesUserLambda');
const {
  query: queryTaskSchedulesUser,
} = require('./queryTaskSchedulesUserLambda');
const {createOrUpdateDOSchedule} = require('./createOrUpdateDOScheduleLambda');
const {deleteDOSchedule} = require('./deleteDOScheduleLambda');

module.exports.scheduler = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'listDOSchedules':
      queryDOSchedules(...params);
      break;
    case 'listDOSchedulesUser':
      queryDOSchedulesUser(...params);
      break;
    case 'listTaskSchedulesUser':
      queryTaskSchedulesUser(...params);
      break;
    case 'createOrUpdateDOSchedule':
      createOrUpdateDOSchedule(...params);
      break;
    case 'deleteDOSchedule':
      deleteDOSchedule(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
