const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryUsers(event.request.headers);

    // console.log(
    //   'Done queryUsers. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryUsers', err);

    callback(err);
  }
};

async function queryUsers({'app-name': appName, 'app-api-key': apiKey}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/users?&api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = [], count = 0, total = 0} = json;
  const users = [];

  for (var user of data) {
    const userObj = {
      id: user.ContactId,
      email: user.Email,
      firstName: user.FirstName,
      lastName: user.LastName,
      templateId: user.TemplateId,
      contactLevel: user.ContactLevel,
    };

    users.push(userObj);
  }

  return {
    items: users,
    count: Number(count),
    total: Number(total),
  };
}
