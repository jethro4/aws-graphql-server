const {query: queryValidEmail} = require('./queryValidEmailLambda');
const {
  query: queryValidDomainEmails,
} = require('./queryValidDomainEmailsLambda');
const {query: queryUsers} = require('./queryUsersLambda');
const {createUser} = require('./createUserLambda');
const {updateUser} = require('./updateUserLambda');
const {deleteUser} = require('./deleteUserLambda');

module.exports.user = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'getValidEmail':
      queryValidEmail(...params);
      break;
    case 'listValidDomainEmails':
      queryValidDomainEmails(...params);
      break;
    case 'listUsers':
      queryUsers(...params);
      break;
    case 'createUser':
      createUser(...params);
      break;
    case 'updateUser':
      updateUser(...params);
      break;
    case 'deleteUser':
      deleteUser(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
