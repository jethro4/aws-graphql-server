const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryValidEmail(
      event.request.headers,
      event.arguments,
    );

    // console.log('Done queryValidEmail. response:', JSON.stringify(response));

    callback(null, response);
  } catch (err) {
    console.log('Error on queryValidEmail', err);
    callback(err);
  }
};

async function queryValidEmail(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/validate_email?api_key=${apiKey}&Email=${params.email}&userId=${params.userId}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  return {
    email: json?.email,
    user: json?.user,
    domain: json?.domain,
    role: json?.role,
    disposable: json?.disposable,
    free: json?.free,
    score: json?.score,
    updated: json?.updated,
    status: json?.status,
    didYouMean: json?.did_you_mean,
    formatValid: json?.format_valid,
    mxFound: json?.mx_found,
    smtpCheck: json?.smtp_check,
    catchAll: json?.catch_all,
  };
}
