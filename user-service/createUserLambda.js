const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createUser = async (event, context, callback) => {
  try {
    const postResponse = await createUserFunc(
      event.request.headers,
      event.arguments.input,
    );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createUser', err);
    callback(err);
  }
};

async function createUserFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    access_key: apiKey,
    email: params.email,
    firstname: params.firstName,
    lastname: params.lastName,
    templateid: params.templateId,
    ...(params.contactLevel && {
      userLevel: params.contactLevel,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {
    id: params.contactId,
    email: params.email,
    firstName: params.firstName,
    lastName: params.lastName,
    templateId: params.templateId,
    contactLevel: params.contactLevel,
  };
}
