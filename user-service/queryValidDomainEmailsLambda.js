const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {parseJson} = require('../utils/string');

module.exports.query = async (event, context, callback) => {
  try {
    const response = await queryValidDomainEmails(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryValidDomainEmails. response:',
    //   JSON.stringify(response),
    // );

    callback(null, response);
  } catch (err) {
    console.log('Error on queryValidDomainEmails', err);
    callback(err);
  }
};

module.exports.transformDomain = transformDomain;

async function queryValidDomainEmails(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    ...(params.domain && {
      domainid: params.domain,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/validate_email`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const err = json.record?.errors?.[0];

  if (err) {
    throw new ValidDomainsQueryException(err.message);
  }

  const {record = []} = json;
  const items = [];
  const data = parseJson(record);

  data.forEach((item) => {
    items.push(transformDomain(item));
  });

  return items;
}

function transformDomain(item = {}) {
  return {
    domain: item.domain,
    username: item.username,
    valid: item.valid,
    subdomain: item.subdomain,
    legacy: item.legacy,
    dns: item.dns ? Object.values(item.dns) : [],
  };
}

class ValidDomainsQueryException extends Error {
  constructor(message) {
    super(message);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ValidDomainsQueryException);
    }
    this.name = 'ValidDomainsQueryException';
  }
}
