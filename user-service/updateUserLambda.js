const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.updateUser = async (event, context, callback) => {
  try {
    const postResponse = await updateUserFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log('Done updateUser. postResponse:', JSON.stringify(postResponse));

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on updateUser', err);
    callback(err);
  }
};

async function updateUserFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    templateId: params.templateId,
    userLevel: params.contactLevel,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user/edit/${params.id}`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {
    id: params.id,
    templateId: params.templateId,
    contactLevel: params.contactLevel,
  };
}
