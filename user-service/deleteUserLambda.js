const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteUser = async (event, context, callback) => {
  try {
    const postResponse = await deleteUserFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log('Done deleteUser. postResponse:', JSON.stringify(postResponse));

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteUser', err);
    callback(err);
  }
};

async function deleteUserFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    userContactId: params.id,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/user`;

  const response = await http.delete(url, body);

  await getJsonResponse(response);

  return {
    success: true,
    id: params.id,
  };
}
