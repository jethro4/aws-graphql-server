const {query} = require('../contact-service/queryContactMetadataLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_9bd202a08f8c2146',
    },
  },
  arguments: {
    email: 'pieter@macanta.org',
  },
};

query(payload, {}, () => {});
