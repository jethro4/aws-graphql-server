const {
  postTriggerCondition,
} = require('../automation-service/postTriggerConditionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_aa9827389274ef5c',
      id: 'tc_d33d6e6febef0c7f',
      name: 'Test Create 3 Edited',
      description: 'Test 3',
      doType: 'Macanta Installs',
      doConditions: [
        {
          fieldName: 'App Name',
          logic: '',
          operator: 'is',
          value: 'Test 3',
        },
        {
          fieldName: 'Last Invoice Value',
          logic: 'or',
          operator: '~>~',
          value: '0',
        },
      ],
      contactConditions: [
        // {
        //   relationship: 'Macanta User',
        //   logic: '',
        // },
      ],
      userConditions: [
        {
          relationship: 'Macanta User',
          logic: '',
          operator: 'is',
          userId: '554840',
        },
      ],
    },
  },
};

postTriggerCondition(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
