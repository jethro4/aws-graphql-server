const {updateAppSettings} = require('../app-service/updateAppSettingsLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      type: 'ui_colour',
      value: '#663399',

      // fileName: 'Macanta-Logo.jpg',
      // mimeType: 'image/jpeg',
      // type: 'macanta_custom_logo',
      // value: '<image_base64_value>'
    },
  },
};

updateAppSettings(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
