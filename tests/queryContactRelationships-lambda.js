const {
  query,
} = require('../relationship-service/queryContactRelationshipsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    type: 'direct',
    // type: 'indirect',
    id: '555043',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
