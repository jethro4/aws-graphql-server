const {query} = require('../admin-service/queryWidgetCountsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_673f13c9b9a39288',
      force: true,
    },
  },
  arguments: {
    userId: '554840',
    queryId: 'qry_4bAxCdARuXKYboI1',
  },
};

query(payload, {}, () => {});
