const {
  deleteQueryBuilder,
} = require('../admin-service/deleteQueryBuilderLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      queryId: 'qry_3fd5551cc9b1a768',
    },
  },
};

deleteQueryBuilder(payload, {}, () => {});
