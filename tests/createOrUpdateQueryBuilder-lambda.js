const {
  createOrUpdateQueryBuilder,
} = require('../admin-service/createOrUpdateQueryBuilderLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      // id: 'qry_26bd363feb95bc3d',
      name: 'Test Create 3',
      description: 'Test 3',
      doType: 'Macanta Installs',
      doConditions: [
        {
          name: 'App Name',
          logic: '',
          operator: 'is',
          value: 'Test 3',
        },
        {
          name: 'Last Invoice Value',
          logic: 'or',
          operator: '~>~',
          value: '0',
        },
      ],
      contactConditions: [
        // {
        //   relationship: 'Macanta User',
        //   logic: '',
        // },
      ],
      userConditions: [
        {
          relationship: 'Macanta User',
          logic: '',
          operator: 'is',
          userId: '554840',
        },
      ],
      chosenFields: ['App Name', 'FirstName'],
      criteriaMessageA: 'Msg A',
      criteriaMessageB: 'Msg B',
    },
  },
};

createOrUpdateQueryBuilder(payload, {}, () => {});
