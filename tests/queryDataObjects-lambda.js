const {query} = require('../data-object-service/queryDataObjectsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_25008c16c6112b7a',
    },
  },
  arguments: {
    q: 'Phoenix',
    groupId: 'ci_kbgaluau',
    contactId: '554840',
    relationship: 'Prospect',
    page: 0,
    limit: 25,
    order: 'desc',
    orderBy: 'Opportunity Type',
    contactField: true,
    filter:
      '[{"queryCDFieldLogic":"and","queryCDFieldName":"Opportunity Stage","queryCDFieldOperator":"contains","queryCDFieldValue":"No Call Scheduled","queryCDFieldValues":null}]',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
