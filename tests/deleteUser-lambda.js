const {deleteUser} = require('../user-service/deleteUserLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: '554906',
    },
  },
};

deleteUser(payload, {}, () => {});
