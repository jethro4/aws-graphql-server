const {
  postFieldAction,
} = require('../automation-service/postFieldActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'fa_9954e07eba66fbfc',
      name: 'Field Action Edited',
      description: 'Test 3',
      doType: 'Macanta Installs',
      actionType: 'Addition',
      fieldValues: {
        queryCDFieldName1: 'App Name',
        queryCDFieldName2: 'Field 2',
        queryCDFieldNameResult: 'Field 3',
      },
      contactRelationship: 'Macanta User',
    },
  },
};

postFieldAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
