const {
  deleteWorkflowBoard,
} = require('../admin-service/deleteWorkflowBoardLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 'pm_7159896675446e78',
    },
  },
};

deleteWorkflowBoard(payload, {}, () => {});
