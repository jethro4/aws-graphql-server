const {query} = require('../app-service/queryAppSettingsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
    },
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
