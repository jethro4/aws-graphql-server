const {
  connectContactDirectRelationship,
} = require('../relationship-service/connectContactDirectRelationshipLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      contactId: '555043',
      relationId: 're_5328bc34a0a5d176',
      relationships: [
        {
          contactId: '554840',
          relationId: 're_dcc78bbc79d8ffe6',
        },
      ],
    },
  },
};

connectContactDirectRelationship(payload, {}, () => {});
