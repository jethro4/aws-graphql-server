const {query} = require('../data-object-service/queryRelationshipsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    groupId: 'ci_k30e5zmn',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
