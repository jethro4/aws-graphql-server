const {
  createOrUpdateFormBuilder,
} = require('../admin-service/createOrUpdateFormBuilderLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_cfc7c7860f51de48',
    },
  },
  arguments: {
    input: {
      // id: 'frm_39f3030b64763fd7',
      title: 'Test 2',
      description: '',
      thankMessage: '',
      type: 'Integrated',
      contactGroups: [
        {
          subGroupName: 'General',
          fields: [
            {
              fieldId: 'field_email',
              label: 'Email',
              tooltip: 'This will be used as your ID',
              required: true,
              column: 1,
            },
          ],
          columns: 3,
        },
      ],
      dataObject: {
        dataObjectId: 'ci_k30e5zmn',
        dataObjectGroups: [
          {
            subGroupName: 'General',
            fields: [
              {
                fieldId: 'field_integration',
                label: 'Sample Ingegration Field',
                required: true,
                tooltip: 'Required',
                column: 1,
              },
            ],
            columns: 2,
          },
        ],
        relationships: ['re_34cc668d5636f556', 're_295d08cb3ce03081'],
        integrationField: {
          limit: 4,
          displayFields: '~CD.App Name~ with ~CD.Status~ (End)',
          type: 'Radio',
          order: 'DESC',
          orderBy: 'App Name',
          conditions: [
            {
              name: 'App Name',
              logic: 'AND',
              operator: 'contains',
              values: null,
              value: 'test',
            },
            {
              name: 'Status',
              logic: 'OR',
              operator: 'contains',
              values: ['Active Subscription'],
              value: null,
            },
          ],
        },
      },
      customCSS: '',
    },
  },
};

createOrUpdateFormBuilder(payload, {}, () => {});
