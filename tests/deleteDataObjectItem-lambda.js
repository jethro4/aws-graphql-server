const {
  deleteDataObjectItem,
} = require('../data-object-service/deleteDataObjectItemLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 'item_67b0a8f707c65432',
    },
  },
};

deleteDataObjectItem(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
