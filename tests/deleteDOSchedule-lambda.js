const {
  deleteDOSchedule,
} = require('../scheduler-service/deleteDOScheduleLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_5e321825d9a49310',
    },
  },
  arguments: {
    input: {
      id: '8',
    },
  },
};

deleteDOSchedule(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
