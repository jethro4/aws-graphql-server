const {postSmsAction} = require('../automation-service/postSmsActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'sms_2dfceb6b8a0cc827',
      name: 'Test SMS Action Edited',
      description: 'Testing POST SMS Lambda Function',
      phoneField: 'Phone2',
      message: 'Test Message',
    },
  },
};

postSmsAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
