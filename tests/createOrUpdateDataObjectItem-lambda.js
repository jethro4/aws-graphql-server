const {
  createOrUpdateDataObjectItem,
} = require('../data-object-service/createOrUpdateDataObjectItemLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      groupId: 'ci_k30e5zmn',
      // doItemId: 'item_ea9e138d4d4d64f7',
      contactId: '554840',
      relationships: ['Macanta User', 'App Owner'],
      fields: [
        {key: 'App Name', value: 'Test 4'},
        {key: 'Opt-In', value: 'Yes'},
      ],
    },
  },
};

createOrUpdateDataObjectItem(payload, {}, () => {});
