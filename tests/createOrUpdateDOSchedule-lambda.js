const {
  createOrUpdateDOSchedule,
} = require('../scheduler-service/createOrUpdateDOScheduleLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_5e321825d9a49310',
    },
  },
  arguments: {
    input: {
      id: '2',
      title: 'Last Login Scheduler - Edited',
      description: 'This Shows User When Their Last Login - Edited',

      // name: 'Create New 1',
      // description: 'Created 1',
      // groupId: 'ci_aa82f4c085572d46',
      // contactRelationshipId: 're_c86805cd7628c24e',
      // userRelationshipId: 're_05cef39a0d219159',
      // startDateFieldId: 'field_c8e8c4e22919e8a7',
      // endDateFieldId: 'field_5178dc4a348e654a',
      // schedulerType: 'DateTime',

      // name: 'Create New 2',
      // description: 'Created 2',
      // groupId: 'ci_aa82f4c085572d46',
      // contactRelationshipId: 're_c86805cd7628c24e',
      // userRelationshipId: 're_05cef39a0d219159',
      // startDateFieldId: 'field_083b72c4c8ec75d0',
      // endDateFieldId: '',
      // schedulerType: 'Date',
    },
  },
};

createOrUpdateDOSchedule(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
