const {
  emailContactSupport,
} = require('../communication-service/emailContactSupportLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_4989c84cf182e10b',
    },
  },
  arguments: {
    input: {
      subject: 'Test subject',
      message: 'Test message',
    },
  },
};

emailContactSupport(payload, {}, () => {});
