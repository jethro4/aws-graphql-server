const {
  connectDOToDORelationship,
} = require('../relationship-service/connectDOToDORelationshipLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      doItemId: 'item_9a3cd76a3be6f004',
      groupId: 'ci_4458c47816f984ed',
      data: [
        {
          doItemId: 'item_402f2c9b68f19a98',
          groupId: 'ci_4458c47816f984ed',
        },
      ],
    },
  },
};

connectDOToDORelationship(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
