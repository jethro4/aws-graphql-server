const {
  mergeContactDuplicates,
} = require('../contact-service/mergeContactDuplicatesLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      primaryContactId: '554883',
      duplicateContactIds: ['554880'],
      dataObjectActions: [
        {
          title: 'Macanta Installs',
          action: 'transferWithNewRelationship',
          relationship: 'App Owner',
        },
      ],
      noteTaskAction: 'transferAll',
    },
  },
};

mergeContactDuplicates(payload, {}, () => {});
