const {
  deleteContactDirectRelationship,
} = require('../relationship-service/deleteContactDirectRelationshipLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      contactId: '554840',
      relatedContactId: '554925',
    },
  },
};

deleteContactDirectRelationship(payload, {}, () => {});
