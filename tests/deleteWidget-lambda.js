const {deleteWidget} = require('../admin-service/deleteWidgetLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      id: 'test',
      userId: '554840',
      queryId: 'qry_3lAJeWDlrtQHQ62T',
    },
  },
};

deleteWidget(payload, {}, () => {});
