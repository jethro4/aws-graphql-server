const {updateUser} = require('../user-service/updateUserLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: '554838',
      templateId: '1',
      contactLevel: 'Admin',
      // templateId: '6',
      // contactLevel: 'User',
    },
  },
};

updateUser(payload, {}, () => {});
