const {
  createOrUpdateDOSectionCondition,
} = require('../data-object-service/createOrUpdateDOSectionConditionLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      dataObjectName: 'Jethro Object 2 Edited 1',
      dataObjectId: 'ci_df532061be958d16',
      sections: [
        {
          sectionName: 'General 2',
          conditions: [
            {
              name: 'Test Decimal',
              logic: '',
              operator: '',
              value: '2',
              values: null,
            },
            {
              name: 'Test Whole Number',
              logic: 'and',
              operator: '',
              value: '5',
              values: null,
            },
          ],
        },
        {
          sectionName: 'General 3',
          conditions: [
            {
              name: 'Test Checkbox Choices 1 Edited 2',
              logic: '',
              operator: '',
              value: null,
              values: ['Jet 03'],
            },
          ],
        },
      ],
    },
  },
};

createOrUpdateDOSectionCondition(payload, {}, () => {});
