const {
  postWebhookAction,
} = require('../automation-service/postWebhookActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'aw_e16f2b48dc82dd41',
      name: 'WebHook Lambda Test Edited',
      description: 'WebHook from Lambda ',
      doType: 'Macanta Installs',
      postURL: 'https://crm.macantacrm.com',
      fieldKeyValue: [
        {
          fieldName: 'App Name',
          value: 'Test 3',
        },
        {
          fieldName: 'Last Invoice Value',
          value: '0',
        },
      ],
      customKeyValue: [
        {
          fieldName: 'Some Field',
          value: 'Test 3',
        },
        {
          fieldName: 'Some Field 2',
          value: '0',
        },
      ],
      contactConditions: [
        {
          relationship: 'Prospect',
          logic: '',
        },
      ],
    },
  },
};

postWebhookAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
