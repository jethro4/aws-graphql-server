const {
  createOrUpdateListAssignment,
} = require('../admin-service/createOrUpdateListAssignmentLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',

      queryId: 'qry_jeL0kJPCaWisj1ak',
      assignmentType: 'roundRobin',
      unassignmentBehavior: 'noAction',
      selectedRoundRobin: 'Round Robin Test',
      selectedUsers: [],
      assignedRelationship: 'Initial Call Host',
      status: 'inactive',

      // queryId: 'qry_4bAxCdARuXKYboI1',
      // assignmentType: 'selectedUsers',
      // unassignmentBehavior: 'unAssignWhenDORemoved',
      // selectedRoundRobin: '',
      // selectedUsers: ['554840', '1311'],
      // assignedRelationship: 'Macanta User',
    },
  },
};

createOrUpdateListAssignment(payload, {}, () => {});
