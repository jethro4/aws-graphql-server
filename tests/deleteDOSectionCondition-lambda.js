const {
  deleteDOSectionCondition,
} = require('../data-object-service/deleteDOSectionConditionLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      dataObjectId: 'ci_83ab48a72f4471b9',
      sectionName: 'Test',
    },
  },
};

deleteDOSectionCondition(payload, {}, () => {});
