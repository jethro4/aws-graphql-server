const {
  deleteDataObjectField,
} = require('../data-object-service/deleteDataObjectFieldLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 'field_5af02df111ca957b',
    },
  },
};

deleteDataObjectField(payload, {}, () => {});
