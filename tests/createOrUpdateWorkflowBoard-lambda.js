const {
  createOrUpdateWorkflowBoard,
} = require('../admin-service/createOrUpdateWorkflowBoardLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_cf87f73284d4d143',
    },
  },
  arguments: {
    input: {
      id: 'pm_Go0uAYwER0iQI30s',
      title: 'Test Macanta Installs board 1 Edited 2',
      description: 'For testing only',
      type: 'Macanta Installs',
      displayFields: ['FirstName', 'LastName', 'App Name', 'Status'],
      keyField: 'Status',
      relationship: 'Macanta User',
      activeColumnsColor: 'active',
    },
  },
};

createOrUpdateWorkflowBoard(payload, {}, () => {});
