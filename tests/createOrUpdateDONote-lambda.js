const {
  createOrUpdateDONote,
} = require('../data-object-service/createOrUpdateDONoteLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      id: 334,
      doItemId: 'item_3615496cf47ff9e5',
      type: 'General',
      title: 'Test DO 17',
      note: 'Edited 3',
      tags: ['test', 'test2'],
      sessionName: 'sess_49b27c7359b2da94',
    },
  },
};

createOrUpdateDONote(payload, {}, () => {});
