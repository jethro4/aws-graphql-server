const {
  createOrUpdateContact,
} = require('../contact-service/createOrUpdateContactLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_01ece595de5fb5f9',
    },
  },
  arguments: {
    input: {
      // id: '554840',
      // email: 'temp1@gmail.com',
      autoGenerateEmail: true,
      firstName: 'First3',
      lastName: 'Last3',
      // phoneNumbers: ['639159897270', '07715102114'],
      // phoneNumberExts: ['12', '345'],
    },
  },
};

createOrUpdateContact(payload, {}, () => {});
