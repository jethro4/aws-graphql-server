const {
  duplicatePermissionTemplate,
} = require('../permission-service/duplicatePermissionTemplateLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: '13',
    },
  },
};

duplicatePermissionTemplate(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
