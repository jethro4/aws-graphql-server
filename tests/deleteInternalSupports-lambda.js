const {
  deleteInternalSupports,
} = require('../admin-service/deleteInternalSupportsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_4989c84cf182e10b',
    },
  },
  arguments: {
    input: {
      emails: ['geover@gmail.com'],
    },
  },
};

deleteInternalSupports(payload, {}, () => {});
