const {
  createOrUpdateEmailSignature,
} = require('../communication-service/createOrUpdateEmailSignatureLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      userId: '554840',
      signature: 'Jethro Estrada signature edit 2',
    },
  },
};

createOrUpdateEmailSignature(payload, {}, () => {});
