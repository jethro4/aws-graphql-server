const {deleteFormBuilder} = require('../admin-service/deleteFormBuilderLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_07b93c4b788e1798',
    },
  },
  arguments: {
    input: {
      id: 'frm_e375582cf4459fbc',
    },
  },
};

deleteFormBuilder(payload, {}, () => {});
