const {query} = require('../data-object-service/queryDataObjectFieldsLambda');

const payload = {
  arguments: {
    apiKey: '1552679244144731',
    appName: 'staging',
    groupId: 'ci_kc2bram8',
    userEmail: 'jethro@macantacrm.com',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
