const {
  deleteNotifications,
} = require('../notification-service/deleteNotificationsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 222,
      userId: 554840,
      // type: 'Task',
    },
  },
};

deleteNotifications(payload, {}, () => {});
