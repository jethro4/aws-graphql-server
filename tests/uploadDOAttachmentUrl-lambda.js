const {
  uploadDOAttachmentUrl,
} = require('../data-object-service/uploadDOAttachmentUrlLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      url: 'https://f000.backblazeb2.com/file/macantacrm-connected-data-attachments/staging/item_81a581ce4ba56ef6/Screen-Shot-2021-05-11-at-12.51.48-PM.png',
      itemId: 'item_51b0c388886d074c',
      fileName: 'Test 22',
    },
  },
};

uploadDOAttachmentUrl(payload, {}, () => {});
