const {
  postTriggerAction,
} = require('../automation-service/postTriggerActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'ta_7a8b3836701eeda7',
      name: 'Test Trigger Action Edited',
      description: 'Trigger Action from Lambda Edited',
      doType: 'Macanta Installs',
      emailActionId: 'ea_lGgLyMZOAj16iB6f',
      smsActionId: 'sms_asdaswq21312',
      fieldActionId: 'fa_sadqwe1234',
      contactActionId: 'ca_123456asdfghj',
      userActionId: 'ua_smzuhIaUZyHpJ2Wi',
      webhookActionId: 'wa_asdasdqwe12sw',
      doConditions: [
        {
          fieldName: 'App Name',
          value: 'Test 3',
        },
        {
          fieldName: 'Last Invoice Value',
          value: '0',
        },
      ],
      contactConditions: [
        {
          relationship: 'Prospect',
          logic: '',
        },
      ],
    },
  },
};

postTriggerAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
