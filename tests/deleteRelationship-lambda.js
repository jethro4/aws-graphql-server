const {
  deleteRelationship,
} = require('../data-object-service/deleteRelationshipLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 're_caf7e30c598a3b9a',
    },
  },
};

deleteRelationship(payload, {}, () => {});
