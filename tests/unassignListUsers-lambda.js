const {unassignListUsers} = require('../admin-service/unassignListUsersLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      queryId: 'qry_jeL0kJPCaWisj1ak',
      action: 'active',
    },
  },
};

unassignListUsers(payload, {}, () => {});
