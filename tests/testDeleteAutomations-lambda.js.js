const {
  deleteAutomations,
} = require('../automation-service/deleteAutomationsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 'ta_7a8b3836701eeda7',
    },
  },
};

deleteAutomations(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
