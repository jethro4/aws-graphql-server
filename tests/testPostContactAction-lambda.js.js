const {
  postContactAction,
} = require('../automation-service/postContactActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'ca_8de9cf9eb0427f82',
      name: 'Contact Action API Test Edited',
      description: 'Description Test',
      doType: 'Macanta Installs',
      actionType: 'Note',
      fieldValues: {
        noteTitle: 'First Lambda Note',
        noteTags: '#test,#general',
        noteText: 'Note to self - sleep',
      },
    },
  },
};

postContactAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
