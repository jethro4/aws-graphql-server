const {
  deletePermissionTemplate,
} = require('../permission-service/deletePermissionTemplateLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: '25',
    },
  },
};

deletePermissionTemplate(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
