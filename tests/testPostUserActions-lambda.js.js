const {postUserAction} = require('../automation-service/postUserActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'ua_a08f1e9a3ec11d28',
      name: 'Notify User Edited',
      description: 'Notify user for some item modification',
      doType: 'Memberships',
      contactRelationship: 'Primary Member',
      macantaUserId: '1366',
      message: 'This is the message to notify user',
    },
  },
};

postUserAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
