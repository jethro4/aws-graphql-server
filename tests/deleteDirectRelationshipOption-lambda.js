const {
  deleteDirectRelationshipOption,
} = require('../relationship-service/deleteDirectRelationshipOptionLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      relationId: 're_d7eb4fae5c9e2522',
    },
  },
};

deleteDirectRelationshipOption(payload, {}, () => {});
