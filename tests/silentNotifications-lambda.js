const {
  silentNotifications,
} = require('../notification-service/silentNotificationsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      userId: 554840,
      // duration: 60,
      type: 'task',
      action: 'off',
    },
  },
};

silentNotifications(payload, {}, () => {});
