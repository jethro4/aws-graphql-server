const {
  createOrUpdateNote,
} = require('../note-task-service/createOrUpdateNoteLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      // id: '81602',
      contactId: 554840,
      title: 'Create note with session 5',
      note: 'test',
      tags: ['test'],
      sessionName: 'sess_eb7a405f9333badb',
    },
  },
};

createOrUpdateNote(payload, {}, () => {});
