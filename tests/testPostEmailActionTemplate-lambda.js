const {
  postEmailActionTemplate,
} = require('../automation-service/postEmailActionTemplateLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      //id:"ea_zOmUDoWANnJGPcJc",
      name: 'Added Via Lambda',
      compiledEmailHTML: 'PCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURCBY4=',
      emailHTML: 'PCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURCBYSFRNTCAxLj=',
      emailCSS: 'LyogQ09ORklHIFNUWUxFUyBQbGVhc2UgZG8gbm90IGRlbGV0ZSBhbmQgZW',
      templateHTML: 'PCFET0NUWVBFIGh0bWwgUFVCTElDICItLy9XM0MvL0RURC=',
      templateCSS:
        'LyogQ09ORklHIFNUWUxFUyBQbGVhc2UgZG8gbm90IGRlbGV0ZSBhbmQgZWRpdCBDU1=',
    },
  },
};

postEmailActionTemplate(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
