const {query} = require('../scheduler-service/queryTaskSchedulesUserLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_8e8ca57eab5b19b5',
    },
  },
  arguments: {
    contactId: '554840',
    dateStart: '2022-10-01',
    dateEnd: '2022-10-31',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
