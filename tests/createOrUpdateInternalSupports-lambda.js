const {
  createOrUpdateInternalSupports,
} = require('../admin-service/createOrUpdateInternalSupportsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_4989c84cf182e10b',
    },
  },
  arguments: {
    input: {
      internalSupports: [
        {
          email: 'geover@gmail.com',
          fullName: 'Geover Zamora',
        },
      ],
    },
  },
};

createOrUpdateInternalSupports(payload, {}, () => {});
