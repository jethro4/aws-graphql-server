const {
  createOrUpdatePermissionTemplate,
} = require('../permission-service/createOrUpdatePermissionTemplateLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: '14',
      name: 'Test 30',
      description: 'Edit 3',
      data: [
        {
          key: 'tabAccess',
          jsonValue:
            '{"notes_tab":"ReadWrite","com_history":"ReadWrite","ci_k30e5zmn":"ReadOnly"}',
        },
        {
          key: 'sectionAccess',
          jsonValue:
            '{"ci_kbgaluau":{"Status Tracking":"ReadWrite","Tracking":"ReadOnly"}}',
        },
        {
          key: 'mqbAccess',
          jsonValue:
            '{"qry_jeL0kJPCaWisj1ak":"ReadWrite","qry_4bAxCdARuXKYboI1":"ReadWrite"}',
        },
        {
          key: 'taskAccess',
          jsonValue:
            '{"OwnTask":"yes","AllUserTask":"no","SpecificUserTask":"yes","UserTask":["554721","554838"]}',
        },
        {
          key: 'inlineEmailTemplateAccess',
          jsonValue:
            '{"1":"Pieter Emails","2":"Group B","5":"Duplicate Emails"}',
        },
        {
          key: 'mqbWidgetAccess',
          jsonValue:
            '{"qry_jeL0kJPCaWisj1ak":"checked","qry_4bAxCdARuXKYboI1":"unchecked"}',
        },
        {
          key: 'mqbWidgetEmail',
          jsonValue:
            '{"qry_jeL0kJPCaWisj1ak":"unchecked","qry_4bAxCdARuXKYboI1":"unchecked"}',
        },
        {
          key: 'contactViewPermissions',
          jsonValue:
            '{"ci_k30e5zmn":{"TypeA":"ReadWrite"},"ci_kbgaluau":{"TypeA":"ReadOnly"},"ci_kc2bram8":{"TypeA":"ReadWrite"},"Global":"ReadWrite","ci_0cb6261e4aa4e881":{"TypeA":"ReadWrite"}}',
        },
        {
          key: 'workflowBoards',
          jsonValue:
            '{"pm_7bfa2f52a0c3ef40":"All","pm_e4c507fda86da471":"CurrentUser"}',
        },
        {
          key: 'permissionMeta',
          jsonValue:
            '{"ActionButton":{"ci_0cb6261e4aa4e881":"Show","ci_a9483ed9b23ae8ba":"Show","ci_d3b6ac3d9d2e2ef1":"Hidden","ci_e5b7f5c869ee64f4":"Show","ci_k30e5zmn":"Show","ci_kbgaluau":"Show","ci_kc2bram8":"Show","ci_keoawaqy":"Show","ci_kkcu0uyc":"Show"},"ConnectButton":{"ci_0cb6261e4aa4e881":"Show","ci_a9483ed9b23ae8ba":"Hidden","ci_d3b6ac3d9d2e2ef1":"Show","ci_e5b7f5c869ee64f4":"Hidden","ci_k30e5zmn":"Show","ci_kbgaluau":"Show","ci_kc2bram8":"Show","ci_keoawaqy":"Show","ci_kkcu0uyc":"Show"},"PurchaseButton":"Hidden","AllowEmailSync":"yes","AllowAddRemoveEmailDomainFilter":"yes","AllowUserToMergeContacts":"yes","AllowUserDeleteContact":"yes","AllowUserDeleteDOItem":"yes","AllowUserAttachment":"yes","AllowUserDeleteDOAttachment":"yes","AllowContactExport":"yes","AllowDataObjectExport":"yes","AllowNoteTaskExport":"yes","AllowAddDirectRelationship":"yes","UserDOEditPermission":{"WithOtherUserDataToggle":"yes","WithOtherUserRelationshipToggle":"yes","WithOtherUserRelationshipDataToggle":"yes","WithOtherUserRelationshipRelationshipToggle":"yes","OtherUserRelationship":["re_09a525c71679400d","re_0ede51667d7844a3","re_1a6cb6ced7b3c8aa"]}}',
        },
      ],
    },
  },
};

createOrUpdatePermissionTemplate(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
