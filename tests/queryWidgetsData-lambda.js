const {query} = require('../admin-service/queryWidgetsDataLambda');

const payload = {
  arguments: {
    apiKey: '1552679244144731',
    appName: 'staging',
    limit: 25,
    page: 0,
    q: '',
    queryId: 'qry_4bAxCdARuXKYboI1',
    range: 'main',
    type: 'MacantaQuery',
    userId: '554840',
    itemId: 'item_68d44fa78d537ad7',
  },
};

query(payload, {}, () => {});
