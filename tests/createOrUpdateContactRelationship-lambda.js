const {
  createOrUpdateContactRelationship,
} = require('../relationship-service/createOrUpdateContactRelationshipLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      name: 'Direct Rel 4 - Edited',
      description: 'Edited 1',
      id: 're_dcc78bbc79d8ffe6',
    },
  },
};

createOrUpdateContactRelationship(payload, {}, () => {});
