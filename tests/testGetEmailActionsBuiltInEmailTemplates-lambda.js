const {
  query,
} = require('../automation-service/getEmailActionsBuiltInEmailTemplatesLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      id: 'default-macanta',
    },
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
