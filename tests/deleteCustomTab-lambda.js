const {deleteCustomTab} = require('../admin-service/deleteCustomTabLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 'ct_ed79d6196b7880d9',
    },
  },
};

deleteCustomTab(payload, {}, () => {});
