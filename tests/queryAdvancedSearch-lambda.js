const {query} = require('../admin-service/queryAdvancedSearchLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'tk217',
      'app-api-key': 'grywe6b7m3ljn08fzs5q2oau4x9t1kdi',
      'session-id': 'sess_c2c3bbdfb79b4bda',
    },
  },
  arguments: {
    chosenFields:
      '["FirstName","LastName","Email","Phone1","Renewal Rate","Membership End Date","Membership Type","Next Action Date","Next Action Note","Promotion Status"]',
    contactConditions:
      '[{"id":1645500919840,"relationship":"Primary Member","logic":""}]',
    doConditions:
      '[{"id":1645500919840,"name":"Membership End Date","logic":"","operator":"~<~","values":null,"value":"46 days"},{"id":1645500919841,"name":"Membership Status","logic":"and","operator":"","values":["Active"],"value":""},{"id":1645500919842,"name":"Membership Type","logic":"and","operator":"","values":["Preferred"],"value":""}]',
    doType: 'Memberships',
    userConditions: '[]',
  },
};

query(payload, {}, () => {});
