const {
  deleteDOToDORelationship,
} = require('../relationship-service/deleteDOToDORelationshipLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      groupId: 'ci_ea6d915d5ad9acee',
      itemIdA: 'item_9c670e5e33f0a75e',
      itemIdB: 'item_9892d0f6113b155b',
    },
  },
};

deleteDOToDORelationship(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
