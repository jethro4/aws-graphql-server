const {
  connectOrDisconnectRelationship,
} = require('../data-object-service/connectOrDisconnectRelationshipLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      doItemId: 'item_5de99eced46a5270',
      contactId: '554840',
      groupId: 'ci_k30e5zmn',
      // role: 'Macanta User',
      roles: ['Macanta User'],
    },
  },
};

connectOrDisconnectRelationship(payload, {}, () => {});
