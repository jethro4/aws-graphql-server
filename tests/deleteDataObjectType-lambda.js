const {
  deleteDataObjectType,
} = require('../data-object-service/deleteDataObjectTypeLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      id: 'ci_36eafed20439dd49',
    },
  },
};

deleteDataObjectType(payload, {}, () => {});
