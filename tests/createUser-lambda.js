const {createUser} = require('../user-service/createUserLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    input: {
      contactId: '1000018',
      email: 'macantaemailtesting+test148257@gmail.com',
      firstName: 'Pieter',
      lastName: 'de Villiers',
      templateId: '6',
      contactLevel: 'User',
      // templateId: '1',
      // contactLevel: 'Admin',
    },
  },
};

createUser(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
