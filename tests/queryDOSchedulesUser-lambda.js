const {query} = require('../scheduler-service/queryDOSchedulesUserLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
      'session-id': 'sess_525f54a37446be99',
    },
  },
  arguments: {
    schedulerId: '2',
    contactId: '554889',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
