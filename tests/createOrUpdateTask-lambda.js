const {
  createOrUpdateTask,
} = require('../note-task-service/createOrUpdateTaskLambda');

const payload = {
  arguments: {
    input: {
      // id: "272",
      appName: 'staging',
      apiKey: '1552679244144731',
      contactId: 1311,
      title: 'CREATE Test TASK 2',
      note: 'test',
      tags: ['test'],
      actionDate: '2021-08-02 5:45AM',
      userEmail: 'jethroestrada237@gmail.com',
      sessionName: 'sess_eb7a405f9333badb',
    },
  },
};

createOrUpdateTask(payload, {}, () => {});

// {
//     "api_key": "qpj91vodlgbxf3cekrnw7zih4ys0825t",
//     "contactId":9,
//     "task_note":"Task Content",
//     "task_description":"Task Title",
//     "action_date":"2020-07-06 5:45PM",
//     "user_email":"jethro@macantacrm.com",
//     "tags":"#task",
// }
