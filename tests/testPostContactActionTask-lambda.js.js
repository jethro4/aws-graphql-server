const {
    postContactAction,
} = require('../automation-service/postContactActionLambda');

const payload = {
    arguments: {
        input: {
            appName: 'staging',
            apiKey: '1552679244144731',
            sessionName: 'sess_dfe281f21478f09a',
            //id: 'ca_8de9cf9eb0427f82',
            name: 'Contact Action Task API Test',
            description: 'Description Test',
            doType: 'Macanta Installs',
            actionType: 'Task',
            fieldValues:
                {
                    taskTitle: 'First API Task',
                    taskDate: '3 Days',
                    taskText: 'I just wanted to say Hi!',
                    assignType: 'User',
                    assignTypeValue: 'Geover Zamora',
                }
        },
    },
};

postContactAction(payload, {}, (err, res) => {
    console.log(JSON.stringify(res));
});
