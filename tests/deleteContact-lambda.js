const {deleteContact} = require('../contact-service/deleteContactLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      id: 554909,
    },
  },
};

deleteContact(payload, {}, () => {});
