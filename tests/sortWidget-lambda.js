const {sortWidgets} = require('../admin-service/sortWidgetsLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      userId: '554840',
      widgetsOrder: [
        'Jethro Query test1 State Test',
        'Web Pieter',
        'Test Jethro Task Widget Title',
        'Macanta Installs',
        'Companies',
        'My Tasks',
      ],
    },
  },
};

sortWidgets(payload, {}, () => {});
