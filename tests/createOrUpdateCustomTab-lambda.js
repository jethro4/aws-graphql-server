const {
  createOrUpdateCustomTab,
} = require('../admin-service/createOrUpdateCustomTabLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      id: 'ct_ki62gb1g',
      title: 'Embed Test - Edited',
      content:
        '<p><iframe src="https://nps.loyaltymetrics.cl/r/494682_5fbcf92863a797.16338694" style="overflow: hidden;" width="1200" height="1200" frameborder="0"></iframe></p>',
      link: 'yes',
    },
  },
};

createOrUpdateCustomTab(payload, {}, () => {});
