const {
  query,
} = require('../relationship-service/queryDOToDORelationshipsLambda');

const payload = {
  request: {
    headers: {
      'app-name': 'staging',
      'app-api-key': '1552679244144731',
    },
  },
  arguments: {
    doItemId: 'item_9a3cd76a3be6f004',
    groupId: 'ci_4458c47816f984ed',
    type: 'direct',

    // doItemId: 'item_9a3cd76a3be6f004',
    // groupId: 'ci_4458c47816f984ed',
    // type: 'indirect',
  },
};

query(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
