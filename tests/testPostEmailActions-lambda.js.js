const {
  postEmailAction,
} = require('../automation-service/postEmailActionLambda');

const payload = {
  arguments: {
    input: {
      appName: 'staging',
      apiKey: '1552679244144731',
      sessionName: 'sess_f4859f3a85738513',
      id: 'ea_1b2a84aa32b20101',
      name: 'New Email Action From Lambda',
      description: 'New Email Action From Lambda Description',
      fromAddress: 'email@email.com',
      fromName: 'John Smith',
      subject: 'Test Email Lambda to Staging',
      previewText: 'Test Message',
      senderSigRelationship: 'Test Message',
      attachment: 'yes',
      ccEmail: [
        {contactId: '123456', bcc: 'no'},
        {contactId: '1234567', bcc: 'no'},
      ],
    },
  },
};

postEmailAction(payload, {}, (err, res) => {
  console.log(JSON.stringify(res));
});
