const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateRelationship = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postRelationship(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateRelationship. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateRelationship', err);
    callback(err);
  }
};

async function postRelationship(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    relationships: [
      {
        ...(params.id && {
          Id: params.id,
        }),
        RelationshipName: params.title,
        RelationshipDescription: params.description,
      },
    ],
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/relationship`;

  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  return {
    id: json.id || params.id,
    title: params.title,
    description: params.description,
  };
}
