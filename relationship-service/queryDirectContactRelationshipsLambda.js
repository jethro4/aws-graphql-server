const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDirectContactRelationships(
      event.request.headers,
    );

    // console.log(
    //   'Done queryDirectContactRelationships. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDirectContactRelationships', err);

    callback(err);
  }
};

async function queryDirectContactRelationships({
  'app-name': appName,
  'app-api-key': apiKey,
}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_relation?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json);
}

function transformResponse(json) {
  const {data = []} = json || {};
  let items = [];

  data.forEach((item) => {
    if (!items.some((d) => d.name === item.Label)) {
      items.push({
        id: item.Id,
        name: item.Label,
        description: item.Description,
      });
    }
  });

  return items;
}
