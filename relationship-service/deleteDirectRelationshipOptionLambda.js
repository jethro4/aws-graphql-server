const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteDirectRelationshipOption = async (
  event,
  context,
  callback,
) => {
  try {
    const deleteResponse = await deleteDirectRelationshipOptionFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteDirectRelationshipOption. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deleteDirectRelationshipOption', err);
    callback(err);
  }
};

async function deleteDirectRelationshipOptionFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    relationId: params.relationId,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_relation`;
  const response = await http.delete(url, body);
  await getJsonResponse(response);

  return {
    id: params.relationId,
    success: true,
  };
}
