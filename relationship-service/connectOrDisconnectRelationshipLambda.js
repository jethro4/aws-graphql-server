const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.connectOrDisconnectRelationship = async (
  event,
  context,
  callback,
) => {
  const {appName, apiKey, sessionName, ...data} = event.arguments.input;

  let relationship = 'unlink';

  if (data.role) {
    relationship = data.role;
  } else if (data.roles) {
    relationship =
      !data.roles.length || data.roles.includes('unlink')
        ? 'unlink'
        : data.roles.map((role) => role.trim()).join(',');
  }

  try {
    const body = {
      access_key: apiKey,
      sessionName,
      cd_guid: data.doItemId,
      contactId: data.contactId,
      connected_group: data.groupId,
      relationship,
    };

    if (body.relationship !== 'unlink') {
      const unlinkedBody = {...body, relationship: 'unlink'};
      await postRelationship(appName, unlinkedBody);
    }
    const postResponse = await postRelationship(appName, body);

    // console.log(
    //   'Done connectOrDisconnectRelationship. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on connectOrDisconnectRelationship', err);
    callback(err);
  }
};

async function postRelationship(appName, body) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/edit`;

  const response = await http.post(url, body);

  await getJsonResponse(response);

  return {success: true};
}
