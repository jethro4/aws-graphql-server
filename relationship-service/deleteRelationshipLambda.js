const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteRelationship = async (event, context, callback) => {
  try {
    const postResponse = await deleteRelationshipFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteRelationship. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteRelationship', err);
    callback(err);
  }
};

async function deleteRelationshipFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    relationshipIds: [params.id],
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/relationship`;

  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  return {
    success: json.Status,
    id: params.id,
  };
}
