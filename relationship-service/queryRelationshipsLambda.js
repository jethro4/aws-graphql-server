const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryRelationships(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryRelationships. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryRelationships', err);

    callback(err);
  }
};

module.exports.queryRelationships = queryRelationships;

async function queryRelationships(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const {groupId} = params;

  let url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/relationships?access_key=${apiKey}`;

  if (groupId) {
    url = url.concat(`&type=${groupId}`);
  }

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const relationships = transformResponse(json, groupId);

  return relationships;
}

function transformResponse(json, groupId) {
  const relationships = [];

  for (var relationship of json) {
    const relationshipObj = {
      id: relationship.Id,
      role: relationship.Title,
      description: relationship.Description,
      ...(groupId && {
        groupId,
        exclusive: relationship.exclusive,
        limit: Number(relationship.limit) || 0,
        autoAssignLoggedInUser: relationship.AutoAssignLoggedInUser === 'yes',
        autoAssignContact: relationship.AutoAssignContact === 'yes',
      }),
    };

    relationships.push(relationshipObj);
  }

  return relationships;
}
