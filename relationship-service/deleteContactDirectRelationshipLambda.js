const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.deleteContactDirectRelationship = async (
  event,
  context,
  callback,
) => {
  try {
    const deleteResponse = await deleteContactDirectRelationshipFunc(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done deleteContactDirectRelationship. deleteResponse:',
    //   JSON.stringify(deleteResponse),
    // );

    callback(null, deleteResponse);
  } catch (err) {
    console.log('Error on deleteContactDirectRelationship', err);
    callback(err);
  }
};

async function deleteContactDirectRelationshipFunc(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    contactId: params.contactId,
    relatedContactId: params.relatedContactId,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_relationship`;
  const response = await http.delete(url, body);
  await getJsonResponse(response);

  return {
    id: params.contactId,
    success: true,
  };
}
