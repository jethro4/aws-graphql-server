const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryContactRelationships(
      event.request.headers,
      event.arguments,
    );

    // console.log(
    //   'Done queryContactRelationships. queryResponse:',
    //   JSON.stringify(queryResponse),
    // );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryContactRelationships', err);

    callback(err);
  }
};

async function queryContactRelationships(
  {'app-name': appName, 'app-api-key': apiKey},
  {type, id},
) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_relationship?api_key=${apiKey}&contactId=${id}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  return transformResponse(json, type);
}

function transformResponse(json, type) {
  const {data} = json || {};
  const isIndirect = type === 'indirect';
  const itemData =
    data[isIndirect ? 'indirectRelationship' : 'directRelationship'] || [];

  const itemsObj = itemData.reduce((acc, item) => {
    if (item.relatedContact?.contactId) {
      const name = `${item.relatedContact.firstName || ''} ${
        item.relatedContact.lastName || ''
      }`.trim();
      const relItem = acc[name] || {
        contactId: item.relatedContact.contactId,
        name,
        email: item.relatedContact.email,
        relationships: [],
        ...(!isIndirect && {
          directRelationId: item.relationId,
        }),
      };

      if (!relItem.relationships.some((rel) => rel.relId === item.relationId)) {
        relItem.relationships.push({
          relId: isIndirect ? item.relationId : item.relatedContact.relationId,
          name: isIndirect
            ? item.relationName
            : item.relatedContact.relationName,
        });
      }

      acc[name] = relItem;
    }

    return acc;
  }, {});

  const items = Object.values(itemsObj);

  return items;
}
