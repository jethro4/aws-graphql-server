const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {transformId} = require('./queryDOToDORelationshipsLambda');

module.exports.deleteDOToDORelationship = async (event, context, callback) => {
  try {
    const postResponse = await deleteDOToDORelationshipFunc(
      event.request.headers,
      event.arguments.input,
    );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on deleteDOToDORelationship', err);
    callback(err);
  }
};

async function deleteDOToDORelationshipFunc(headers, params) {
  await callDeleteDOToDORelationship(headers, {
    itemIdA: params.itemIdA,
    itemIdB: params.itemIdB,
  });

  try {
    await callDeleteDOToDORelationship(headers, {
      itemIdA: params.itemIdB,
      itemIdB: params.itemIdA,
    });
  } catch (e) {
    // no need to throw error for failed 2nd deletion
  }

  return {
    success: true,
    id: transformId({
      groupId: params.groupId,
      itemIdA: params.itemIdA,
      itemIdB: params.itemIdB,
      type: 'direct',
    }),
  };
}

async function callDeleteDOToDORelationship(headers, params) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    itemIdA: params.itemIdA,
    itemIdB: params.itemIdB,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/type/connect_item`;
  const response = await http.delete(url, body);

  const json = await getJsonResponse(response);

  if (!json.Status) {
    throw new Error('Delete failed');
  }
}
