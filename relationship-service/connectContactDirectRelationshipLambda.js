const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.connectContactDirectRelationship = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postConnectContactDirectRelationship(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done connectContactDirectRelationship. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on connectContactDirectRelationship', err);
    callback(err);
  }
};

async function postConnectContactDirectRelationship(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    contactId: params.contactId,
    relation: params.relationId,
    relationships: params.relationships?.reduce((acc, item) => {
      acc[item.contactId] = item.relationId;

      return acc;
    }, {}),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_relationship`;
  const response = await http.post(url, body);
  await getJsonResponse(response);

  return {
    success: true,
  };
}
