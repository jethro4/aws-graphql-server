const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {syncPromises} = require('../utils/promise');
const {
  queryDOToDORelationships,
  transformItem,
} = require('./queryDOToDORelationshipsLambda');
const {queryDOToDORelatedGroups} = require('./queryDOToDORelatedGroupsLambda');
const {sortArray} = require('../utils/array');

module.exports.connectDOToDORelationship = async (event, context, callback) => {
  try {
    const postResponse = await postConnectDOToDORelationship(
      event.request.headers,
      event.arguments.input,
    );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on connectDOToDORelationship', err);
    callback(err);
  }
};

async function postConnectDOToDORelationship(headers, params) {
  const type = 'direct';

  const doRelatedGroups = await queryDOToDORelatedGroups(headers);

  await syncPromises(params.data, async (d) => {
    const groupIds = sortArray([params.groupId, d.groupId]);
    let relatedGroupId = doRelatedGroups.find((relatedGroup) =>
      groupIds.every(
        (groupId, index) => relatedGroup.groupIds[index] === groupId,
      ),
    )?.relatedGroupId;

    if (!relatedGroupId) {
      const connectedDOGroup = await connectDOGroups(headers, groupIds);

      relatedGroupId = connectedDOGroup.relatedGroupId;
    }

    await connectDOToDORelationship(headers, params, {
      relatedGroupId: relatedGroupId,
      itemIdA: params.doItemId,
      itemIdB: d.doItemId,
    });

    try {
      await connectDOToDORelationship(headers, params, {
        relatedGroupId: relatedGroupId,
        itemIdA: d.doItemId,
        itemIdB: params.doItemId,
      });
    } catch (e) {
      // no need to throw error for failed 2nd connection
    }
  });

  const doDirectItems = await queryDOToDORelationships(headers, {
    doItemId: params.doItemId,
    groupId: params.groupId,
    type: 'direct',
  });

  return params.data.map((d) => {
    const item = doDirectItems.find(
      (directItem) => directItem.doItemId === d.doItemId,
    );

    return transformItem({
      itemIdA: params.doItemId,
      itemIdB: d.doItemId,
      groupId: d.groupId,
      type,
      data: item.data,
    });
  });
}

async function connectDOToDORelationship(
  headers,
  params,
  {relatedGroupId, itemIdA, itemIdB},
) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    relatedGroupId: relatedGroupId,
    itemIdA,
    itemIdB,
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/type/connect_item`;
  const response = await http.post(url, body);

  await getJsonResponse(response);
}

async function connectDOGroups(headers, groupIds) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const body = {
    api_key: apiKey,
    groupIdA: groupIds[0],
    groupIdB: groupIds[1],
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/type/connect`;
  const response = await http.post(url, body);

  const json = await getJsonResponse(response);

  const {RelatedGroupId: relatedGroupId} = json.data;
  return {relatedGroupId};
}
