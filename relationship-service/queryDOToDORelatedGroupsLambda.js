const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {sortArray} = require('../utils/array');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDOToDORelatedGroups(event.request.headers);

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDOToDORelatedGroups', err);

    callback(err);
  }
};

module.exports.queryDOToDORelatedGroups = queryDOToDORelatedGroups;

async function queryDOToDORelatedGroups({
  'app-name': appName,
  'app-api-key': apiKey,
}) {
  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/types/connection?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const relationships = transformResponse(json);

  return relationships;
}

function transformResponse(json) {
  const {data = []} = json;
  const relatedGroups = [];

  data.forEach((d) => {
    relatedGroups.push({
      relatedGroupId: d.relatedGroupId,
      groupIds: sortArray([d.groupIdA, d.groupIdB]),
    });
  });

  return relatedGroups;
}
