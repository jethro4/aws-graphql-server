const isArray = require('lodash/isArray');
const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');
const {sortArray, convertPropertiesToArray} = require('../utils/array');
const {queryDOToDORelatedGroups} = require('./queryDOToDORelatedGroupsLambda');

module.exports.query = async (event, context, callback) => {
  try {
    const queryResponse = await queryDOToDORelationships(
      event.request.headers,
      event.arguments,
    );

    callback(null, queryResponse);
  } catch (err) {
    console.log('Error on queryDOToDORelationships', err);

    callback(err);
  }
};

module.exports.queryDOToDORelationships = queryDOToDORelationships;
module.exports.transformId = transformId;
module.exports.transformItem = transformItem;

async function queryDOToDORelationships(headers, params) {
  let relationships;

  const doRelatedGroups = await queryDOToDORelatedGroups(headers);

  if (params.type === 'indirect') {
    relationships = await queryDirectDORelationships(
      headers,
      params,
      doRelatedGroups,
    );
  } else {
    relationships = await queryDirectDORelationships(
      headers,
      params,
      doRelatedGroups,
    );
  }

  return relationships;
}

async function queryDirectDORelationships(
  headers,
  {doItemId, groupId, type = 'direct'},
  doRelatedGroups,
) {
  const {'app-name': appName, 'app-api-key': apiKey} = headers;

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/data_object/types/connection/itemA/${doItemId}?api_key=${apiKey}`;

  const response = await http.get(url);

  const json = await getJsonResponse(response);

  const relationships = transformDirectDOResponse(
    json,
    doRelatedGroups,
    doItemId,
    groupId,
    type,
  );

  return relationships;
}

function transformDirectDOResponse(
  json,
  doRelatedGroups,
  doItemId,
  groupId,
  type,
) {
  const {
    data: {relatedGroups},
  } = json;

  const relatedGroupIds = Object.keys(relatedGroups);

  const connectedDORelationships = [];

  relatedGroupIds.forEach((relatedGroupId) => {
    const doRelatedGroup = doRelatedGroups.find(
      (relatedGroup) => relatedGroup.relatedGroupId === relatedGroupId,
    );
    const derivedGroupId =
      doRelatedGroup?.groupIds.filter((id) => id !== groupId)[0] || groupId;
    const doItems = relatedGroups[relatedGroupId];

    if (isArray(doItems)) {
      doItems.forEach((item) => {
        connectedDORelationships.push(
          transformItem({
            itemIdA: doItemId,
            itemIdB: item.id,
            groupId: derivedGroupId,
            type,
            data: convertPropertiesToArray(item, {
              labelKey: 'fieldName',
              valueKey: 'value',
              omitKeys: ['id'],
            }),
          }),
        );
      });
    }
  });

  return connectedDORelationships;
}

function transformId({groupId, itemIdA, itemIdB, type}) {
  const itemIds = sortArray([itemIdA, itemIdB]);

  return [groupId, type, ...itemIds].join('-');
}

function transformItem({groupId, itemIdA, itemIdB, type, data}) {
  return {
    id: transformId({groupId, itemIdA, itemIdB, type}),
    doItemId: itemIdB,
    groupId,
    type,
    data,
  };
}
