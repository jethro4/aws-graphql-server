const {query: queryRelationships} = require('./queryRelationshipsLambda');
const {
  query: queryDOToDORelationships,
} = require('./queryDOToDORelationshipsLambda');
const {
  query: queryDOToDORelatedGroups,
} = require('./queryDOToDORelatedGroupsLambda');
const {
  createOrUpdateRelationship,
} = require('./createOrUpdateRelationshipLambda');
const {
  connectOrDisconnectRelationship,
} = require('./connectOrDisconnectRelationshipLambda');
const {deleteRelationship} = require('./deleteRelationshipLambda');
const {
  connectDOToDORelationship,
} = require('./connectDOToDORelationshipLambda');
const {deleteDOToDORelationship} = require('./deleteDOToDORelationshipLambda');
const {
  query: queryContactRelationships,
} = require('./queryContactRelationshipsLambda');
const {
  query: queryDirectContactRelationships,
} = require('./queryDirectContactRelationshipsLambda');
const {
  createOrUpdateContactRelationship,
} = require('./createOrUpdateContactRelationshipLambda');
const {
  connectContactDirectRelationship,
} = require('./connectContactDirectRelationshipLambda');
const {
  deleteContactDirectRelationship,
} = require('./deleteContactDirectRelationshipLambda');
const {
  deleteDirectRelationshipOption,
} = require('./deleteDirectRelationshipOptionLambda');

module.exports.relationship = handleRequest;

function handleRequest(...params) {
  const event = params[0];
  const callback = params[2];

  console.log(
    'Lambda handleRequest',
    event.info.fieldName,
    event.info.parentTypeName,
    event,
  );

  switch (event.info.fieldName) {
    case 'listRelationships':
      queryRelationships(...params);
      break;
    case 'listDOToDORelationships':
      queryDOToDORelationships(...params);
      break;
    case 'listDOToDORelatedGroups':
      queryDOToDORelatedGroups(...params);
      break;
    case 'createOrUpdateRelationship':
      createOrUpdateRelationship(...params);
      break;
    case 'connectOrDisconnectRelationship':
      connectOrDisconnectRelationship(...params);
      break;
    case 'deleteRelationship':
      deleteRelationship(...params);
      break;
    case 'connectDOToDORelationship':
      connectDOToDORelationship(...params);
      break;
    case 'deleteDOToDORelationship':
      deleteDOToDORelationship(...params);
      break;
    case 'listContactRelationships':
      queryContactRelationships(...params);
      break;
    case 'listDirectContactRelationships':
      queryDirectContactRelationships(...params);
      break;
    case 'createOrUpdateContactRelationship':
      createOrUpdateContactRelationship(...params);
      break;
    case 'connectContactDirectRelationship':
      connectContactDirectRelationship(...params);
      break;
    case 'deleteContactDirectRelationship':
      deleteContactDirectRelationship(...params);
      break;
    case 'deleteDirectRelationshipOption':
      deleteDirectRelationshipOption(...params);
      break;
    default:
      callback(new UnknownFieldException(event.info.fieldName), null);
      break;
  }
}

class UnknownFieldException extends Error {
  constructor(eventFieldName) {
    super('Unknown field: ' + eventFieldName);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, UnknownFieldException);
    }
    this.name = 'UnknownFieldException';
  }
}
