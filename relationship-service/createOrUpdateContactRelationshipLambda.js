const http = require('../utils/http');
const envConfig = require('../config/envConfig');
const getJsonResponse = require('../utils/getJsonResponse');

module.exports.createOrUpdateContactRelationship = async (
  event,
  context,
  callback,
) => {
  try {
    const postResponse = await postContactRelationships(
      event.request.headers,
      event.arguments.input,
    );

    // console.log(
    //   'Done createOrUpdateContactRelationship. postResponse:',
    //   JSON.stringify(postResponse),
    // );

    callback(null, postResponse);
  } catch (err) {
    console.log('Error on createOrUpdateContactRelationship', err);
    callback(err);
  }
};

async function postContactRelationships(
  {'app-name': appName, 'app-api-key': apiKey},
  params,
) {
  const body = {
    api_key: apiKey,
    relationName: params.name,
    relationDescription: params.description,
    ...(params.id && {
      relationId: params.id,
    }),
  };

  const url = `https://${appName}.${envConfig.apiDomain}/rest/v1/contact_relation`;
  const response = await http.post(url, body);
  const json = await getJsonResponse(response);

  return {
    id: params.id || (json.data?.length && json.data[0].Id),
    name: params.name,
    description: params.description,
  };
}
